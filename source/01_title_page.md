<!-- 
This is the Latex-heavy title page. 
People outside UCL may want to remove the header logo 
and add the centred logo
-->

\begin{titlepage}
    \begin{center}

        \includegraphics[width=0.4\textwidth,natwidth=610,natheight=642]{style/univ_logo.eps}
        %\vspace*{0.5cm}

	
        \Large
        Tesis de Ingeniería en Informática
        
        \huge
        Estudio de la correspondencia entre modelos UML y código embebido, eficiente y orientado a objetos en lenguaje C


        
        \Large
        Autor: Jonathan Emanuel Marino (87350) (jonymarino@gmail.com)


                
        \vfill
        
        \normalsize
        Tutores:

        Lic. Rosita Wachenchauzer (rositaw@gmail.com)    \\
	Ing. Diego Essaya (dessaya@gmail.com) \\

        \vspace{0.5cm}



        
        \normalsize
        Facultad de ingeniería UBA, Buenos Aires\\
        21/09/2019

    \end{center}
\end{titlepage}

