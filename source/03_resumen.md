# Resumen {.unnumbered}
C es un lenguaje de programación desarrollado a principios de la década de 1970,  ampliamente usado hasta el día de hoy con gran preponderancia en los sistemas embebidos. El desarrollo de software (en cualquier lenguaje) puede ser potenciado con el uso de modelos y generación automática de código a través de estos
lenguajes. Los modelos orientados a objetos son bien representados con UML (por sus siglas en
inglés, Unified Modeling Language), un lenguaje de modelado con gran aceptación y popularidad. Por su parte, C es un lenguaje que no contiene conceptos de orientación a objetos en forma nativa.  
Esta tesis tiene como primer objetivo estudiar qué modelos orientados a objetos son fácilmente expresables en C cuando es utilizado junto con distintas especificaciones y frameworks que contienen los conceptos de orientación a objetos. Esto puede mostrar a C como un lenguaje apropiado para la enseñanza de la orientación a objetos: un único lenguaje no relacionado con alguna forma de orientación a objetos tomando, de cierta manera, la forma de otros lenguajes y a la vez dejando al descubierto sus costos.  
Además, la escritura de código en C bajo estas especificaciones o frameworks es dificultosa. Esta tesis busca, también, analizar en qué medida la experiencia del programador es mejorada al utilizar un generador de código desde UML para producir dicho código.  
Por último, el tercer objetivo de esta tesis es encontrar la factibilidad de implementar un generador de código para los frameworks estudiados desde diagramas UML con herramientas actuales.  

\pagenumbering{roman}
\setcounter{page}{1}
