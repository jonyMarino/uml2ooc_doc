# Agradecimientos {.unnumbered}

<!-- This is for acknowledging all of the people who helped out -->
De acuerdo al judaísmo el agradecimiento al Todopoderoso debe ir primero, sin su generosidad nada habría.  
Agradezco enormemente a mi esposa Carolina, es imposible lograr objetivos que requieran sacrificio en la vida si nuestra media alma no los valorara, gracias por esperarme tantos días bien entrada la noche para cenar juntos y luego ir a trabajar al otro día temprano, gracias por nuestros hermosos hijos Uriel y David que son el combustible más poderoso para nuestro progreso.  
Agradezco a mi Madre por el impresionante esmero y sacrificio que ha hecho para que tengamos una buena educación. Recién cuando uno es padre puede darse cuenta el tremendo desafío que puede ser criar tres hijos tantos años sola. Todavía recuerdo esos tuppers gigantes con el almuerzo y la cena los días que me quedaba todo el día en la facultad. Algo que ahora entiendo es que la autoestima de los hijos está en gran medida depositada en los padres, autoestima que me permite hasta hoy día soñar en grande.  
Gracias a mis hermanas Samanta y Johanna por hacer tan agradable la vida en familia, tantos momentos juntos y tantos más por vivir. A mis cuñados Mati y Pato por encargarse de hacerlas felices y encajar tan bien en la familia.  
Gracias a mis abuelos Jorge y Lidia que inculcaron en la familia el amor y respeto por el estudio, y que nos ayudaron para que recibiéramos una educación de nivel. Varias veces su casa fue media casa mía para poder estar más cerca de la secundaria o de la universidad y siempre mi abuela me esperó con un plato caliente en su casa. Sé que mi abuelo está contento por este momento en el mundo de la verdad.  
Gracias a mi cuñado Federico Somoza, editor de la revista del Hospital Italiano, por la ayuda en la corrección de este escrito y en general por ser tan buena persona con nosotros.  
Gracias a mis suegros Hugo y Lita, si no fuera por su ayuda a la familia sería muy difícil disponer del tiempo para proyectos de cualquier tipo. Gracias por su amor, tolerancia y apertura.  
Gracias a mis tutores de tesis que en forma desinteresada se ocupan de los alumnos que deseamos recibirnos con este honor.  
Gracias a mi primer jefe, Daniel Litvinov socio de Dhacel SRL, que me permitió desarrollarme y aplicar en los sistemas embebidos los conocimientos que fueron la semilla de esta tesis. Gracias a mi último jefe hasta el momento, Sergio Starkloff socio de Surix SRL, por darme lugar a utilizar el generador de código creado para esta tesis en un ambiente de producción ayudando a profundizar mi conocimiento en el tema.  
Gracias a la UBA por la educación formal que he recibido y a la nación Argentina que me la ha dado gratuitamente.  
Gracias a cada uno de los familiares, amigos y personas que influencian en mi vida para bien.  
Gracias al pueblo judío por preservar su milenaria tradición religiosa de la que me he nutrido diariamente desde los 21 años y me ayuda a ver la vida de forma distinta, con un propósito y, por lo tanto, con más energía.  

Por último de nuevo agradezco al Dador constante de la vida ya que todos estos agradecimientos son fundados en Él.  

<!-- Use the \newpage command to force a new page -->

\newpage
