# Abreviaturas {.unnumbered}

\begin{tabbing}
\textbf{API }~~~~~~~~~~~~ \= \textbf{I}nterfaz de \textbf{P}rogramación de \textbf{A}plicaciones  \\  
\textbf{OO  }~~~~~~~~~~~~ \= \textbf{O}rientación a  \textbf{O}bjetos \\  
\textbf{POO }~~~~~~~~~~~ \= \textbf{P}rogramación \textbf{O}rientada a  \textbf{O}bjetos \\  
\textbf{LPOO}~~~~~~~~~~~ \= \textbf{L}enguaje  de \textbf{P}rogramación \textbf{O}rientada a  \textbf{O}bjetos \\  
\textbf{COO }~~~~~~~~~~~~ \= \textbf{C} con características de la \textbf{O}rientación a  \textbf{O}bjetos \\  
\textbf{LOC }~~~~~~~~~~~~ \= \textbf{L}ines \textbf{O}f  \textbf{C}ode: Líneas de código \\  
\textbf{RTTI}~~~~~~~~~~~~ \= \textbf{R}un \textbf{T}ime  \textbf{T}ype  \textbf{I}nformation: \\  
~~~~~~~~~~~~~~~~~~~~~~  		Información de tipo en tiempo de ejecución  \\  
\textbf{UML }~~~~~~~~~~~~ \= \textbf{U}nified \textbf{M}odel \textbf{L}anguage: Lenguaje de modelado unificado  \\  
\end{tabbing}

\newpage
\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}

