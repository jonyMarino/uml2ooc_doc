# Introducción 

## Acerca

La programación orientada a objetos en C es posible y es utilizada en la actualidad para todo tipo de aplicaciones a pesar de que C no sea un lenguaje orientado a objetos. Según como se resuelva codificar la orientación a objetos en C, o sea con qué librerías, herramientas y bajo que estándar de C, esa codificación resulta dificultosa. Sin embargo, no por eso debemos desechar a C para programar orientado a objetos. Con las herramientas adecuadas podemos suplir estas dificultades en la codificación orientada a objetos en C.  

Esta tesis busca analizar la conveniencia de utilizar un generador de código desde diagramas de clase UML (lenguaje de modelado unificado) como herramienta de ayuda para codificar C orientado a objetos, además de servir como herramienta para facilitar el uso de modelos en el desarrollo.

Para este estudio se han seleccionado distintas especificaciones o frameworks para la codificación de C orientado a objetos, cuyos objetivos van desde los sistemas embebidos altamente restringidos[^0] hasta servir de lenguajes de propósito  general. Se ha desarrollado, para algunos de ellos, un generador de código desde diagramas de clase UML analizando la ventaja obtenida al utilizarla.

[^0]: Nos referimos a restricciones de recursos como memoria ROM y RAM, así como velocidad en ciclos de procesamiento. "Altamente restringido" no es un atributo bien definido, en esta tesis nos referimos a sistemas con menos de 1MB de RAM y ROM. En estos sistemas el lenguaje C es el más utilizado, como veremos.

## Estructura de la tesis

Este es un breve resumen del contenido de cada capítulo. El **Capítulo 2** introduce los conceptos de orientación a objetos y a UML como lenguaje adecuado para representar dichos conceptos. El **Capítulo 3**  muestra la necesidad que existe de la programación orientada a objetos en C y da algunos ejemplos de su factibilidad. El **Capítulo 4** introduce distintas especificaciones y frameworks para codificar C orientado a objetos, los conceptos que contienen y sus dificultades en la codificación. Se propone una representación en UML para el código bajo dichas especificaciones o frameworks, representación que está desprovista de dichas dificultades. El **Capítulo 5** introduce herramientas para transformar  modelos en código.El **Capítulo 6** muestra la factibilidad de implementar un generador de código desde diagramas UML para los frameworks analizados. El **Capítulo 7** habla acerca de las conclusiones extraídas de esta tesis y trabajo futuro.  


