# Programación orientada a objetos y UML


## Orientación a Objetos


### Introducción
<!--
Qué es la orientación a objetos
#### Historia
En el comienzo los programas se escribían bajo el modelo GO-TO, una instrucción que permite definir el flujo de un programa saltando desde la instrucción go-to a cualquier parte del código del programa. Cualquier problema computacional puede resolverse bajo este modelo pero a medida que los programas fueron aumentando su complejidad este modelo mostró sus problemas. Él código generado para resolver problemas complejos bajo este modelo comenzó a compararse con el enriedo de un plato de espagueti apodándose código espagueti. Nadie, quizás sólo el autor del código era capaz de entender y por lo tanto mantener un programa complejo con saltos hacia todos lados.
De esta forma surgió, en la década del 60, la programación estructurada, un paradigma de programación orientado a mejorar la claridad del código recurriendo únicamente a subrutinas y tres estructuras básicas: secuencia, selección (if y switch) e iteración (bucles for y while). De esta forma vuelve innecesario el uso de la instrucción GO-TO. 
A medida que los programas se fueron haciendo todavía más complejos[Dynace2004] 

 Actualidad
The contemporary view of software development takes an object-oriented perspective. In this approach, the main building block of all software systems is the object or class. Simply put, an object is a thing, generally drawn from the vocabulary of the problem space or the solution space; a class is a description of a set of common objects. Every object has identity (you can name it or otherwise distinguish it from other objects), state (there's generally some data associated with it), and behavior (you can do things to the object, and it can do things to other objects, as well).  
The object-oriented approach to software development is decidedly a part of the mainstream simply because it has proven to be of value in building systems in all sorts of problem domains and encompassing all degrees of size and complexity. Furthermore, most contemporary languages, operating systems, and tools are object-oriented in some fashion, giving greater cause to view the world in terms of objects.
[@BRJ98]
traducción:
 En este enfoque, el bloque de construcción principal de todos los sistemas de software es el objeto o la clase. En pocas palabras, un objeto es una cosa, generalmente extraído del vocabulario del espacio del problema o del espacio de la solución; una clase es una descripción de un conjunto de objetos comunes. Cada objeto tiene identidad (puede nombrarlo o, de lo contrario, distinguirlo de otros objetos), estado (generalmente hay algunos datos asociados con él) y comportamiento (puede hacer cosas al objeto, y puede hacer cosas a otros objetos, como bien).
El enfoque orientado a objetos para el desarrollo de software es decididamente parte de la corriente principal simplemente porque ha demostrado ser valioso en la construcción de sistemas en todo tipo de dominios de problemas y abarca todos los grados de tamaño y complejidad. Además, la mayoría de los lenguajes contemporáneos, los sistemas operativos y las herramientas están orientados a objetos de alguna manera, lo que proporciona una causa mayor para ver el mundo en términos de objetos.
-->
La mayoría del software contemporáneo  es desarrollado bajo el enfoque de la orientación a objetos. Bajo este enfoque la unidad básica de construcción de todos los sistemas de software es el objeto. Un objeto busca representar un sujeto dentro del dominio  del problema o la solución; las clases son una descripción general de un objeto y son los generadores de objetos con esa descripción. Cada objeto tiene identidad, estado y comportamiento. Identidad implica que puede distinguirse de los demás objetos y asignársele un nombre en el código. Estado implica que tiene datos asociados con él, que pueden ir cambiando durante la vida del objeto. Comportamiento implica la capacidad de un objeto de hacer cosas y de operar sobre otros objetos y variables incluyendolo a él mismo, la forma de operar sobre los objetos es mediante el envío de mensajes a los mismos.
En este enfoque, llamado paradigma, se busca identificar los objetos del dominio del problema y se busca distribuir la responsabilidad de la solución en dichos objetos.
Las virtudes de la programación orientada a objetos son por demás conocidas: modularidad,  flexibilidad,  extensibilidad, reutilización de código. La orientación a objetos ha sido la aplicación de los buenos principios de programación enseñados con anterioridad a su existencia [@Douglass2010].
El enfoque de la orientación a objetos ha demostrado ser valioso en la comprensión de problemas y su solución en todo tipo de dominios de problemas, así también como grados de tamaño y complejidad.  Además, la mayoría de los lenguajes contemporáneos están orientados a objetos de alguna manera, lo que proporciona una causa mayor para ver el mundo en términos de objetos.
Fuera de esta definición simplista, definir exactamente qué es la orientación a objetos es una tarea muy difícil. Algunos consideran como esencial lo que para otros no lo es. Alrededor de la orientación a objetos distintos conceptos han sido elaborados, y cada implementación a tomado un subconjunto de aquellos [@Hendrickx2004].


### Conceptos
<!--
Let’s take a look at the common concepts that are often associated with OO.
Encapsulation, objects, classes, inheritance and polymorph-ism may be regarded
as the essential basis for OO.
For one, encapsulation may as well be the corner stone of OO. [PJ00] summa-
rizes this nicely by his story about putting a group of OO gurus together who
have to create a list of what they see as properties of OO. The list contained
only the concepts that all the gurus had in common on their list. This resulted
in only one word: ”encapsulation”.
Moreover, [PJ00] sums 9 concepts which are ”encapsulation”, ”information
and implementation hiding”, ”state retention”, ”object identity”, ”messages”,
”classes”, ”inheritance”, ”polymorph-ism” and ”genericity”. We group most of
those concepts together to the five we stated (as will be noted in our overview).
Also, Stroutstrup asserts ”The basic support a programmer needs to write
object-oriented programs consists of a class mechanism with inheritance and
a mechanism that allows calls of member functions to depend on the actual
type of an object (in cases where the actual type is unknown at compile time)”.
[Str91]

Indeed, it are those concepts that also return in many environments like C++,
Java, Smalltalk, .Net and other class based OO environments.


Although the reader should be familiar with those concepts, lets take a closer
look at them for sake of clarity and completeness in context of this thesis. The
given description of those concepts is by no means comprehensive or complete,
but that’s not the intention.
-->
Stroutstrup afirma "El soporte básico que necesita un programador para escribir
programas orientados a objetos consiste en un mecanismo de clase con herencia y
un mecanismo que permite que las llamadas de funciones miembro dependan del
tipo de un objeto (en los casos en que el tipo es desconocido en tiempo de compilación) ". [@Str91]
De aquí se desprenden los tres conceptos más esenciales de la orientación a objetos: Encapsulación, herencia y polimorfismo.
<!--Además, Meiler enumera 9 conceptos como básicos, ellos son "encapsulación", "información
y ocultación de la implementación "," retención de estado "," identidad de objeto "," mensajes ",
"Clases", "herencia", "polimorfismo" y "genericidad"[@PJ00].  -->
Para obtener mayor claridad de los frameworks que estudiaremos introduciremos primero estos conceptos básicos. A continuación introduciremos otros conceptos implementados por algunos de ellos. La descripción dada de esos conceptos no es de ninguna manera completa sino introductoria.
<!--
fuente:
***encapsulation*** Encapsulation and data hiding are important concepts used
in OO. With encapsulation data is closed in an object where the user is
provided with a well defined interface such that the data only can be
manipulated through that interface. With data hiding, the data is not
only encapsulated (think ”protected”) but also invisible from the user.
objects and classes An object is described by a class, where the class
describes a type. An object is an instance of such a type. Often an
object is being summarized as a state, an identity and behavior [Boo91].
The state of an object is determined by the values of its encapsulated
data. The Behavior of an object is defined by the interface on that
object. An identity can be any identifier, often referred to as an object
handle or reference.
**encapsulación fuerte** explicar encapsulación fuerte (strong encapsulation)
-->


### Encapsulación
La encapsulación y la ocultación de datos es uno de lo conceptos más importantes de la POO. La misma se consigue a través de la definición de clases en los LPOO. Las clases son el bloque de construcción más importante en la mayoría de los sistemas orientados a objeto [@BRJ98]. El implementador de la clase define los tipos de datos de la misma y la forma de interactuar con ellos mediante una interfaz bien definida, puede haber datos totalmente accesibles por el usuario y, por el otro lado, otros totalmente invisibles al mismo. 


#### Objetos y clases
<!--fuente: Classes are the most important building block of any object-oriented system. BRJ98
-->
Un objeto es descrito por una clase, donde la clase describe un tipo. Un objeto es una instancia de tal tipo. A menudo un objeto se resume como una identidad, un estado, y comportamiento [@Boo91]. 
El estado de un objeto está determinado por los valores de sus datos, una clase define los tipos de datos de la misma y un objeto contiene valores para esos datos. El comportamiento de un objeto está definido por la interfaz definida a su vez en la clase. Una identidad puede ser cualquier identificador, a menudo referido como una referencia al mismo.


#### Encapsulación fuerte
Si una clase sólo permite acceder a sus datos a través de su interfaz, esto se llama encapsulación fuerte. Ciertas implementaciones (tanto lenguajes como en nuestro caso frameworks) de POO pueden obligar la encapsulación fuerte.
<!--
fuente:
***inheritance*** Inheritance is a concept in which a type can be extended: a type can be specialized. It is said that a class B extends another class A and thereby inherits all the properties from class A. I.e. it inherits both data and interface from class A. Inheritance is an important relation between classes.
-->


### Herencia
La herencia es la capacidad que tiene una clase para  extender a otra. Se dice que una clase B extiende otra clase A y, por lo tanto, hereda todas las propiedades de la clase A, eso significa que hereda los datos y la interfaz de la clase A. La herencia es una relación importante entre las clases y es una de las herramientas más utilizadas para la reutilización de código. Las implementaciones de POO pueden soportar tanto **herencia simple** como **herencia múltiple**. Herencia simple significa que una clase sólo puede heredar a lo sumo de otra más. Herencia múltiple significa que una clase puede heredar de una o más clases. La herencia múltiple es bastante criticada por la complejidad que puede traer en los diseños y la complejidad de resolución del problema del diamante [@Estrada2007].

#### Clase base
Una clase base es una clase que no hereda de ningún otra.


#### Clase derivada o hija
Una clase derivada es aquella que hereda de una o varias clases, que pueden ser a su vez derivadas o base. Se dice que una clase derivada o hija hereda de una clase padre.

<!--
fuente:
polymorph-ism polymorphism actually means several things, like message name overloading and operator overloading. However, in the context of this thesis a very strict meaning to polymorph-ism is given.
***Polymorph-ism*** in the context of traditional OOPLs is the possibility of an object to take different forms at runtime: sending a message to an object of a certain (static) type can invoke different behaviors depending on the runtime (or dynamic) type.
-->
### Polimorfismo
Es la capacidad de que una referencia a un tipo tome diferentes formas en tiempo de ejecución: el envío de un mensaje a un objeto de cierto tipo (estático) puede invocar diferentes comportamientos según el tipo de tiempo de ejecución (o dinámico).


#### Mensajes y métodos
Un método es la definición del comportamiento de un objeto ante cierto mensaje en cierto contexto, en caso de que una clase padre ya contenga definición de dicho método, una clase hija puede redefinir dicho comportamiento. El usuario de un objeto envía un mensaje al mismo y el objeto puede ejecutar un método ante tal mensaje.  


#### Reenvío de mensajes y delegados
Cuando un objeto no posee un método para responder a cierto mensaje, si es que soporta el reenvío de mensajes, puede reenviar el mensaje a otro objeto que sí sepa responder a dicho mensaje, dicho objeto es un delegado.


#### Métodos abstractos
Los métodos polimórficos definidos por una clase que no define su implementación son llamados métodos abstractos. La idea de un método abstracto es que las clases derivadas definan su implementación. Una clase con métodos abstractos es llamada una clase abstracta y no puede generar instancias de la misma.


<!--
But even this list is of course subject to discussion. A discussion which has
been many times done before. Some people require that some other concepts
should be part of the OO paradigm, like exception handling, garbage
collecting, built-in operator overloading, runtime introspection and reflection.
However, the 5 latter concepts are tight to the programming
language/environment itself. When OO is viewed purely as a way of thinking,
we find ourselves on the level of program analysis and design. The 5 latter
concepts are not important on this level. What remains are the notion of
classes and the relation between them. In this way OO becomes primarily
what is known as Object Oriented Analysis and Design (OOA&D).
Again, for the sake of clarity, the latter 5 concepts are described here:
-->
Hasta aquí introducimos los conceptos que identificamos como esenciales de la POO.  Los siguientes conceptos, sin embargo, están relacionados con la orientación a objetos y son implementados por los frameworks que estudiaremos. Algunos de ellos son considerados como esenciales a la orientación a objetos por algunos  gurúes, pero en la práctica son más dependientes de la implementación [@Hendrickx2004].


### Interfaces
Una interfaz consiste en un listado de declaración de métodos carentes de implementación.
Una clase se dice que realiza una interfaz e incluye a sus métodos como propios dándoles una implementación a los mismos. 


### Mixin
Los mixins son una mezcla entre clases e interfaces. Definen y pueden implementar nuevos métodos, e incluso pueden poseer datos miembros pero no pueden ser instanciados. Lo único que puede hacerse con un Mixin es que sea heredado por una clase. Puede verse a un Mixin como una interfaz pero que define una implementación. 


### Metaclases
Las instancias de las clases son objetos. Una metaclase es una clase cuyas instancias son clases. Esto permite crear clases en tiempo de ejecución.

<!--
fuente:
***exception handling*** A mechanism for anticipating and handling exceptions.
An exception occurs when something does not go as was intended. A
mechanism for easy exception handling allows the developer to handle
such an exception elegantly. The most known exception handling
systems are those in C++, Java and C#.
-->
### Manejo de excepciones
Un mecanismo para anticipar y manejar excepciones. Se produce una excepción cuando algo no sale como se pretendía. Un mecanismo para un fácil manejo de excepciones permite al desarrollador manejar una excepción elegantemente. Permite separar el código que debería ejecutarse sin problemas de las excepciones y resolver las excepciones en el contexto donde pueden tratarse. Si bien puede haber sistemas de excepciones basadas en un sistema de objetos en C específico a un framework, también existen librerías que pueden utilizarse bajo cualquier especificación o framework [^1].

[^1]: Ver por ejemplo [Exception](http://cern.ch/laurent.deniau/html/exception.tgz) y [cexception](http://www.throwtheswitch.org/cexception)
<!--
fuente;
***garbage collecting*** A system that frees unused memory in the running program. This frees the developer of handling memory management manually.
-->


### Recolector de basura
Un sistema que libera memoria no utilizada en el programa durante la ejecución del mismo. Esto libera al desarrollador de tener que manejar la gestión de memoria manualmente. De nuevo existen librerías que son independientes de un framework de orientación a objetos [^2].  

[^2]: Ver por ejemplo [GCBoehm](https://www.hboehm.info/gc/) y [TinyGC](http://tinygc.sourceforge.net/)

<!--***encapsulation***encapsulation Encapsulation and data hiding are important concepts used
in OO. With encapsulation data is closed in an object where the user is
provided with a well defined interface such that the data only can be
manipulated through that interface. With data hiding, the data is not
only encapsulated (think ”protected”) but also invisible from the user.
objects and classes An object is described by a class, where the class
describes a type. An object is an instance of such a type. Often an
object is being summarized as a state, an identity and behavior [Boo91].
The state of an object is determined by the values of its encapsulated
data. The Behavior of an object is defined by the interface on that
object. An identity can be any identifier, often referred to as an object
handle or reference.
***inheritance*** Inheritance is a concept in which a type can be extended: a type
can be specialized. It is said that a class B extends another class A and
thereby inherits all the properties from class A. I.e. it inherits both data
and interface from class A. Inheritance is an important relation between
classes. This relation is described by an inheritance tree.
polymorph-ism Polymorph-ism actually means several things, like message
name overloading and operator overloading. However, in the context of
this thesis a very strict meaning to polymorph-ism is given.
***Polymorph-ism*** in the context of traditional OOPLs is the possibility of
an object to take different forms at runtime: sending a message to an
object of a certain (static) type can invoke different behaviors depending
on the runtime (or dynamic) type.
But even this list is of course subject to discussion. A discussion which has
been many times done before. Some people require that some other concepts
should be part of the OO paradigm, like exception handling, garbage
collecting, built-in operator overloading, runtime introspection and reflection.
However, the 5 latter concepts are tight to the programming
language/environment itself. When OO is viewed purely as a way of thinking,
we find ourselves on the level of program analysis and design. The 5 latter
concepts are not important on this level. What remains are the notion of
classes and the relation between them. In this way OO becomes primarily
what is known as Object Oriented Analysis and Design (OOA&D).
Again, for the sake of clarity, the latter 5 concepts are described here:
***exception handling*** A mechanism for anticipating and handling exceptions.
An exception occurs when something does not go as was intended. A
mechanism for easy exception handling allows the developer to handle
such an exception elegantly. The most known exception handling
systems are those in C++, Java and C#.
***garbage collecting*** A system that frees unused memory in the running
program. This frees the developer of handling memory management
manually.
***built-in operator overloading*** The ability to override the meaning of
existing operators such as ”+” when used in the context of certain
objects.
fuente:
***runtime introspection*** The ability to introspect an object at runtime. This
enables the developer to query the object and ask for runtime type
information (RTI or RTTI). E.g. what type are you? What is your
name? What is your parent in the inheritance tree?
-->


### Introspección de tiempo de ejecución 
La capacidad de realizar una introspección de un objeto en tiempo de ejecución. Esto permite al desarrollador consultar el objeto y solicitar información de tipo de tiempo de ejecución (RTTI). Por este mecanismo puede obtenerse el tipo de un objeto, el nombre del tipo y cuáles son sus clases padres.
<!--
fuente:
***reflection*** Reflection is defined by [MIKC92] as ”the ability of an executing system of programmed objects to make attributes of the objects to be themselves the subject of computation”. Both Java and .Net are said to have reflection. We make a distinction between two levels of reflection. The first one is as we know it in Java: there is runtime introspection and byte-code introspection. The second one can be summarized as a system where runtime introspection, byte-code introspection and runtime byte-code generation is available such as in the .Net platform. A level 0 of reflection may be regarded as runtime introspection.
[@Hendrickx2004]
-->



###  Reflexión
La reflexión se define por Peter W. Madany, Nayeem Islam, Panos Kougiouris, y Roy H. Campbell como "la capacidad de un sistema de ejecución de objetos programados para hacer que los atributos de los objetos sean en sí mismos objeto de cálculo" [@MIKC92]. En otras palabras, la capacidad de obtener los miembros de los objetos y otra información estructural en tiempo de ejecución. La introspección de tiempo de ejecución puede considerarse un nivel bajo de reflexión [@Hendrickx2004].


#### Programación clave-valor (key-value programming)
La codificación de valor-clave es un mecanismo para acceder a las propiedades de un objeto indirectamente, utilizando cadenas para identificar propiedades, en lugar de invocar un método de acceso o acceder a ellas directamente a través de variables de instancia.
[@Objective-C].


### Duck Typing
Una implementación con capacidad de Duck typing permite enviar cualquier mensaje a cualquier objeto, y la implementación sólo se preocupa de que el objeto pueda aceptar el mensaje, no requiere que el objeto sea de un tipo particular, como hacen Java y C ++ [@Eckel]. En cambio duck typing sigue la filosofía de "[si] camina como un pato, nada como un pato y suena como un pato, a esa ave yo la llamo un pato." [^3], lo que significa que los tipos no son lo más importantes sino el comportamiento (métodos) de los objetos.

[^3]: Davis, Robin S. "_Who's Sitting on Your Nest Egg?" página 7.

### Genericidad
Existen dos acepciones del término que nos interesan:


#### En lenguajes fuertemente tipados 
<!--[Genericidad](https://en.wikipedia.org/wiki/Generic_function)
-->
Una definición simple de un lenguaje fuertemente tipado es que las funciones escritas para cierto tipo sólo pueden ser invocadas con instancias de esos tipos (u otros que hereden de ellos). Por lo que por cada tipo no relacionado con el otro obligatoriamente hay que escribir otra función o método aunque las funciones hagan prácticamente lo mismo. La genericidad permite escribir funciones independientes del tipo que luego en tiempo de compilación se generan las distintas versiones de la función para los distintos tipos con las que se lo usa.  


#### En lenguajes débilmente tipados con funciones genéricas
 En lenguajes débilmente tipados pueden definirse funciones genéricas con múltiples implementaciones que difieren en los tipos de sus argumentos. En tiempo de ejecución, al llamar a tales funciones, es ejecutada la implementación de la función que mejor coincida con los tipos utilizados para la llamada (el nivel de la herencia es contemplado en la elección). Si la función a ejecutar depende de varios de sus argumentos, esto se llama despacho múltiple (multiple dispatch), si sólo depende del primer argumento se llama despacho único (single dispatch). La diferencia entre una función con despacho único y un método polimórfico es que la primera no necesita ser definida como miembro de la clase.


## UML y la Programación Orientada a Objetos


### Qué es UML
<!--



The Unified Modeling Language (UML) is a graphical language for visualizing, specifying, constructing, and documenting the artifacts of a software-intensive system. The UML gives you a standard way to write a system's blueprints, covering conceptual things, such as business processes and system functions, as well as concrete things, such as classes written in a specific programming language, database schemas, and reusable software components. [@BRJ98]
-->
El Lenguaje de modelado unificado (UML) es un lenguaje gráfico para especificar, construir y documentar los artefactos de software. Un artefacto de software es cualquier resultado tangible en el proceso de desarrollo de software. UML provee un lenguaje común para describir las partes importantes de un sistema desde un enfoque conceptual. Puede describir desde procesos de negocio y funciones de un sistema así como cosas concretas como clases escritas en un lenguaje de programación específico y componentes reutilizables de software.  
La notación de UML está especificada por el metamodelo de UML. En esta tesis usaremos la versión de metamodelo 2.5.1 [@UML2017].  
La mayoría del contenido de las próximas secciones está basado en el libro "The Unified Modeling Language User Guide" de los autores de UML  (Grady Booch, James Rumbaugh e Ivar Jacobson) [@BRJ98].


### Ventajas del uso de UML para todos los LPOO
<!--
A language provides a vocabulary and the rules for combining words in that vocabulary for the
purpose of communication. A modeling language is a language whose vocabulary and rules
focus on the conceptual and physical representation of a system.
-->
Un lenguaje consiste en un vocabulario y reglas para utilizar ese vocabulario. Un lenguaje se utiliza para comunicar. Un lenguaje de modelado se utiliza para comunicar modelos. Estos modelos son representaciones conceptuales o físicas de un sistema.   
<!--
For many programmers, the distance between thinking of an implementation and then pounding it
out in code is close to zero. You think it, you code it. In fact, some things are best cast directly in
code. Text is a wonderfully minimal and direct way to write expressions and algorithms.
-->
Muchos programadores están acostumbrados a  codificar inmediatamente después de haber pensado la solución al problema que están resolviendo (sino antes). A veces el texto es la mejor forma de representar algo, como es el caso  en fórmulas matemáticas y algoritmos.
<!--
In such cases, the programmer is still doing some modeling, albeit entirely mentally. He or she
may even sketch out a few ideas on a white board or on a napkin. However, there are several
problems with this. 
-->
En tales casos, los programadores siguen creando un modelo mental que luego es implementado en el código. También podría crear un borrador de su visión mental, pero este enfoque tiene varios inconvenientes: 
<!--
First, communicating those conceptual models to others is error-prone unless everyone involved speaks the same language. Typically, projects and organizations develop their own language, and it is difficult to understand what's going on if you are an outsider or new to the group.
-->
Primero porque comunicar estos modelos a otros es una tarea difícil y propensa a errores. El desarrollador o los desarrolladores crean un lenguaje para poder expresar su modelo y quienes no sean introducidos en ese lenguaje personalizado no podrán o les será difícil entenderlo. 

<!--
 Second, there are some things about a software system you can't understand unless you
build models that transcend the textual programming language. For example, the meaning of a
class hierarchy can be inferred, but not directly grasped, by staring at the code for all the classes
in the hierarchy.
-->
Segundo, hay algunos aspectos de los  sistemas de software que no pueden ser directamente comprendidos a través de tan solo mirar el código. Por ejemplo, extraer el significado de cierta jerarquía de herencia de clases desde el código implicaría inferirlo a partir de varios archivos mientras que a con un modelo podría captarse en forma directa.
<!--
if
the developer who cut the code never wrote down the models that are in his or her head, that information would be lost forever or, at best, only partially recreatable from the implementation,
-->
Si el programador que escribió el código nunca dejó un modelo que documente lo que estaba en su cabeza como solución al problema, esa información puede perderse o llegar a reproducirse parcialmente si ese programador no estuviese más.  


### UML es un lenguaje para construir
Si bien UML no es un lenguaje de programación visual, sus modelos pueden ser conectados a una variedad de lenguajes de programación. Eso significa que es posible mapear modelos en UML a distintos Lenguajes de Programación Orientados a Objetos como JAVA o C++. Las cosas que son mejor expresadas visualmente son hechas en UML, mientras que las cosas que son mejor expresadas textualmente son hechas en el código.
<!--
This mapping permits forward engineering: The generation of code from a UML model into a
programming language. The reverse is also possible: You can reconstruct a model from an
implementation back into the UML. Reverse engineering is not magic. Unless you encode that
information in the implementation, information is lost when moving forward from models to code.
-->
Este mapeo permite la generación de código desde modelos UML, esto es llamado ingeniería directa. También es posible realizar ingeniería inversa: generar modelos UML a partir de código. Como dijimos, el código puede no contener toda la información del modelo por lo tanto lo que no se encuentre en el texto en forma desambiguada no podrá regenerar el modelo a partir del cual se creó el código.

<!--
Reverse engineering thus requires tool support with human intervention. Combining these two
paths of forward code generation and reverse engineering yields round-trip engineering, meaning
the ability to work in either a graphical or a textual view, while tools keep the two views
consistent.
-->
Generar código desde modelos es llamado ingeniería directa, y generar modelos desde código ingeniería inversa. La combinación de los dos da como resultado la ingeniería circular (en inglés round-trip engineering), lo que implica poder trabajar tanto en los gráficos como en el código mientras las herramientas se encargan de mantener la consistencia entre ambos.
En esta tesis buscaremos demostrar que la ingeniería directa también es posible para C a pesar de no ser un LPOO. Bajo ese supuesto es de esperar que la ingeniería inversa también sea posible. 


### El lenguaje de UML
Ahora introduciremos al lenguaje de UML. De ninguna manera buscamos dar una definición cabal del mismo sino tan solo introducir los elementos que utilizaremos para dar una representación en UML a código C escrito bajo las distintas especificaciones y frameworks que estudiaremos.



#### Clases  
<!--
The UML provides a graphical representation of class, as well, as Figure 4-1 shows. This
notation permits you to visualize an abstraction apart from any specific programming language
and in a way that lets you emphasize the most important parts of an abstraction: its name,
attributes, and operations.
-->
UML provee una representación gráfica de una clase. La siguiente figura nos muestra tal representación.  
![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLiZBpqqgIgtcuamiASfCAYt9v-BCJIt9JydNqEIgvN98pKi1gWK0)\  
<!--
class nombre{
	atributo
	metodo()
}
-->
  
Esta  notación permite visualizar una abstracción, el concepto de clase, independiente de un lenguaje de programación específico. El mismo enfatiza en tres bloques las partes más importantes de la clase: su nombre, sus atributos y sus métodos.


##### Nombres
El nombre de una clase debe ser único en el paquete en el cual es contenido (los paquetes se introducirán luego). Los nombres pueden ser cualquier texto incluyendo letras, números y signos de puntuación (excepto el signo `:`). Esta flexibilidad en el nombre puede no ser soportada por todo lenguaje de programación.


##### Atributos
<!-- fuente BRJ98: An attribute is a named property of a class that describes a range of values that instances of the property may hold-->
El concepto de atributo en UML es equivalente al de cualquier LPOO <!-- pregunta: por lo que no sería mejor ponerlo arriba?-->. Un atributo es una propiedad de una clase que describe un rango de valores que las instancias de tal atributo (contenidas por las instancias de la clase) pueden tener. 


##### Operaciones
Las operaciones son equivalentes a lo que introdujimos como métodos en los LPOO y son el elemento principal para describir el comportamiento de una clase<!-- aclaración: pongo principal porque un state chart también define su comportamiento-->.
<!-- 
fuente:
You can specify other features of an operation, such as marking it polymorphic or constant, or
specifying its visibility, as discussed in Chapter 9.-->



<!--
fuente: Visibility
One of the most important details you can specify for a classifier's attributes and operations is its
visibility. The visibility of a feature specifies whether it can be used by other classifiers. In the
UML, you can specify any of three levels of visibility.

1. public Any outside classifier with visibility to the given classifier can use the feature;
specified by prepending the symbol +
2. Any descendant of the classifier can use the feature; specified by prepending
protected the symbol #
3. private Only the classifier itself can use the feature; specified by prepending the symbol
-->
#### Visibilidad
Los atributos y operaciones en UML poseen una visibilidad. La visibilidad especifica si el elemento puede ser accedido desde otras clases. En UML existen tres niveles de visibilidad:
1. pública: Cualquier clase con visibilidad a esta clase puede acceder al elemento con visibilidad pública.
2. protegida: Cualquier clase que descienda de esta clase la puede acceder.
3. privada: sólo el clasificador mismo puede utilizar el elemento.


<!--
fuente: Alcance
Another important detail you can specify for a classifier's attributes and operations is its owner
scope. The owner scope of a feature specifies whether the feature appears in each instance of
the classifier or whether there is just a single instance of the feature for all instances of the
classifier. In the UML, you can specify two kinds of owner scope.

1. instance Each instance of the classifier holds its own value for the feature.
2. classifier There is just one value of the feature for all instances of the classifier.
-->


#### Alcance
Otra característica importante de los atributos y operaciones es su propietario de alcance (en inglés: owner scope). El mismo indica si el atributo u operación es de clase o de instancia. De instancia significa que el elemento aparece u opera en cada instancia de clase, mientras si es de clase significa que existe un único elemento para todas las instancias de la clase. En el metamodelo de UML esto implica que el atributo `isStatic` sea `true`, lo que significaría de clase, o `false` lo que significaría que es de instancia.  


<!--
fuente: Abstract, Root, Leaf, and Polymorphic Elements

You use generalization relationships to model a lattice of classes, with more-generalized abstractions at the top of the hierarchy and more-specific ones at the bottom. Within these hierarchies, it's common to specify that certain classes are abstract• meaning that they may not have any direct instances. In the UML, you specify that a class is abstract by writing its name in italics.
-->
#### Elementos abstractos, polimórficos y constantes
Se utilizan relaciones de herencia para modelar una jerarquía de clases, con clases más generalizadas en la parte superior de la jerarquía y otras más específicas en la parte inferior. Dentro de estas jerarquías, es común especificar que ciertas clases son abstractas, lo que significa que es posible que no tengan ninguna instancia directa. En el metamodelo de UML esto significa que el atributo `isAbstract` sea `true`.
<!--
fuente: 
Typically, an operation is polymorphic, which means that, in a hierarchy of classes, you can specify operations with the same signature at different points in the hierarchy. Ones in the child classes override the behavior of ones in the parent classes. When a message is dispatched at run time, the operation in the hierarchy that is invoked is chosen polymorphically• that is, a match is determined at run time according to the type of the object. In the UML, you specify an abstract operation by writing its name in italics, just as you do for a class. By contrast, Icon::getID() is a leaf operation, so designated by the property leaf. This means that the operation is not polymorphic and may not be overridden.
**Note**
Abstract operations map to what C++ calls pure virtual operations; leaf operations in
the UML map to C++ nonvirtual operations.
  explicar cada una de estas propiedades de elementos: isLeaf isConst isAbstract
-->
Una operación puede ser polimórfica, lo que significa que, en una jerarquía de clases, puede especificar operaciones con la misma firma en diferentes puntos de la jerarquía. Las implementaciones en las clases hijas anulan el comportamiento en las clases padre. Cuando se envía un mensaje en el tiempo de ejecución, la operación en la jerarquía que se invoca se elige de manera polimórfica; es decir, se determina una coincidencia en el tiempo de ejecución según el tipo de objeto. En UML, puede especificarse que una operación es abstracta (también con el atributo `isAbstract`) lo que significa que no provee una implementación y la misma se posterga a clases hijas, o también puede ser polimórfica especificando una implementación, esto se consigue con el atributo `isLeaf` en `false`. Una operación con el atributo `isLeaf` en `true` significa que no es polimórfica.  
Por último, una operación puede ser constante, esto significa que no modifica al objeto al que se le hace la llamada. Esto se consigue con el atributo `isConst` en `true`.



#### Paquetes


#### Modelando tipos primitivos
<!--
fuente:
Types are discussed in Chapter 11.
At the other extreme, the things you model may be drawn directly from the programming
language you are using to implement a solution. Typically, these abstractions involve primitive
types, such as integers, characters, strings, and even enumeration types, that you might create
yourself.
-->
Los tipos modelados pueden ser traídos directamente del lenguaje de programación usado para implementar la solución, estos tipos son llamados tipos primitivos (en inglés primitive type).

#### Relaciones
<!--
fuente:
When you build abstractions, you'll discover that very few of your classes stand alone. Instead, most of them collaborate with others in a number of ways. Therefore, when you model a system, not only must you identify the things that form the vocabulary of your system, you must also model how these things stand in relation to one another.
-->
Al modelar un sistema ninguna clase queda aislada. Las mismas colaboran entre sí de varias formas. Por lo tanto, no sólo se deben identificar las cosas que forman el vocabulario del sistema (modelado en clases), por el contrario se deben identificar cómo estas clases se relacionan la una con la otra. UML define cuatro tipos de relaciones: Dependencias, asociaciones, generalizaciones y realizaciones.


##### Dependencia
<!--
First, a dependency is a semantic relationship between two things in which a change to on
e thing
(the independent thing) may affect the semantics of the other thing (the dependent thing).
-->
Una dependencia es una relación semántica entre dos cosas en donde un cambio en la semántica de uno (la cosa independiente) puede afectar la semántica de otra cosa (la cosa dependiente). La dependencia se modela con una flecha punteada. El siguiente diagrama muestra una clase B que depende de una clase A.  
<!--
@startuml
class A
class B
A <.. B
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLd24qavSZWesUdf0sY4rBmKe3m00)
\ 


##### Asociación
<!-- fuente:
an association is a structural relationship that describes a set of links, a link being a connection among objects. 
-->
Una asociación es una relación estructural que describe un conjunto de vínculos entre dos clases. Una asociación contiene una multiplicidad (una instancia de la clase A con cuantas instancias de la clase B puede estar vinculada y viceversa). La siguiente figura nos muestra una asociación entre una clase A y otra B donde cada A  se vincula con varios B (representado por el asterisco) y cada B con un solo A (representado por el número uno).  
<!--
@startuml
class A
class B
A "1" -- "*" B
@enduml
#-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLd24qavSZWfAXaeAkheAIbfAGAv3QbuAK1a0)
\ 

Las asociaciones pueden tener nombres en sus extremos que representan el nombre con el que se accede al tipo asociado. Los atributos cuyos tipos son clases pueden ser modelados mediante asociaciones. 


###### Agregación
<!-- fuente:
Aggregation is a special kind of association, representing a structural relationship between a whole and its parts.
-->
Una agregación es un tipo especial de asociación que representa una relación estructural entre el todo y sus partes. 
Una agregación adiciona la semántica "es un" a una asociación.
La agregación es representada por un rombo vacío como en la siguiente figura.

<!--
@startuml
class Compañía
class Empleado
Compañía "*" o-- "*" Empleado
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLd3Epor8FBpnyDf4g81hRa5EQc9AFXT2Ha59IqaXNrSNH2Fa7LBpKe3c0000)
\ 


###### Composición
<!--
fuente: However, there is a variation of simple aggregation• composition• that does add some important
semantics. Composition is a form of aggregation, with strong ownership and coincident lifetime as
part of the whole. Parts with non-fixed multiplicity may be created after the composite itself, but
once created they live and die with it. Such parts can also be explicitly removed before the death
of the composite.
-->
Una composición es un tipo de agregación pero que agrega otra semántica a la asociación. Expresa quién es el poseedor de la parte y que el tiempo de vida de la parte está contenido dentro del tiempo de vida del todo (la parte puede ser instanciada junto con el todo o después y es destruida junto al todo o antes). La composición  es representada con un rombo lleno como en la siguiente figura  
<!--
@startuml
class Persona
class Ojo
Persona "1" *-- "2" Ojo
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLWX8BIhEpqk4ylopyhcW8WfAXaeAMhgw2af6Ic1nXzIy5A0-0000)
\ 


##### Generalización
<!--
fuente: a generalization is a specialization/generalization relationship in which objects of the specialized element (the child) are substitutable for objects of the generalized element (the parent). In this way, the child shares the structure and the behavior of the parent.
-->
Una generalización es una relación de generalización o especialización en la que los objetos del elemento especializado (el hijo o child en inglés) son sustituibles por los objetos del elemento generalizado (el padre o parent en inglés). De esta manera, la clase hija comparte la estructura y el comportamiento de la clase padre.  
El siguiente diagrama nos muestra a la izquierda un ejemplo de herencia simple y a la derecha un ejemplo de herencia múltiple.

<!--
@startuml
class Padre1
class Hijo1
Padre1 <|-- Hijo1

class Padre2
class Padre3
class Hijo2
Padre2 <|-- Hijo2
Padre3 <|-- Hijo2
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLWX8J2bADOHoF3ApyWsv848ADZMwkb0HPBL6o1nZ98r6N11vg4Q4Y36oY4DgNWhGTW00)
\ 


##### Realización
Una realización es una relación semántica entre interfaces y clases, en donde una interfaz especifica un contrato que otra clase garantiza realizar. Una interfaz se modela igual que una clase pero con otro estereotipo (los estereotipos son explicados en la sección siguiente). La realización es modelada como una generalización con líneas punteadas, el siguiente diagrama nos muestra un ejemplo de ello.  
<!--
@startuml
class clase
interface interfaz
interfaz <|.. clase
@enduml
-->
![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLW2HgLoPUIMfHMc9oQaAK5OL5uoXO5EZfmTLvf2QbmBK2W00)
\ 


<!--
fuente:
Stereotypes
These four basic elements of the UML are discussed in Chapter 2.
The UML provides a language for structural things, behavioral things, grouping things, and
notational things. These four basic kinds of things address the overwhelming majority of the
systems you'll need to model. However, sometimes you'll want to introduce new things that speak
the vocabulary of your domain and look like primitive building blocks.
-->
#### Estereotipos
UML es un lenguaje de modelado general que busca poder describir la mayoría de los sistemas que uno debe modelar. A veces, es necesario introducir nuevas cosas que hacen parte del vocabulario del dominio del problema o de la solución o implementación y se ven como bloques primitivos de construcción (como las clases, paquetes o relaciones). Los estereotipos son aplicados a un elemento ya existente de UML cambiando o agregando significado a dicho elemento cuando se le asocia con el estereotipo.
La siguiente figura nos muestra como se ve un elemento clase cuando se le agrega un estereotipo.
<!--
@startuml
class Metaclase <<metaclass>>
@enduml
-->

![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLl1DBKa4iL8LR6noeUnYEpikXzIy5A0g0000)\


El estereotipo `metaclass` es aplicado al elemento clase y con el mismo puede describirse metaclases como `Metaclase` en el ejemplo.


<!--
fuente: Tagged Values
Every thing in the UML has its own set of properties: classes have names, attributes, and
operations; associations have names and two or more ends (each with its own properties); and
so on. With stereotypes, you can add new things to the UML; with tagged values, you can add
new properties
-->
##### Valores Etiquetados
Al igual que los elementos de UML tienen asociados distintas propiedades, por ejemplo, las clases tienen asociadas un nombre. Así también los estereotipos pueden tener asociados nuevas propiedades definidas para esos estereotipos. 


#### Interfaces
Hay distintas formas de representar una interfaz en UML. La forma que utilizaremos en esta tesis es a través de un estereotipo aplicado a una clase.


<!--
### Class Diagram
A class diagram shows a set of classes, interfaces, and collaborations and their relationships.
Class diagrams are the most common diagram found in modeling object-oriented systems. You use class diagrams to illustrate the static design view of a system. 

Class diagrams are important not only for visualizing, specifying, and documenting structural models, but also for constructing executable systems through forward and reverse engineering.
-->
#### Diagramas de clase
Un diagrama de clase muestra un conjunto de clases, interfaces, paquetes y sus relaciones.
Los diagramas de clase son los diagramas más comunes en el modelado de sistemas orientados a objetos. Los diagramas de clase son utilizados para ilustrar la vista de diseño estático de un sistema.

Los diagramas de clase son importantes no solo para visualizar, especificar y documentar modelos estructurales, sino también para construir sistemas ejecutables mediante ingeniería directa e inversa. [@BRJ98]

<!--
#### Getting Started
When you build a house, you start with a vocabulary that includes basic building blocks, such as
walls, floors, windows, doors, ceilings, and joists. These things are largely structural (walls have
height, width, and thickness), but they're also somewhat behavioral (different kinds of walls can
support different loads, doors open and close, there are constraints on the span of a unsupported
floor). In fact, you can't consider these structural and behavioral features independently. Rather,
when you build your house, you must consider how they interact. The process of architecting your
house thus involves assembling these things in a unique and pleasing manner intended to satisfy
all your functional and nonfunctional requirements. The blueprints you create to visualize your
house and to specify its details to your contractors for construction are, in effect, graphical
presentations of these things and their relationships.Building software has much the same characteristics except that, given the fluidity of software, you have the ability to define your own basic building blocks from scratch. With the UML, you use class diagrams to visualize the static aspects of these building blocks and their relationships and to specify their details for construction, as you can see in Figure 8-1.

### Forward and Reverse Engineering
The importance ofmodeling is discussed in Chapter 1.
Modeling is important, but you have to remember that the primary product of a development team
is software, not diagrams. Of course, the reason you create models is to predictably deliver at the
right time the right software that satisfies the evolving goals of its users and the business. For this
reason, it's important that the models you create and the implementations you deploy map to one
another and do so in a way that minimizes or even eliminates the cost of keeping your models
and your implementation in sync with one another.

The UML does not specify a particular mapping to any object-oriented programming language, but the UML was designed with such mappings in mind. This is especially true for class diagrams, whose contents have a clear mapping to all the industrial-strength object-oriented languages, such as Java, C++, Smalltalk,
Eiffel, Ada, ObjectPascal, and Forte. The UML was also designed to map to a variety of commercial object-based languages, such as Visual Basic.
-->
### Ingeniería directa y diagramas de clase
UML no especifica un mapeo particular a ningún lenguaje de programación orientado a objetos, pero UML fue diseñado con tales mapeos en mente. Esto es especialmente cierto para los diagramas de clase, cuyos contenidos tienen una asignación clara a todos los lenguajes orientados a objetos de uso industrial, como Java, C ++, Smalltalk, Eiffel, Ada, Object Pascal y Forte [@BRJ98].  

<!--
Forward engineering is the process of transforming a model into code through a mapping to an
implementation language. Forward engineering results in a loss of information, because models
written in the UML are semantically richer than any current object-oriented programming
language. In fact, this is a major reason why you need models in addition to code. Structural
features, such as collaborations, and behavioral features, such as interactions, can be visualized
clearly in the UML, but not so clearly from raw code.
To forward engineer a class diagram,
• Identify the rules for mapping to your implementation language or languages of choice.
This is something you'll want to do for your project or your organization as a whole.
• Depending on the semantics of the languages you choose, you may have to constrain
your use of certain UML features. For example, the UML permits you to model multiple
inheritance, but Smalltalk permits only single inheritance. You can either choose to
prohibit developers from modeling with multiple inheritance (which makes your models
language-dependent) or develop idioms that transform these richer features into the
implementation language (which makes the mapping more complex).
• Use tagged values to specify your target language. You can do this at the level of
individual classes if you need precise control. You can also do so at a higher level, such
as with collaborations or packages.
• Use tools to forward engineer your models.
-->
La ingeniería directa es el proceso de transformación de un modelo en código a través de un mapeo a un lenguaje de implementación.
Para generar código a partir de un diagrama de clase los autores  de UML dan las siguientes directivas:  
• Identifique las reglas para mapear su idioma de implementación con UML. Esto es algo que querrá hacer por su proyecto o su organización en general.  
• Dependiendo de la semántica de los lenguajes que elija, es posible que deba restringir el uso de ciertas funciones UML. Por ejemplo, UML le permite modelar herencia múltiple, pero JAVA permite sólo herencia simple. Puede elegir restringir a los desarrolladores el modelado de la herencia múltiple (lo que hace que sus modelos dependan del lenguaje) o desarrolle modismos que transformen estas características en el lenguaje de implementación (lo que hace que el mapeo sea más complejo).  
• Utilice valores etiquetados para especificar su idioma de destino. Puede hacerlo a nivel de clases individuales si necesita un control preciso. También puede hacerlo en un nivel superior, como en los paquetes.  
• Usa herramientas de ingeniería directa para los modelos.  

En esta tesis buscaremos estudiar si es posible realizar ingeniería directa en C valiéndonos de frameworks o especificaciones para codificar C OO para que el mapeo entre UML y el código sea de forma intuitiva. Dependiendo el framework que utilicemos este mapeo puede ser más o  menos complejo, pero minimizaremos su complejidad mapeando sólo a la semántica soportada por el framework. 


<!--
**Reverse engineering** is the process of transforming code into a model through a mapping from a
specific implementation language. Reverse engineering results in a flood of information, some of
which is at a lower level of detail than you'll need to build useful models. At the same time,
reverse engineering is incomplete. There is a loss of information when forward engineering
models into code, and so you can't completely recreate a model from code unless your tools
encode information in the source comments that goes beyond the semantics of the
implementation language.



garbage collecting A system that frees unused memory in the running
program. This frees the developer of handling memory management
manually.
built-in operator overloading The ability to override the meaning of
existing operators such as ”+” when used in the context of certain
objects.
runtime introspection The ability to introspect an object at runtime. This
enables the developer to query the object and ask for runtime type
information (RTI or RTTI). E.g. what type are you? What is your
name? What is your parent in the inheritance tree?
reflection Reflection is defined by [MIKC92] as ”the ability of an executing
system of programmed objects to make attributes of the objects to be
themselves the subject of computation”. Both Java and .Net are said to
have reflection. We make a distinction between two levels of reflection.
The first one is as we know it in Java: there is runtime introspection and
byte-code introspection. The second one can be summarized as a system
where runtime introspection, byte-code introspection and runtime
byte-code generation is available such as in the .Net platform. A level 0
of reflection may be regarded as runtime introspection.
[@Hendrickx2004]

[Genericidad](https://en.wikipedia.org/wiki/Generic_function)

#### Escencia
La orientación a objetos es más que una forma de programar, es una forma de pensar, es un paradigma. [@Hendrickx2004]
Las virtudes de la programación orientada a objetos son por demás conocidas: modularidad,  flexibilidad,  extensibilidad, reutilización de código. La orientación a objetos ha sido la aplicación de los buenos principios de programación enseñados con anterioridad a su existencia [@Douglass2010]
Up till now we have spoken about OO in different forms. Indeed, OO
compromises different aspects, like the things we have mentioned before:
programming, analysis and design. However, what is important is the
paradigm surrounding OO and all its different aspects.
When dealing with those different aspects, it is important to know that all
those aspects are actually loosely coupled. Nevertheless they all rely on the
same paradigm. This can be summarized as:
• A design can be Object Oriented, even if the resulting program isn’t.
[Mad88]
• A program can be Object Oriented, even if the language it is written in
isn’t. [Mad88]
• An Object Oriented program can be written in almost any language, but
a language can’t be associated with object oriented-ness unless it
promotes Object Oriented programs. [Str91]
Albeit, in this thesis, Object Oriented Programming does not mean using an
Object Oriented Programming Language (OOPL), but means implementing
an Object Oriented Design into a suitable programming language of choice,
which should result in an OO program.
Object Oriented Programming Languages provide mechanisms to support
the Object Oriented paradigm and programming style. Such an OOPL
simplifies the job of implementing an OO design, but an OOPL is not a must.
Implementing an OO design can be done in virtually any language. Such an
language may be C [Som96], but that does not mean C is an OOPL: it just enables the developer to write OO programs. [Str91]. (Likewise, using an
OOPL does not mean you are thinking within the OO paradigm).
Bjarne Strourstrup his opinion is of very high interest as Bjarne Strourstrup
goes on saying that a language enabling you to write OO programs, should not
be considered if it takes an exceptional amount of effort to do it [Str91]. We
regard this opinion as rather naive, as a language is only a mere tool in the
tool-suite: responsibilities of the tool-suite can be divided among the tools, as
long the end result is an usable environment.
Indeed, in the article ”The Silver bullet” [Jr.87] the author of the document
is searching for the silver bullet that is needed in software engineering. He
makes a difference in the essential and the accidental mistakes. The essential
ones are inherent to the problem in software engineering: fashioning complex
conceptual invisible constructs which are hard to understand and grasp.
Accidental mistakes are related to ”non-inherent problems” such as problems
regarding programming mistakes, environments providing little comfort, etc.
In his search for the silver bullet he asserts that nor yet another high level
language nor tool-support are candidates for the silver. However, he takes
more hope in Object Orientation, the paradigm, than in any other thing as a
possible silver bullet: High level languages and tool-support both only solve
accidental mistakes, not the essential ones. [@Hendrickx2004]

## Summary
Object Orientation is actually a name compromising different aspects, all
relying on the concepts that have sum up earlier.
With the definition of OO Programming in mind (I.e. implementing an OO
design into a language) it are thus those concepts that needs support in a
development environment.
A development environment is a tool-suite that only helps the developer in
removing the accidental features and thus providing a pleasant environment,
independent in how the responsibilities are divided among the elements of such
a tool-suite. Such an element is the programming language. This language
may or may not be an OOPL: for OO an OOPL is not a must, and C may be
a candidate target language for implementing an OO design. [@Hendrickx2004] 
-->


<!--stackedit_data:
eyJoaXN0b3J5IjpbMTEyNjE5NjYxNCwxNzE3NzI2MjM3LDE1MT
I5NDE0MjEsLTY5NzU1MjM4MiwtMTI5MDI2NTQxNSwxMjE5MjIz
Mzk5LDg0OTg5NDMxMCwxNzAxNzI4MDY1LDE0NDA4Mjg1NzcsMT
U1NjkyOTI1NCwxNDI5MjY2MjI2LDE3NDQ1NjU5OCwtNDQyMjA2
MDI0LDE4MDM2MzMzNDQsMTc0MzcxNDY0LDIwMDQ2NzgwMTEsMT
k3OTQ0OTAwNCw1NzI2NDQxMTYsMTczNTQ1OTc5MSwtMTQ4MTgw
ODcyXX0=
-->
