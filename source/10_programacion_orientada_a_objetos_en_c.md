
# Programación orientada a Objetos en C


## El uso de C en el siglo XXI
A pesar de su antigüedad, C es ampliamente utilizado, tanto como lenguaje de iniciación a la programación como para  producción de sistemas operativos, sistemas restringidos o con alta demanda de eficiencia y sistemas embebidos.
La popularidad de este lenguaje puede medirse en su trayectoria en el índice TIOBE [@tiobe] (al momento de la escritura de esta tesis en el segundo lugar) y aún más desproporcionada es su preponderancia en los sistemas embebidos: es el lenguaje de programación principal para aproximadamente el 70% de los diseñadores de firmware. Una revisión longitudinal de las encuestas de la industria que abarcan desde 2005 hasta 2018 muestra que C no sólo fue de manera holgada el idioma más utilizado, sino que en realidad aumentó su participación de mercado del 50% al 70% aproximadamente en esos años. Dentro de la comunidad de sistemas embebidos, parece que el año pico para C++ (segundo lenguaje utilizado) fue 2006 [@Barrs2018].


## La portabilidad y eficiencia del código escrito en lenguaje C
 “C tiene las ventajas de una gran disponibilidad de compiladores para una gran variedad de procesadores y una bien merecida reputación de eficiencia en tiempo de ejecución.”[@Douglass2010].
 
Más allá de estar trabajando para una arquitectura que disponga de compiladores o intérpretes para otros lenguajes, al elegir desarrollar en lenguaje C el código generado será portable a otras arquitecturas que podrían no disponer de otros compiladores o intérpretes. Claro que, si el código escrito hace referencia a la plataforma o entorno, ese mismo código no será portable a otras plataformas, pero el código que no lo hace puede ser compilado con cualquier compilador C para otra plataforma.  
Otro punto en la potabilidad es que las librerías en C pueden ser ejecutadas por una gran variedad de otros lenguajes  (mediante una interfaz de función exterior (FFI) ).  
Otra aclaración necesaria es que existen distintos estándares ISO de C (a saber ISO C89, ISO C99 e ISO C11), los más modernos pueden todavía no ser soportados por todas las plataformas mientras que el ISO C89 es altamente conocido por su disponibilidad y portabilidad [@Deniau2009] 
Por supuesto que este soporte del lenguaje por parte de tantas plataformas se debe a la simpleza y popularidad del mismo.  
Siendo así, programar orientado a objetos en lenguaje C puede ser más conveniente que hacerlo bajo otros lenguajes aunque nativamente soporten este paradigma.


## Programación orientada a objetos en C
Bajo el paradigma de orientación a objetos se han creado distintos lenguajes como ser C++, Java, SmallTalk, CLOS y muchos más. En cambio el lenguaje C  fue concebido bajo el paradigma de programación estructurada.  
<!--
fuente: “La programación estructurada es una disciplina de desarrollo de software que enfatiza dos aspectos separados y distintos datos y funciones [...] La programación orientada a objetos está basada en un paradigma ortogonal[...]”. Los ladrillos de construcción en la orientación a objetos son las clases. “Una clase combina juntas tanto datos (guardados en atributos) y procedimientos (conocidos como operaciones) que operan en los datos, pero ellos tienden a ser inherentemente fuertemente relacionados de alguna forma.”  [@Douglass2010]
-->
La programación estructurada no contiene ninguno de los conceptos de la programación orientada a objetos que hemos visto.
<!--aca-->
<!--
fuente: 
“Sin embargo una interesante pregunta surge: ¿Puede la programación orientada a objetos ser expresada en un lenguaje estructurado, como el C? e incluso que pueda ser hecha, ¿debe ser hecha? Recuerdo una discusión similar en el pasado cuando los lenguajes más comunes eran los lenguajes de ensamblador. ¿Será posible escribir programas estructurados en código assembler? Después de todo no proveen intrínsecamente las características de un lenguaje estructurado. La respuesta es claramente: Si![...] Lo mismo es verdad con programación orientada a objetos en lenguajes estructurados como C.” [@Douglass2010] 
-->
Sin embargo, la siguiente pregunta surge: ¿puede programarse orientado a objetos con un lenguaje estructurado? Douglass compara esto a cuando antiguamente se hacían la misma pregunta con respecto al lenguaje ensamblador, ¿podría ser utilizado para programar en forma estructurada? La respuesta a ambas preguntas es sí, tan sólo se debe buscar la forma de implementar los conceptos de cada paradigma en el lenguaje deseado  [@Douglass2010].


La orientación a objetos es más que una forma de programar, es una forma de pensar, es un paradigma. 
<!--
fuente:
When OO is viewed purely as a way of thinking,
we find ourselves on the level of program analysis and design. The 5 latter
concepts are not important on this level. What remains are the notion of
classes and the relation between them. In this way OO becomes primarily
what is known as Object Oriented Analysis and Design (OOA&D).
-->
Cuando la POO es vista como un enfoque, una forma de pensar, entonces estamos hablando del análisis y diseño del software. De esta manera la POO se convierte principalmente en lo que es conocido como Análisis y Diseño orientado a objetos (OOA&D por sus siglas en inglés). [@Hendrickx2004]

<!--
fuente:
what is important is the paradigm surrounding OO and all its different aspects.
When dealing with those different aspects, it is important to know that all
those aspects are actually loosely coupled. Nevertheless they all rely on the
same paradigm. This can be summarized as:
• A design can be Object Oriented, even if the resulting program isn’t.
[Mad88]
• A program can be Object Oriented, even if the language it is written in
isn’t. [Mad88]
• An Object Oriented program can be written in almost any language, but
a language can’t be associated with object oriented-ness unless it
promotes Object Oriented programs. [Str91]
-->
Cada uno de estos son parte de los diferentes aspectos que posee la OO: análisis, diseño, programación, etc. Los distintos aspectos de la OO pueden ser independientes entre sí.
Esto significa:  
1. Un diseño puede ser Orientado a Objetos, incluso si el programa resultante no lo es [@Mad88].  
2. Un programa puede ser Orientado a Objetos, incluso si el lenguaje en el que está escrito no lo es [@Mad88].  
3. Un programa Orientado a Objetos puede ser escrito en casi cualquier lenguaje, pero un lenguaje no puede ser asociado con la orientación a objetos amenos que promueva programas Orientados a Objetos [@Str91].


<!--
Albeit, in this thesis, Object Oriented Programming does not mean using an
Object Oriented Programming Language (OOPL), but means implementing
an Object Oriented Design into a suitable programming language of choice,
which should result in an OO program.
-->
En esta tesis la Programación Orientada a Objetos (POO) no significa usar un Lenguaje de Programación orientado a objetos (LPOO), sino que significa implementar un Diseño Orientado a Objetos en el lenguaje de programación C, lo que resulta en un programa Orientado a Objetos(OO). 
 
 
<!--
Object Oriented Programming Languages provide mechanisms to support
the Object Oriented paradigm and programming style. Such an OOPL
simplifies the job of implementing an OO design, but an OOPL is not a must.
Implementing an OO design can be done in virtually any language. Such an
language may be C [Som96], but that does not mean C is an OOPL: it just enables the developer to write OO programs. [Str91].(Likewise, using an OOPL does not mean you are thinking within the OO paradigm).
-->
Los LPOO proveen mecanismos para soportar los conceptos de la POO y un estilo de programación en consonancia con los mismos. Tales LPOO simplifican el trabajo de implementar un diseño OO, pero un LPOO no es obligatorio para tales diseños.
<!-- a partir de aca podría pasar para el capítulo que viene-->
Implementar un diseño OO puede ser hecho en prácticamente cualquier lenguaje. Tal lenguaje puede ser C [@Som96], pero no significa que C sea un LPOO, tan solo permite al programador escribir programas OO [@Str91].
<!--
fuente:
Bjarne Strourstrup his opinion is of very high interest as Bjarne Strourstrup goes on saying that a language enabling you to write OO programs, should not be considered if it takes an exceptional amount of effort to do it [Str91]. We
regard this opinion as rather naive, as a language is only a mere tool in the tool-suite: responsibilities of the tool-suite can be divided among the tools, as long the end result is an usable environment.
-->
<!-- a partir de aca Fede no corrigió-->
La opinión de Bjarne Stroustrup es la más popular afirmando que un lenguaje que permita escribir programas OO no debería ser considerado para tal función si se requiere de un esfuerzo excepcional para hacerlo [@Str91]. Esta opinión es un tanto injusta, ya que un lenguaje es solo una herramienta en el conjunto de las que pueden utilizarse para crear programas OO. Las dificultades que presenta el lenguaje para expresarse en una manera OO puede ser suplida por una o varias herramientas [@Hendrickx2004].  
<!--
fuera:
Indeed, in the article ”The Silver bullet” [Jr.87] the author of the document
is searching for the silver bullet that is needed in software engineering. He
makes a difference in the essential and the accidental mistakes. The essential
ones are inherent to the problem in software engineering: fashioning complex
conceptual invisible constructs which are hard to understand and grasp.
Accidental mistakes are related to ”non-inherent problems” such as problems
regarding programming mistakes, environments providing little comfort, etc.
In his search for the silver bullet he asserts that nor yet another high level
language nor tool-support are candidates for the silver. However, he takes
more hope in Object Orientation, the paradigm, than in any other thing as a
possible silver bullet: High level languages and tool-support both only solve
accidental mistakes, not the essential ones. [@Hendrickx2004]
-->


La OO en C es ampliamente utilizada hoy día, Bruce Powel Douglass escribió un libro de Patrones de Diseño que utilizan la orientación a objetos para sistemas embebidos en C [@Douglass2010]. 


## Dificultades en la programación orientada a objetos en lenguaje C
La codificación disciplinada de C orientada a objetos es muy propensa a errores. Por ejemplo, la forma más utilizada de conseguir polimorfismo es a través de punteros a función (la especialización puede ser fácilmente hecha con declaraciones switch case pero es mucho más versátil si, en cambio, se utilizan punteros a función) [@Douglass2010]. “La desventaja es que los punteros a función son engañosos y los punteros son la causa principal de los defectos introducidos por el programador en programas en C” [@Douglass2010]. Cada función debe ser referenciada por el puntero correcto y a veces se debe inicializar los punteros en una lista, por lo que un cambio de orden puede ser fatal. También, la cantidad de código que se debe escribir para que una clase en C contenga estos conceptos es significativa [@Axel].
Si bien esto ha llevado a mal juzgar la capacidad de C como un lenguaje alternativo para el paradigma de orientación a objetos, no se debe juzgar al lenguaje en aislamiento sino a todo el toolsuite de desarrollo [@Hendrickx2004].  
En el siguiente capítulo veremos en detalle las dificultades que se presentan para codificar COO de acuerdo a distintos frameworks, luego presentaremos un generador de código desde diagramas de clase UML como facilitador para dicha codificación.  



* Debido a la preponderancia de C en los sistemas embebidos buscamos mostrar el interés especial que tienen herramientas de este tipo para los mismos.*

## La necesidad de diseño en los sistemas embebidos
“Actualmente, la creciente complejidad del proceso de desarrollo de software embebido, demanda técnicas de diseño capaces de manejar tal complejidad eficientemente” [@Lennis] Esta eficiencia es crítica para reducir el costo de desarrollo ya que “cerca del 80% del costo en el desarrollo de sistemas embebidos es atribuido a cuestiones de diseño”[@ITRS_Design].  
Debemos preguntarnos cuál es el motivo de tales esfuerzos en el diseño. Para eso debemos analizar las características intrínsecas de los sistemas embebidos que hacen del diseño algo esencial.  
“Una de las más notables características de los sistemas embebidos es la enormidad de sus restricciones.\[...\]  recursos como la memoria, alimentación, refrigeración, o poder computacional contribuyen al costo de cada unidad”  [@Douglass2010]. “Las presiones para agregar capacidades \[al sistema\] mientras simultáneamente reducir los costos implica que los desarrolladores de embebidos deben continuamente buscar maneras de mejorar sus diseños, y su habilidad de diseñar eficientemente”[@Douglass2010].
Si deseamos realizar cambios, extendiendo el sistema o bajando su consumo de recursos, lo recomendable es tener un diseño con bajo impacto a los cambios y al mismo tiempo eficiente. 
También, distintos artefactos de software deben correr en distintas plataformas. Esto lo hemos visto “con la caída de los costos de hardware, sistemas originalmente hechos en pequeños procesadores de 8 bit se han actualizado a procesadores de 16 ó 32 bit más capaces. Esto es necesario para agregar la complejidad adicional de comportamiento requerido en los dispositivos embebidos modernos y se hace posible por los bajos costos recurrentes de partes y manufactura” [@Douglass2010].  
Un tercer punto a tener en cuenta es que “muchos proyectos de sistemas embebidos involucran el desarrollo de hardware electrónico y mecánico simultáneamente con el software. Esto introduce un desafío especial para el desarrollador que debe elaborar el software solamente sobre la base de especificaciones de prediseño, de cómo el hardware debería trabajar. Muy frecuentemente, esas especificaciones son nada más que nociones de funcionalidad, haciendo imposible crear el software correcto hasta que el hardware es entregado.” [@Douglass2010].
Estos problemas pueden ser atacados eficientemente con un diseño orientado a objetos. Una inversión en la cadena de dependencias posibilita que la implementación del negocio a resolver sea independiente del hardware. A la vez, identificar por dónde vendrán los cambios en las capacidades del sistema o sus optimizaciones, junto con un buen diseño orientado a objetos permite que el impacto en el código original por los cambios sea minimizado, reduciendo los costos de dichos cambios.  
Además de esto, para tener un testing efectivo se debe pensar en un diseño orientado a las pruebas. Bruce Powel Douglass, gurú de diseño y procesos para sistemas embebidos, recomienda utilizar TDD (desarrollo dirigido por las pruebas)  [@Douglass2010]. James W. Grenning es autor de “Test Driven Development for Embedded C”, un libro dedicado a este asunto para sistemas embebidos. Él nos escribe: “El componente central de TDD son las pruebas unitarias, sin un diseño modular que permita descomponer el sistemas en partes no sería esto posible” [@Grenning]. La modularidad es un elemento básico en la orientación a objetos contenida en el concepto de encapsulamiento.  
Toda propuesta de cómo implementar características de orientación a objetos en C embebido debe seguir la normativa de no hacerlo a costa de un obligado incremento en el costo de hardware. Al mismo tiempo, para sistemas complejos y menos restringidos se debería permitir contar con más facilidades para el desarrollador para que haga frente a tal complejidad con un menor costo de diseño, de ser necesario cediendo algo de eficiencia.  
Cualquier avance en los procesos de desarrollo de software embebido en C implicará un importante impacto en la mayoría de los desarrollos en  sistemas embebidos actuales.


## Definiciones en COO
A continuación presentaremos algunas definiciones para formar un lenguaje en común que nos permita describir las distintos frameworks de COO de forma más sencilla.


### Herencia de estructuras
Ya definimos de que se trata la herencia de clases en la POO. El mismo concepto podemos aplicarlo a las estructuras en C y es la base en todas las implementaciones para la herencia de clases en COO.
Uno puede declarar una estructura A y luego definir funciones que reciban en sus argumentos a dicha estructura. Si esta estructura es incluida dentro de otra estructura B como primer variable de la estructura, entonces todas las funciones escritas para la estructura A servirán también para la estructura B. Es cierto que el compilador de C puede advertirnos de estar usando una estructura B en vez de una estructura A, pero si los argumentos son referencias un simple casteo de una referencia de B a una de A puede solucionarlo.
```C
struct A{
	char * name;
};
struct B{
	struct A a;
	int age;
};
void doSomethig(struct A * a);
/*...*/
struct B b;
doSomethig((struct A*) &b);
```
La contra de esta técnica es que para acceder a un dato en A desde una estructura B hay que utilizar un nombre más de referencia. Por ejemplo para acceder a la variable `name` en `b` se debe escribir `b.a.name`, y si seguimos  agregando más capas de herencia esto se vuelve muy molesto para el programador. Hay distintas maneras de mitigar este problema, por ejemplo codificando macros o funciones que  manipulen dichos datos a partir de referencias a las estructuras.  
En el capítulo siguiente veremos otras dos formas de resolver esto una utilizando estructuras anónimas de ISO C11 y la otra utilizando macros de listas de los miembros de las estructuras. 


### Estructura de instancia de clase
Instanciar una clase significa poder tener una representación en memoria de un posible estado de una clase, o sea que sus atributos de instancia tengan algún valor permitido por la clase. Si deseamos poder realizar esto en lenguaje C deberemos tener una estructura que contenga todos los atributos de la clase y que instanciar dicha estructura sea uno de los requisitos para obtener una instancia de dicha clase (luego para finalizar la instanciación  se deberá llamar a una función que haga de constructor de la misma). A dicha estructura la llamaremos **estructura de instancia de clase**.   
Esta estructura puede no tener el mismo nombre de la clase y es posible que la estructura de RTTI (ver a continuación) sea la que tiene dicho nombre.


### Tabla virtual
Como ya vimos, el concepto de polimorfismo implica que bajo el mismo mensaje enviado a un objeto o, lo que es lo mismo, instancia de clase, se ejecuta una función dependiente del tipo de clase de la cual pertenece el objeto. La manera más utilizada para implementar esto es que junto con las variables de instancia en la estructura de instancia de clase, haya una referencia que permita referenciar a una tabla de punteros a función. Cada clase, entonces, proveerá su propia tabla y en el lugar donde una asigna cierta función para ser llamada ante cierto mensaje, otra podrá asignar otra función. Dicha tabla es llamada **tabla virtual**.  
En el siguiente capítulo veremos distintas implementaciones de tablas virtuales que poseen distintas características.  
Además introduciremos 2 frameworks (Dynace y COS) que utilizan otro enfoque para el soporte del polimorfismo, la de las funciones genéricas que introducimos en el capítulo anterior. Bajo este enfoque, la función encargada de recibir el mensaje al objeto (el despachador) es la que contiene las referencias a función a ser ejecutadas para cada tipo de clase. Esto se consigue registrando cada función de implementación en el dispatcher.


### Despachador o Selector
En general, existe una función que representa a cada mensaje que puede ser aplicado a todos o algunos objetos. La misma es encargada de ejecutar la función correspondiente para el tipo de clase del objeto. Esta función es llamada **despachador** o **selector** (o en inglés **dispatcher** o **selector**).


### Estructura de RTTI
Es común que en los lenguajes orientados a objetos exista una referencia a la clase misma a la cual se la puede interrogar para obtener su nombre y miembros y estructura. En C esta referencia será a la instancia de una estructura que pueda contener dicha información. Llamaremos a dicha estructura **estructura de RTTI**.

