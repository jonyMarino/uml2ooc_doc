# Estado del arte en programación orientada a objetos en C

## Variedad de frameworks y especificaciones {#frameworks}
<!--
 An OOPL knows the concepts of OO and provides for those mechanisms to ease the implementation of an OO design. Although C does not know the concepts of OO, Object Oriented Programming in C is possible. It is been said that C enables the technique of OO programming.[Str91]
 -->
Un LPOO conoce conceptos del paradigma de OO y provee mecanismos para facilitar el desarrollo en base a esos conceptos. A pesar de que C no conoce los conceptos del paradigma de OO de por sí, de todas formas es posible facilitar el desarrollo con sus conceptos.[@Str91]
<!-- 
This is because of two things. First, due its nature, C is quite a ﬂexible programming language. Secondly, the constructs behind the mechanisms of an OOPL, are in fact quite often simple and straightforward constructs that can be applied in a language such as C.
Of course diﬀerent approaches to such constructs are possible. [@Hendrickx2004].
-->
Esto se debe a dos factores. El primero es que C es un lenguaje de programación muy flexible. Y, en segundo lugar, los modelos de objetos  que permiten soportar los conceptos de orientación a objetos son perfectamente implementables en C.
En esta sección veremos como se ha especificado la codificación en C bajo ese paradigma por distintos autores, los distintos conceptos que han podido ser implementados por cada uno, el modelo de objetos que los contiene.  
<!-- POSIBILIDAD
Resaltaremos algunos problemas que presentan en eficiencia de uso de RAM o tiempo de ejecución para su uso en sistemas embebidos y de ser posible propondremos algunas mejoras.  
-->  
Por último remarcaremos qué dificultades presenta cada una de estas especificaciones para el programador C.  Y propondremos una representación en UML para dicha codificación libre de esas dificultades.
Las especificaciones que analizaremos son:  
1. La de Ben Klemens en su libro 21st Century C [@Klemens2013]  
2. SOOPC  de Miro Samek [@Miro2015]  
3. OOPC de Laurent Deniau [@OOPC]  
4. OOC de Tibor Miseta [@Tibor2017]  
5. OOC-S de Laurent Deniau [@OOCS]  
6. OOC de Axel Tobias Schreiner [@Schreiner93]  
7. GObject de glib [@GObject]  
8. Dynace de Blake McBride [@Blake2004]  
9. COS  de Laurent Deniau [@Deniau2009].  


Para cada especificación enumeraremos los conceptos de orientación a objetos soportado. Esto es importante, como dijimos en el resumen, para analizar a C como un lenguaje apropiado para la enseñanza de la orientación a objetos. Un único lenguaje, simple en sus reglas y muchas veces el primero introducido a los estudiantes, que bajo algunos de estos frameworks se incorporan los conceptos para su enseñanza. Al ser frameworks de código abierto, los mecanismos por los cuales se logra tal soporte de conceptos quedan al descubierto, mostrando así los costos de dicho soporte, en forma quizás aproximada, para cualquier otro LPOO.  
Quien desee obtener una mejor introducción del framework para poder codificar bajo el mismo, se recomienda buscar su documentación en las referencias dadas para el mismo.  


## Modelos de objetos
Los modelos de objetos nos muestran como están organizados los datos que dan soporte a la implementación de los conceptos de orientación a objetos de cada framework.  
Para los frameworks más adecuados para sistemas altamente restringidos hemos incluido un análisis detallado de las estructuras que dan soporte al framework. Esto permite evaluar al framework en su uso de memoria (RAM o ROM) y uso de tiempo de CPU (que depende de la cantidad de indirecciones existentes para llegar a los datos). 
Se han representado, con explicaciones, en diagramas de clase UML. 


### Cómo leer los modelos de objetos

A la hora de elegir los nombres para las estructuras, variables y referencias, para facilitar la introducción al framework, se eligieron los mismos que se utilizan en su codificación en C.  
Algunas diferencias con un diagrama de clases UML de acuerdo al estándar son:  

* Cada estructura o unión en C es representada por una clase en el diagrama. Las uniones con una U y las estructuras con una S.
<!--
@startuml
class estructura << (S,orchid) >>
class union << (U,green) >>
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLaWjBYagJIufBKfKiB5Hq0ZMoIzApiXCqLImiuEgACtBpCy3IuRgf1UbfkQ1vHnIyrA0fW00){width="40%"}
\ 

* El nombre de una estructura en C puede tener uno o varios alias, que son representados en el diagrama UML por una lista separada por comas en el nombre de la clase.  
<!--
@startuml
class estructura as "estructura, alias"<< (S,orchid) >>
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLaWjBYagJIufBKfKI2nMK49mTHGISp8JYvLiR1GqWdNoYv8pCbCq5Upik1nIyrA0-W40){width="40%"}
\ 

* El alias de una estructura en C puede ser un nombre que se utiliza para representar un puntero a la misma, representado en el diagrama por un `*` seguido por el nombre del alias.  
<!--
@startuml
class structura as "estructura, alias, *referencia"<< (S,orchid) >>
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/FSVD2SKW303WkrC4Jw_54KJSeHE4c58XMiZF_hszx_PrSrIF9Oa4pS1SWpmK0Gqo_ri0PQ8LE9LlLjuqCTSAlwkyIiyS1xIMEkyHIpu0){width="40%"}
\ 

* Cuando todas las instancias de una estructura A (no heredada) referencian a la misma instancia de otra estructura B, esto se representará asociando la clase que representa a la estructura A con una instancia de B (objeto UML).  
<!--
@startuml
class A << (S,orchid) >>
object "instancia_de_b:B" as b

A -- "1" b
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLd1KiB5Hq0ZMoIzApiXCqLImi-FAJyfAJIvHKChC0whDIyvCZ4z9ZK-oSb9II2nMIEBYSbJGrLLGCbG2iXrIyrA0PW40){width="40%"}
\ 


* Los arreglos se identifican bajo el estereotipo `<<array>>`. Un arreglo posee un tipo. Los arreglos pueden componerse de instancias de su tipo y definen los elementos del arreglo.  
<!--
@startuml
object "Arreglo:Tipo" as arr << array >>
object ":Tipo" as nodo
arr *-- nodo
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuSfFoafDBb5GSYmgIatFoRSAoIp8Lr98B5P8B2fIiB41KOcL2dPsS8K85NdvAVbSN21rMhgw49v3QbuAq5G0){width="20%"}
\ 


Representaremos la herencia de estructuras como una composición de estructuras indicando que se trata del primer elemento de la estructura.


## Codificación
Para cada framework se mostrarán ejemplos de codificación bajo los mismos para facilitar el análisis. Para ellos hemos creado pequeños prototipos ejecutables que pueden encontrarse en el repositorio especificado en el apéndice.


## Dificultades en la codificación
El estudio de estos frameworks busca explorar si desde el modelo de objetos más sencillo (que soporta menor cantidad de conceptos de orientación a objetos) hasta el más complejo (que soporta una mayor cantidad de conceptos), la dificultad de implementar los conceptos que define (clases, interfaces, etc.) es grande, incluso haciendo uso intensivo del preprocesador de C. Presentaremos una representación en UML para la codificación para cada framework que no presenta dicha dificultad.  
Dentro de cada análisis incluiremos las "dificultades en la codificación" que son las dificultades para prototipar una clase de acuerdo a su relación con otras clases o interfaces, declarar sus métodos sin cuerpos y modificar cualquiera de estas cosas.  
Una dificultad que nombraremos aquí por ser común a todos los frameworks al estar basados en C es que cualquier inicialización o destrucción de atributos con tipos de clase debe ser codificada manualmente ante la inexistencia de un mecanismo automático que lo realice. Además la utilización dentro de un método de un objeto alocado en memoria automática también precisa de una inicialización y destrucción explícitas.


## Propuestas de expresión en UML
Para cada framework se dará una propuesta de expresión bajo UML. Con la misma se busca mostrar que UML no posee las dificultades estudiadas. Además nos servirá como base para el estudio de generación de código desde UML para estos frameworks. Las propuestas de expresión en UML estarán basadas en la definición de UML 2.5.1 dadas por la OMG [@UML2017].  


## Ben Klemens {#BenKlemens}
### Introducción
En su libro [@Klemens2013], busca mostrar nuevas formas de resolver problemas en lenguaje C de acuerdo a los nuevos estándares C99 y C11, y con las bibliotecas actuales de desarrollo en C como glib. Busca conseguir una sintaxis legible y mantenible para su codificación, sin intentar imitar a otros lenguajes a costa de un preprocesamiento extensivo con el preprocesador de C.
De acuerdo a su especificación, el usuario de las clases, obtiene un uso muy familiar con respecto a otros LPOO, llamando a los métodos polimórficos en la forma C++ o JAVA, aunque enviando el objeto como primer parámetro: 
```C
object->method(object);
```
Aunque veremos como eliminar esta repetición mediante una macro.


### Conceptos soportados
Los conceptos de la orientación a objetos soportados son mínimos:  
1. Encapsulamiento  
2. Herencia  
3. Polimorfismo   


### Modelo de objetos
El siguiente diagrama es una representación del modelo de objetos. Es el modelo más simple en el que los datos (atributos y métodos) de instancia se encuentran en la misma estructura y no hay referencias nativas a datos de clase (aunque veremos como representarlos).   
<!--
@startuml
object Hijo
object Padre{
[variables de instancia de Padre]
[métodos de instancia de Padre]
}
Padre --* Hijo : super <

object Hijo{
[instancia de Padre]
[variables de instancia de Hijo]
[métodos de instancia de Hijo]
}
@enduml
-->
![Modelo de objetos para Ben Klemens \label{model_klemens} ](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuSfFoafDBb7moCpAXx434bEAKgkveiiIYp8Jat9IYnLIKXKoyu1Qyf8p4u4SY8fOhkZSr9Byb7oSyhLSO8Q2hguMs085AuNYqeBK8WKRBcHxWLPXqOt7Vh0kkFMuf6kv75BpKe1-1000)
\ 


La herencia representada en la figura como la relación Padre-Hijo se consigue incluyendo una instancia del Padre en el comienzo de la instancia de Hijo. De esta manera las funciones y métodos escritos para las instancias de Padre se pueden utilizar para las instancias de Hijo.  
No existe una estructura que contenga datos de clase, aunque sí se podría referenciar a estos con punteros. Por ejemplo, si se desea definir una variable de clase entera  ```i```  todos los objetos podrían contener un puntero al mismo llamado -por ejemplo- ```class_i```. Lo mismo es aplicable a métodos de clase.
Comparado con un modelo de objetos basados en tabla virtual como los siguientes 5, o como otros LOOP como C++ o JAVA, este modelo generalmente presentará un mayor consumo de memoria  RAM ya que todas las referencias a métodos son copiadas individualmente en la estructura de la instancia. Por otro lado, ejecutar estos métodos polimórficos consumirá menos tiempo ya que no es necesario desreferenciar la tabla virtual. Además, las referencias individuales se pueden utilizar para cambiar los métodos a ejecutar en tiempo de ejecución en base al estado del objeto, lo que puede describir más limpiamente el comportamiento dependiente de estados como sucede con lenguajes prototipados como JavaScript. <!--
https://drive.google.com/file/d/0BxqE7uSMqmQgY2dta3VKZ3plVDg/view?usp=sharing
y
https://en.wikipedia.org/wiki/Prototype-based_programming
-->  


### Codificación
La declaración de una clase base, es decir que no hereda de ninguna otra, no es más que la declaración de una estructura en C con sus métodos representados en punteros a función y junto a 3 métodos de instanciación (new), copia (copy) y liberación (free) de objetos.  
A continuación un ejemplo con una clase Parent
```C
//parent.h
typedef void (*method_p_t)(parent*self);
typedef struct parent{
	int private_attribute1;
	int attribute2;
	method_p_t method;
}parent;

parent * parent_new();
parent * parent_copy(parent * parentToCopy);
void parent_free(parent * parentToFree);
```
Para representar la visibilidad de los miembros no públicos de la clase Klemens recomienda no usar ningún artilugio (como veremos de otros frameworks más adelante) y poner la confianza del lado del usuario de la clase indicando en el nombre del miembro su visibilidad (scope), así el atributo privado `attribute1` se llamará `private_attribute1`.




La implementación de la clase consiste en la implementación de los métodos new, copy, free; la implementación de los métodos polimórficos y su asignación al puntero a función que los representa
```C
//parent.c
static void method(parent * self){
	//do something
}
parent * parent_new(){
	parent * out = malloc(sizeof(parent));
	*out = (parent){ .method = method, .private_attribute1 = 0,
			.attribute2 = 0};
	return out;
}

parent * parent_copy(parent * in){
	parent *out = malloc(sizeof(parent));
	*out = *in;
	return out;
}
void parent_free(parent * in){
	free(in);
}
```
Más métodos no polimórficos pueden definirse para la clase en la forma de new, copy o free.  
El uso de la clase es muy intuitivo para usuarios de otros LPOO:
```C
parent * parent1 = parent_new( );
parent1 -> attribute2 = 10;
parent * parent2 = parent_copy( parent1 );
parent2 -> method ( parent2 );
parent_free ( parent1 );
parent_free ( parent2 );
```
 El siguiente código nos muestra como definir una clase que herede de otra.
```C
//child.h
typedef struct child{
	union{
		struct parent;
		struct parent asParent;
	};
	//new attributes and polymorphic methods...
}child;
//new(),copy(),free() and other non polymorphic methods...
```
Las uniones y estructuras anónimas nos permiten acceder directamente a los miembros de la misma sin necesidad de referenciarlas a través de otros nombres. Así para llamar a la función polimórfica `method()` definida en `parent` a través de una instancia de `child` llamada `aChild` va a tener la misma forma que para una instancia de `parent`: `aChild -> method ( aChild )`, aunque este generará un conflicto de tipo ya que el método `method()` espera un primer argumento de tipo `parent` y no de tipo `child`. Se incluye un acceso referenciado por nombre (`asParent`)a la estructura padre (`parent`) para poder resolver los conflictos de tipo para los métodos definidos en clases padres. De esta manera llamando a `aChild -> method ( aChild.asParent )` eliminaremos el conflicto de tipo que generaba el ejemplo anterior.  
Aunque Klemens no lo menciona, con esta especificación podemos generar macros que realicen verificaciones de tipo en tiempo de compilación para enviar mensajes a los objetos:
```C
//parent.h
#define _method(_parent) _Generic((_parent),				\
				parent:_parent -> method ( _parent ), 		\
				default:_parent -> method ( _parent.asParent ) )
```
`_Generic` es una nueva facilidad de C11 que permite evaluar una variable de acuerdo a su tipo. Con esta macro, para enviar el mensaje `method()` a una instancia de parent o a un objeto que herede de parent (por ejemplo `aChild`), no se necesita más que llamar a `_method(aChild)`. Si el argumento que pasamos no es un `parent` o una clase heredada del mismo entonces obtendremos un error en tiempo de compilación por no encontrarse el miembro `asParent` en su estructura.   

La implementación de los métodos `new()` y `free()` deben llamar a los métodos `new()` y `free()` de la clase padre, para `new()` al principio del método y para `free()` al final:  
```C
//child.c
static void method(parent * self){
	//do something
}
child * child_new(){
	child * out = (child *) parent_new();
	out -> method = method;
	// other initialization for child
	return out;
}

void child_free(child * in){
	// child release of resources
	parent_free(&in->asParent);
}
```
En el código de ejemplo vemos como `child` puede asignar su propia implementación del método polimórfico `method()` por lo que `_method(aParent);` llama a la función `method()` definida en parent.c mientras que `_method(aChild)` llama a la función `method()` definida en child.c.
 
### Dificultades en la codificación
| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores  | Inicialización de métodos polimórficos | Contrario a otros LPOO a la definición de un método debe  asignársele su implementación en el método constructor de la clase (`new()`), si esto se olvida generaremos un error crítico al enviar un mensaje con el método no inicializado. |  
|  2 | Propensión a errores y obtención de información | Prototipado de métodos polimórficos redefinidos | Para crear el prototipo de un método ya definido por una clase padre se debe conocer exactamente cuál de los padres es el que definió el método por primera vez, además de que en caso de equivocarnos recibiremos una advertencia por parte del compilador, esto agrega una demora al programador que debe obtener esta información. |  
|3|Repetición de código|Cada método implica de tres a cinco referencias al mismo | Incluirlo en la estructura de la clase, escribir su implementación, asignar la implementación al puntero a función en la estructura que lo representa y si es la primer clase que define el método, implementar su dispatcher.|  
|4|Repetición de código|Nombre de la clase en cada método no polimórfico| Para no colisionar con los nombres utilizados por otras clases, todas las funciones llevan por delante el nombre de la clase |  
|5|Repetición de código|Repetición en los métodos new, copy, free| Los métodos `new`, `copy` y `free` de cada clase son prácticamente iguales. Esto puede solucionarse con una macro.  |  
|6|Repetición de código|Heredar de una clase implica mucho código| Comparado con otros LPOO donde no hace falta más que una palabra reservada, el nombre de la clase padre e hija, esta especificación requiere el uso de nueve palabras. Esto puede solucionarse con una macro.  |
### Propuesta de expresión en UML
El diagrama UML que puede contener estos conceptos es directa, ya que son los más básicos para la orientación a objetos.  
El siguiente diagrama los expresa de forma correcta.
<!-- PLANT_UML
```uml
@startuml
Parent <|-- Child
Parent : int atributte1
Parent : int atributte2
Parent : method()
Child :  method()
@enduml
```
-->


![Propuesta representación UML para Klemens \label{uml_klemens} ](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuGf8B4hDAr6mgT7LLN3EoCn9WObOAMG2oSIIeioaqfAIL4CSuaO8yTpKaepy50rDBh0vG064Y4DgNWhGDW00){width="30%" height="30%"}
\ 


Para especificar que el método es polimórfico el atributo `isLeaf` debe ser falso de acuerdo a la definición de UML [@UML2017] . La herencia se indica como de costumbre en UML. Para especificar en el diagrama que Child contiene su propia implementación del método  `method` se lo vuelve a incluir como miembro de Child. Se puede indicar la visibilidad de los miembros lo que implica una indicación en su nombre.  
Ninguna de las dificultades enumeradas en la sección anterior aplican para este diagrama.

## Simple Object-Oriented Programming in C (SOOPC)

### Introducción
OOPC fue ideado por el autor como una forma simple de representar los conceptos centrales de la POO  en APIs escritas en C. Busca ocultar la dificultad de implementar POO en C para el desarrollador de la aplicación. Es compatible con el estándar ISO C89.


### Conceptos soportados
Los conceptos de la orientación a objetos soportados son mínimos:  
1. Encapsulamiento  
2. Herencia  
3. Polimorfismo  

### Modelo de objetos
Así como la cantidad de conceptos soportados es mínima, también lo es el modelo de objetos necesario para soportarlos.
La siguiente figura representa este modelo de objetos.

<!-- PLANT_UML
```uml
@startuml
class Child << (S,orchid) >>
class Parent << (S,orchid) >>
object ParentVtbl{
	[methods]()
}

Parent "*" o-- "1" ParentVtbl : vTable >
Parent --* Child : super <
@enduml
```
-->


![Representación del modelo de objetos de SOOPC \label{modelo_soopc}>
](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLd3EoCn9KR2nKT08rialIix8JD5KiBE3IWOa5gNcbM3AvYTbfIQNGAN3If9oghauetDJIpBoKufZDJIvQhcue5gLj9GKydLr5PGCbP2KArWfb8Ka9kMaAjZ15EhgQa7S0fGhBYr8BLAmuN98pKi1kXG0)
\ 


En el diagrama, `Parent` es una clase definida por el usuario, podría tener cualquier nombre. Debe contener una referencia a una tabla virtual si es que se trata de una clase con métodos polimórficos o variables de clase.
`Child` es otra clase definida por el usuario que hereda de la clase `Parent`. Los datos de `Parent` deben ser los primeros en memoria de `Child`, esto permite que las funciones que reciben una referencia a `Parent` como argumento, se les pueda pasar un una referencia a `Child`.
`Child`, a través de la referencia a la tabla virtual heredada de `Parent`, referencia a su propia tabla virtual del mismo tipo que la tabla virtual de `Parent`.
Si bien el autor de esta especificación no lo contempla, si la clase `Child` agregara más métodos virtuales para ella y para sus clases heredadas la referencia a la tabla virtual de `Child` podría ser un tipo heredado de `ParentVtbl`, una `ChildVtbl`, al igual que `Child` hereda de `Parent`.

### Codificación
El modelo de objetos quedará más claro con los siguientes ejemplos de codificación de una clase y una clase heredada de esta. La codificación sigue la especificación del autor.

A continuación codificamos la tabla virtual (vtable) de una clase que no herede de ninguna otra. En la estructura agregamos los métodos polimórficos de instancia así como las variables de clase, el método `method` es un ejemplo.
```C
/*Parent.h*/
typedef struct {
	void (*method)(const Parent * self);
	/*más métodos o variables de clase aquí*/
} ParentVtbl;
```

La clase Parent es representada a través de un la estructura Parent.

```C
/*Parent.h*/
typedef struct {
	const ParentVtbl * vptr;
	/*variables de instancia van aquí*/
} Parent;
```
Si Parent no tuviese métodos polimórficos la referencia a una tabla virtual podría postergarse hasta que alguna clase heredada sí los tenga.

La inicialización de la referencia a la tabla virtual  así como la tabla virtual  misma se realiza en el constructor.
```C
/*Parent.c*/
void Parent_metodo_(Parent * self)
{
	/*cuerpo del método*/
} 

void Parent_ctor(Parent * self)
{
	static const ParentVtbl vtbl=
	{
		Parent_metodo_
	};
	self -> vptr = &vtbl;
}
```
Las implementaciones de los métodos terminan con guión bajo(`_`), esto es así para diferenciarlo de las funciones que llaman a dichos métodos (dispatchers).

Los dispatchers hacen las llamadas a los métodos referenciados por las tablas virtuales, y son los utilizados por los usuarios de las clases. Hay tres alternativas para implementarlos: como una función, como una función "inline" (en caso de trabajar con C99) o como una macro. A continuación la presentaremos como función:
```C
/*Parent.c*/
void Parent_metodo(Parent * self)
{
	self -> vptr -> metodo (self);
}
```

Para implementar una herencia de la clase Parent se la ubica como primer miembro de la clase hija.
```C
/*Child.h*/
typedef struct {
	Parent super;
	/* más variables de instancia van aquí*/
} Child;
```

El constructor realiza la misma tarea que el constructor de Parent:
```C
/*Child.c*/
void Child_metodo_(Parent * self)
{
	/*cuerpo del método*/
} 

void Child_ctor(Child * self)
{
	static const ParentVtbl vtbl=
	{
		Child_metodo_
	};
	Parent_ctor(&self->super);
	self -> vptr = &vtbl;
}
```
Vemos que primeramente se realiza una llamada al constructor de la clase padre y luego se inicializa la clase heredada al igual que en cualquier lenguaje orientado a objetos. En caso de precisar un destructor, el orden debe ser el inverso, primero se liberan los recursos de la clase hija y luego los de la clase padre.
Vemos también que la referencia a la tabla virtual de Parent que se asigna en el constructor de Parent es luego reasignada con la tabla virtual de Child.
	
### Dificultades en la codificación


| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores y obtención de información | Inicialización de la tabla virtual | Al ser una especificación compatible con C89 la  única manera de instanciar la tabla virtual como constante obliga a inicializar sus miembros con una lista ordenada de valores, si el orden es incorrecto se estarán llamando a implementaciones de métodos que no corresponden a la llamada hecha, si el prototipo de los métodos es equivalente entonces no tendremos ningún aviso del compilador al respecto. Además se debe conocer la jerarquía de herencias y los métodos virtuales definidos por cada clase, esto agrega una demora al programador que debe obtener esta información.  |  
|  2 | Propensión a errores y obtención de información  | Prototipado de métodos polimórficos redefinidos | Para crear el prototipo de un método ya definido por una clase padre se debe conocer exactamente cuál de los padres es el que definió el método por primera vez, además de que en caso de equivocarnos recibiremos una advertencia por parte del compilador, esto agrega una demora al programador que debe obtener esta información. |  
|3| Repetición de código |código repetitivo para todas las clases| Instanciar una tabla virtual, asignarla a la referencia en el objeto son operaciones que se realizan para todas las clases y se deben repetir manualmente para el implementador.|
|4|Repetición de código|Cada método implica de 3 a 5 referencias al mismo | Incluirlo en los miembros de la tabla virtual, generar su implementación (puede precisar tener su prototipo en el archivo de encabezado (`.h`) si lo precisa una clase heredada), asignar la implementación a la tabla virtual y si es la primer clase que define el método, implementar su dispatcher.|
|5|Repetición de código|Nombre de la clase en cada método miembro| Para no colisionar con los nombres utilizados por otras clases, todas las funciones llevan por delante el nombre de la clase.|


### Propuesta de expresión en UML
El diagrama UML que puede contener estos conceptos es directa ya que son los más básicos para la orientación a objetos. El siguiente diagrama los expresa de forma correcta.  
<!-- PLANT_UML
```uml
@startuml
Parent <|-- Child
Parent : polymorphic_method()
Parent : method()
Parent : attribute
Parent : class_attribute
Child :  polymorphic_method()
@enduml
```
-->


![Propuesta representación UML para SOOPC \label{uml_soopc}
](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuGf8B4hDAr6mgT7LLN3EoCn9WObOAHJavrJcvXSLP6Gcn-UcbcJafsXe8cGnHH9BYZAJIajI4IB9EOd5nV5mSR3vG67i9ZkavgK0NGG0){width="30%" height="30%"}
\ 


Para especificar que el método `polymorphic_method`  es polimórfico la propiedad `isLeaf` debe ser falsa de acuerdo a la definición de UML [@UML2017]. La herencia se indica como de costumbre en UML. Para especificar en el diagrama que Child contiene su propia implementación del método  `polymorphic_method` se lo vuelve a incluir como miembro de Child. Para que el atributo `class_attribute` sea de clase (o sea esté referenciado a través de la tabla virtual por todas las instancias de la clase), la propiedad `isStatic` debe ser verdadera de acuerdo a la definición de UML [@UML2017].
Cualquier especificación de visibilidad en los miembros de la clase o instancia son ignorados exceptuando los métodos no polimórficos que pueden ser públicos (incluidos en el archivo header de la clase) o privados (no incluidos en el archivo header de la clase).
Ninguna de las dificultades enumeradas en la sección anterior aplican para este diagrama.



## OOPC de Laurent Deniau
### Introducción
Este framework está fuertemente basado en C++, por lo que su modelo de objetos y performance son muy parecidos a los de este LPOO. Fue ideado como alternativa a C++ bajo el estándar ISO C89 (en el caso donde no hay un compilador C++ disponible, como sucede para varios sistemas embebidos, o no se quiera utilizar el compilador C++ disponible como tecnología de desarrollo o para no depender de la API de C++), aunque también presenta facilidades adicionales bajo el estándar ISO C99, y además provee interesantes facilidades para depurar.
<!--
These techniques may be useful for programmers who have not a C++ compiler for their architecture (calculators, small systems, embedded systems). It may also be useful for people who are disappointed by C++ compilers which do not behave like the norm says or even do not support all the C++ features or by C++ APIs that change from time to time. In fact, I don't know (at the revised date of this paper) any compiler which fully support the norm C++98. It is clear that the techniques presented here have not the pretension to replace C++, that is impossible without a _cfront_ translator (OOPC uses only C macros and few C lines), but it provides enough to do serious OOP
-->


### Conceptos soportados
1. Encapsulamiento
2. Herencia (múltiple)
3. Polimorfismo
5. Genericidad
6. Excepciones


### Modelo de objetos
Debido principalmente a la herencia múltiple, OOPC posee un modelo de objetos más complejo que los vistos anteriormente, por lo que se analizará con más de un diagrama.  


#### Clase base
El siguiente diagrama representa el modelo de objetos que da soporte a todo el framework. Representa las estructuras e instancias que dan soporte a una clase `Parent` (el nombre `Parent` podría reemplazarse por cualquier otro nombre).  

<!--
@startuml
class t_Parent << (U,orchid) >>{
[m.data]
}
hide t_Parent methods

object "_ooc_vtbl_Parent:_ooc_vtbl_Parent" as _ooc_vtbl_Parent{
_.offset = 0
[(*methods)()]
}

object "_ooc_info_Parent:_ooc_type_info" as _ooc_info_Parent{
name = "Parent"
obj = &_ooc_obj_Parent
super = 0
extraSuper = 0
extraOffset[_OBJECT_MAXEXTRASUPER_] = 0
}

object "Parent:_ooc_class_Parent" as Parent{
  (*Parent)(); 
  (*_Parent)();
  (*alloc)();
  [(*methods)()]
}


Parent "1" -- "1" _ooc_vtbl_Parent : __vptr >
_ooc_vtbl_Parent "1" -- "1" _ooc_info_Parent : _.info >
t_Parent "*" o-- "1" _ooc_vtbl_Parent : __vptr >
_ooc_info_Parent "1" -- "1" Parent : class >
@enduml
-->
![Modelo de objetos OOPC \label{modelo_oopc} ](http://www.plantuml.com/plantuml/png/ZP9FQm8n4CNlyoc67CegLTghjQ8jNWf5yGy88Y7kHhJiRYGPfMNfTsySKrtJIq-RD_dDcvTX-uwKfKEHGvehvv3aM5bT4dQxcCplZKstkwo1lLu5owATAL8h-0PVqrUqqBGrcGCmwm-T4WffJ2gFjCu3qOaB0fN3k5Y1R9lDncd2Pto0PT8CnesauOT6zhjoOsxiwMklkShcCLD1gGhjZKL8S7Bpyev1VmuSkCDUMmwWFycgwQqSSRwb7Bsy3Lzdydsm62vcay5qFXvEv8gvMj1wF5vl_Vc_kH2Jvldit_c4hEMrm5hbkKc3-hiNEDCe7WMsMlo9TuiTbFAu9uizgD_5VT7QkAzzqhwFBdnJeFdle60M3xhGl1SFztMP-T_m1m00)
\ 

<!--
This model is built around three different types available for each class plus a general RTTI (Run-Time Type Information) type:

-   The  _object_  collects the class non-static data (any instances).
-   The  _class_  collects the class static data, class methods and methods (one instance, doesn't exist in C++).
-   The  _virtual table_  collects the class object methods (one instance).
-   The  _type info_  collects the class RTTI (one intance).
The methods in the previous representation can be classified as follow:
-->

Analizaremos los cuatro elementos del diagrama:  
* La unión, representado con el estereotipo `U`,  `t_Parent ` contiene todos los datos de instancia dentro de una estructura sin identificador de tipo y anidada `m` y una referencia a su tabla virtual mediante `__vptr`. Cada instancia de la clase `Parent` significará una instancia de la estructura `t_Parent`.  
*  La tabla virtual `_ooc_vtbl_Parent` es una instancia de la estructura `_ooc_vtbl_Parent`, y contiene los métodos polimórficos de instancia de la clase `Parent`. Contiene una instancia de la estructura `_ooc_vtbl_object` bajo el símbolo `_`, que contiene las variables `offset` e `info`.  `info` es una referencia a la estructura  `_ooc_info_Parent` que contiene la información de la clase `Parent` (RTTI). `offset` es utilizada, como veremos, para manejar la herencia múltiple.  
* La estructura de información de tipo `_ooc_info_Parent` contiene la información de la clase `Parent`, su nombre en `name`, una instancia de `t_Parent` constante no inicializada pero con referencia a la tabla virtual en `obj`, y una referencia la estructura `Parent`.  
* La estructura `Parent` es una instancia de  `_ooc_class_Parent`, contiene los miembros de clase (atributos y métodos de clase) y métodos de instancia no polimórficos. Entre los métodos de clase generados están `Parent()` que inicializa la clase y retorna una instancia constante  inicializada con valores por defecto y con su referencia a la tabla virtual (`_ooc_obj_Parent`),   el destructor de instancia `_Parent()`, y el instanciador `alloc()` que retorna una copia en heap de `_ooc_obj_Parent`. Es común en esta estructura definir métodos de clase como new, copy e init. La referencia a la tabla virtual `__vptr` permite llamar a los métodos polimórficos de `Parent` en forma no polimórfica cuando el tipo del objeto es conocido, además permite que la clase misma (y no una instancia de la misma) sea tratada como un objeto (al contener métodos de clase polimórficos redefinibles por las clases heredadas).

<!--
-   Methods in the  vtbl  are  _object methods_  (C++: virtual member functions).
-   Methods in the  class  working on an object are  _methods_  (C++: member functions).
-   Methods in the  class  not working on an object are  _class methods_  (C++: static member functions).

The ctor() has always the same name as the class and therefore it is always called by _class_._class_() (C++: default constructor _class_::_class_()). The dtor() has always the same name as the class name preceded by an underscore (C++: tilde) and therefore is always called by _class_.__class_() (C++: default destructor _class_::~_class_()). The ator() is always called alloc() (C++: default allocator new(_class_)) and may throw ooc_bad_alloc exception upon failure. In OOPC, the default constructor and allocator are defined, declared and generated since they return only a well formed instance of the object without any dynamic initialization (only static initializations can be performed), but the default destructor is only defined and declared (not generated) since it may require some dymanic freeing and therefore needs to be provided by the class designer (even if empty).

The  __vptr  in  object  points to the virtual table and allows to call object methods without knowing its type. This is the key point of the polymorphism mechanism. It also allows to reach the class RTTI data required for dynamic casting.

The  __vptr  in  class  also points to the virtual table and allows to bypass the polymorphism mechanism. It also allows classes to behave like objects (with some limitations) for object methods calls and RTTI purposes.
-->

#### Herencia simple
El siguiente diagrama muestra las estructuras que dan soporte a una clase `Child` que hereda de `Parent`.

<!--
@startuml
class t_Parent << (U,orchid) >> #lightgrey{
[m.data]
}
hide t_Parent methods

object ":_ooc_vtbl_Parent" as _ooc_vtbl_Parent{
_.offset = 0
[(*methods)()]
}

object "_ooc_info_Parent:_ooc_type_info" as _ooc_info_Parent #lightgrey{
name = "Parent"
obj = &_ooc_obj_Parent
super = 0
extraSuper = 0
extraOffset[_OBJECT_MAXEXTRASUPER_] = 0
}



class t_Child << (U,orchid) >>{
[m.data]
}
hide t_Child methods

t_Child "1" *-- "1" t_Parent : m.Parent >

object "_ooc_vtbl_Child:_ooc_vtbl_Child" as _ooc_vtbl_Child{
[(*methods)()]
}


_ooc_vtbl_Child "1" *-- "1" _ooc_vtbl_Parent : Parent >
t_Child "*" o-- "1" _ooc_vtbl_Child : __vptr >

object "_ooc_info_Child:_ooc_type_info" as _ooc_info_Child{
name = "Child"
obj = &_ooc_obj_Child
extraSuper = 0
extraOffset[_OBJECT_MAXEXTRASUPER_] = 0
}

_ooc_vtbl_Parent "1" -- "1" _ooc_info_Child : _.info >

_ooc_info_Child "1" -- "1" _ooc_info_Parent : super >

object "Child:_ooc_class_Child" as Child{
  (*Child)(); 
  (*_Child)();
  (*alloc)();
  [(*methods)()]
}
_ooc_info_Child "1" -- "1" Child : class >
Child "1" -- "1" _ooc_vtbl_Child : __vptr >
@enduml
-->
![Herencia simple para OOPC \label{model_simple_inheritance_oopc} ](http://www.plantuml.com/plantuml/png/fL9VQy8m47_FfpWw6BNCihrshkZ4by5G_0E2I8Xjj1rf8ssKYUoxhprYLbFnPK_Dxt9t-VrflrIiK8TCa4YmiWH59wpWkO9U3zp5aooY98qx48Rm8D9TedO5Fvt9AlDZfjYQ_93gclzDPLmbCYu9aPil7Ybm0YfbH8zg8tIB0wm4ktWcr9VRRSaLlC4pMRcUNjHnE_KZPXqEflbMwi402-gqvrZ5vNRF5VESPRnwmz5awiKLVCIPwgn7I7dOymAvy6zLiDar72FL5Hs_Vuo6S_evM8wMy-bWjfYCfdIDVHLdOYmT9gc8MuwsR3ITniLBmNbnmEjsyMky3Y3pzJ6q749hSJImi6K_riut32UcfqsXCM-e62A6ikU0jFlrNG2K7lUgWF1MijXpDrXDsuI9i9qbblyHe2qJjJGb8HSZoQzXBSc-kpbcdCDVhUb4Gp_-G8tWj6u0ryDZbTOh8AG68sH2o4WZEznx_2vYyE6AbDLmDye-p-D39du1)
\ 


En gris se encuentran los elementos ya presentados para la clase `Parent`.  
La unión t_Child incluye a la unión t_Parent como primer elemento. Esto, como ya hemos visto en los frameworks anteriores, permite que las instancias de `Child` (o t_Child) puedan ser vistas como instancias de `Parent` (o t_Parent) para los métodos definidos para `Parent`.  
La tabla virtual _ooc_vtbl_Child contiene la estructura de la tabla virtual de Parent (_ooc_vtbl_Parent) como primer elemento de su estructura. Esto también es necesario para que una instancia de `Child` pueda ser considerada una de  `Parent` conteniendo los mismos métodos polimórficos (aunque pueden redefinirse para `Child`). Luego de la estructura _ooc_vtbl_Parent vienen los métodos polimórficos definidos especialmente para `Child`.  
La estructura de RTTI es análoga a la de la clase base `Parent` pero el atributo `super` es inicializado con la estructura de RTTI de  `Parent`. Esto permite realizar casteos dinámicos donde no se conoce la clase del objeto.
<!-- TODO: explicar qué es un casteo dinámico -->

#### Herencia múltiple
El siguiente diagrama muestra cómo quedaría el modelo de objetos si `Child` heredara de una clase adicional `Parent2`. Las estructuras no afectadas respecto a la herencia simple aparecen en gris.

<!--
@startuml
class t_Parent << (U,orchid) >> #lightgrey{
[m.data]
}
hide t_Parent methods
note "Primero en t_Child" as N1
t_Parent .. N1


object ":_ooc_vtbl_Parent" as _ooc_vtbl_Parent #lightgrey{
_.offset = 0
[(*methods)()]
}

object "_ooc_info_Parent:_ooc_type_info" as _ooc_info_Parent #lightgrey{
name = "Parent"
obj = &_ooc_obj_Parent
super = 0
extraSuper = 0
extraOffset[_OBJECT_MAXEXTRASUPER_] = 0
}


class t_Child << (U,orchid) >>{
[m.data]
}
hide t_Child methods

t_Child "1" *-left- "1" t_Parent : m.Parent >

object "_ooc_vtbl_Child:_ooc_vtbl_Child" as _ooc_vtbl_Child{
[(*methods)()]
}


_ooc_vtbl_Child "1" *-- "1" _ooc_vtbl_Parent : Parent >
t_Child "*" o-- "1" _ooc_vtbl_Child : __vptr >

object "_ooc_info_Child:_ooc_type_info" as _ooc_info_Child {
name = "Child"
obj = &_ooc_obj_Child
extraSuper = 1
extraOffset[0] = sizeof(t_Parent)
}

_ooc_vtbl_Parent "1" -- "1" _ooc_info_Child : _.info >

_ooc_info_Child "1" -- "1" _ooc_info_Parent : super >

object "Child:_ooc_class_Child" as Child #lightgrey{
  (*Child)(); 
  (*_Child)();
  (*alloc)();
  [(*methods)()]
}
_ooc_info_Child "1" -- "1" Child : class >

class t_Parent2 << (U,orchid)>>{
[m.data]
}
hide t_Parent2 methods


t_Child "1" *-right- "1" t_Parent2 : m.Parent2

object ":_ooc_vtbl_Parent2" as _ooc_vtbl_Parent2{
_.offset = sizeof(t_Parent)
[(*methods)()]
}

t_Child "*" o-- "1" _ooc_vtbl_Parent2 : m.Parent2.__vptr >
_ooc_vtbl_Child "1" *-- "1" _ooc_vtbl_Parent2 : Parent2 >

object "_ooc_info_Parent2:_ooc_type_info" as _ooc_info_Parent2{
name = "Parent2"
obj = &_ooc_obj_Parent2
super = 0
extraSuper = 0
extraOffset[_OBJECT_MAXEXTRASUPER_] = 0
}

object "Parent2:_ooc_class_Parent2" as Parent2{
  (*Parent2)(); 
  (*_Parent2)();
  (*alloc)();
  [(*methods)()]
}
_ooc_info_Parent2 "1" -- "1" Parent2 : class >
_ooc_vtbl_Parent2 "1" -- "1" _ooc_info_Parent2 : _.info  >
@enduml
-->

![Modelo de objetos para herencia múltiple en OOPC \label{model_multiple_inheritance_oopc} ](http://www.plantuml.com/plantuml/png/hLHTQzmm47pNhrXSABwZCJa_kkcHDDnBeSsH3mWSGJYsd7EnhKFMXQQX_xtMecmaonWAVTEiTjUpEsETzpfN-jWsh6ZolWVDjxaIdOQpCuZlFabLxEjo0UirV6Zgfxr-KkBbbUtQfCnr_i3-iE5Ql5UrGkzbsRDEQW7HLjMjK194DsHSxkkcZ23luSUAKK6I6CYOVFmf2WrHngKi-BD-R6m6Leo37XcUoAhgXOOlSCfsyT9oMCGBmuywOu-wgwJjaM50lnm4HkaxYAQ-q-Mj6BuHMLwcyG0_OirmjYMiFnw4GYxYbrRvZG-laEgENttzjhcyvTylxZVtjzSNDtVRpJL_C7c6Cuc1Imkq20MmcRH_zXQ8LX4iJnfHwHCyq-8pQ1DxN3jBeaLZUJR2ZXWKUvtOEVDoY0PISEg92X4Xsii8f9zFlJBW_FcW5QodnCMSMMrjctT14OTwOjWNSUM9U6hqwklVGbRnsruNPlnWGZE6EmtHm6aI0yqqexlfCbeQ6irTWZCw-iVH34-Ucm7Y9KO7pJu3GauOOTuqih0ea7Y6wjjSoC7myr-MrBVpb9ifbVmyDhGoW_YEJXrBfpCFIZhvegJkEnAA6im_xzGfIWcvTlxd25lOqxJVxUMSuobf_8QbeUdjnVzunOWiKIHdUb8GEMCw2nnJKkJVR6dBN6FQ45dJKM8gFUn5_-fGUYwwyjWsVm40){width="100%" height="100%"}
\ 

Podemos apreciar que la estructura `t_Child` está compuesta primero por la estructura `t_Parent` continuada por  la estructura `t_Parent2`.  
Cada una de sus referencias a una tabla virtual (la contenida en `t_Parent` y la contenida en `t_Parent2`) referencia a una tabla diferente. `t_Parent2` en `t_Child` referencia a una tabla virtual con la misma estructura a la de cualquier instancia de una clase `Parent2` y con la misma referencia a la estructura RTTI de un `Parent2`. Pero existe una gran diferencia con cualquier instancia de una clase `Parent2`, el atributo `offset` guarda donde se encuentra la estructura `t_Parent2` en `t_Child`, esta información permite que al pasar una referencia de `Parent2` en una instancia de `Child` (por ejemplo a una función que espera recibir un `Parent2` como parámetro) se pueda referenciar, a la instancia de `Child`  que la contiene, en tiempo de ejecución (casteo dinámico). Por otro lado `t_Parent` en `t_Child` referencia a los mismas instancias que si tan solo hubiese una herencia simple entre  `t_Parent` y `t_Child`, la única diferencia es que en la estructura de RTTI se indica mediante el atributo `extraSuper` la cantidad de herencias por encima de la simple y en `extraOffset` la posición relativa de las mismas dentro de la estructura `t_Child`.

### Codificación
OOPC facilita varias macros para la codificación y uso de las clases mediante la inclusión del archivo `ooc.h`


#### Codificación de una clase base
##### Interfaz (.h)
```c
//Parent.h
#undef  OBJECT
#define OBJECT Parent

/* Object interface */
BASEOBJECT_INTERFACE

  char const* private(name);
  int private(attribute);
  
BASEOBJECT_METHODS

  void constMethod(print);
  
ENDOF_INTERFACE

/* Class interface */
CLASS_INTERFACE

  t_Parent*const classMethod_(new) char const name[] __;
  void method_(init) char const name[] __;
  void method_(copy) t_Parent const*const parent __;
  int class_attr;

ENDOF_INTERFACE
```
Cada clase comienza redefiniendo (mediante `#undef` y  seguido por `#define`) la macro `OBJECT`, esto es necesario ya que el resto de las macros evalúan este valor.  
Al tratarse de una clase base se utilizan las macros `BASEOBJECT_INTERFACE` y `BASEOBJECT_METHODS` en ese orden.  
Debajo de la macro `BASEOBJECT_INTERFACE` se declaran las variables de instancia (`attribute` en el ejemplo)  y luego de `BASEOBJECT_METHODS` los métodos polimórficos de instancia (`print`).  
Debajo de `CLASS_INTERFACE` se declaran tanto los miembros de clase (variables (`class_attr`)y métodos (`new`)) como los métodos no polimórficos de instancia (`init` y `copy`).
<!--
print is defined as a _constant object method_ (C++: constant virtual member function) using the constMethod() macro, that means it does not modify the this pointer. The method() macro is also available to define non-constant object methods.
-->
El método `print` es definido como un método de instancia constante usando la macro ` constMethod()`, lo que significa que no modifica el estado del objeto al ejecutarse. La macro `method()` se utiliza para métodos no constantes y para los métodos de clase (que no referencian a ningún objeto) `classMethod()`. Cada uno de ellos tiene su equivalente para métodos con argumentos agregando un `_` al final (`method_() constMethod_() classMethod_()`), si nos encontramos en C99 dentro de los paréntesis podemos agregar los argumentos (por ejemplo `method_(init,char const name[])` ), pero bajo C89 los argumentos van a continuación terminados con `__` como en el ejemplo (`method_(init) char const name[] __`).   
Como puede apreciarse, el atributo `attribute` tiene visibilidad privada mediante la macro `private()`, esta macro puede utilizarse tanto para variables como para métodos. Esto se logra cambiando el nombre del argumento por algo indicativo de ser privado (en el ejemplo attribute__ooc_private_40201112L), pero no cambiándolo en el archivo de implementación o código fuente donde se define la macro `IMPLEMENTATION` como se verá a continuación.

##### Implementación (.c)
```c
//Parent.c
// include de otros módulos
#define IMPLEMENTATION

#include <Parent.h>

void
constMethodDecl(print)
{
  // print implementation
}

BASEOBJECT_IMPLEMENTATION

  methodName(print)

ENDOF_IMPLEMENTATION

/*
  --------------------
  Class implementation
  --------------------
*/

initClassDecl() /* requerido */
{
	/* código de inicialización de la clase */
    objDefault(attribute) = 1;
} 

dtorDecl() /* requerido */
{
  /* código del destructor de instancias */
}

t_Parent
classMethodDecl_(*const new) char const name[] __
{
  /* código de método new */ 
  return this;
}

void
methodDecl_(init) char const name[] __
{
  /* código de método init */ 
}

void
methodDecl_(copy) t_Parent const*const per __
{
  /* código de método copy */ 
}

CLASS_IMPLEMENTATION

  methodName(new),
  methodName(init),
  methodName(copy),
  0	/*class_attr=0*/
ENDOF_IMPLEMENTATION
```
El archivo implementación comienza con la inclusión de otros módulos y clases, esto es así para que no sean afectadas por la definición de la macro `IMPLEMENTATION` que es definida a continuación seguida por la inclusión del archivo de interfaz (.h).
La declaración de los métodos para su implementación se realiza mediante una macro paralela a la que se usa para la declaración en la interfaz.


|implementacion|interfaz|
|-------------------|-------------------| 
|classMethodDecl()  |classMethod()|
|classMethodDecl_()  |classMethod_()  |
|methodDecl()  |method()  |
|methodDecl_()  |method_()|
|constMethodDecl()  |constMethod()  |
|constMethodDecl_()  |constMethod_()|

Entre `BASEOBJECT_IMPLEMENTATION` y `ENDOF_IMPLEMENTATION` se encuentran los métodos de instancia (en el mismo orden que se declararon en el interfaz), por lo que antes de estas macros deben declararse.  
Entre las macros `CLASS_IMPLEMENTATION` y `ENDOF_IMPLEMENTATION` se encuentran los miembros de clase (métodos e inicialización de atributos), por lo que antes de estas macros deben declararse sus métodos.
`initClassDecl()` y `dtorDecl()` son macros obligatorias en la implementación de una clase. `initClassDecl()` se ejecuta una sola vez al instanciarse el primer objeto de la clase y se utiliza en una clase base tan solo para inicializar atributos de clase y asignar valores por defecto de los atributos de instancia (mediante la macro `objDefault` que cambia los atributos de `_ooc_obj_Parent`), mientras que `dtorDecl()` se llama cada vez que un objeto de la clase es destruido (mediante la macro `delete()` si se utiliza memoria dinámica o con la llamada explícita al destructor, en el ejemplo `Parent._Parent()`).


#### Codificación de una clase derivada
Siguiendo el ejemplo de modelo de objetos presentamos el código de una clase `Child` que hereda de `Parent` y de `Parent2`.


##### interfaz (.h)
```c
//Child.h
#include <Parent.h>
#include <Parent2.h>

#undef  OBJECT
#define OBJECT Child

/* Object interface */
OBJECT_INTERFACE

  INHERIT_MEMBERS_OF (Parent);
  INHERIT_MEMBERS_OF (Parent2);
  int private(child_attribute);

OBJECT_METHODS

  INHERIT_METHODS_OF (Parent);
  INHERIT_METHODS_OF (Parent2);
  void method(child_method);
  
ENDOF_INTERFACE

/* Class interface */
CLASS_INTERFACE

  t_Child*const classMethod_(new)
     char const name[]__;
  void method_(init)
     char const name[]__;
  void method_(copy) t_Child const*const child __;

ENDOF_INTERFACE

```
Podemos apreciar que debajo se las macros `OBJECT_INTERFACE` y `OBJECT_METHODS` se declara la herencia de `Parent` y de `Parent2` (mediante las macros `INHERIT_MEMBERS_OF` y `INHERIT_METHODS_OF` en el orden de la herencia). Luego de los mismos continúa la declaración de atributos y de métodos propios de `Child`. La declaración de los miembros de clase es la misma que la de una clase base.

##### Implementación (.c)
```c
//Parent.c
#define IMPLEMENTATION

#include "Child.h"

void
methodDecl(child_method)
{
  /*código para child_method()*/
}

void
constMethodOvldDecl(print, Parent)
{
  /*código para redefinición de print()*/
}

OBJECT_IMPLEMENTATION

  SUPERCLASS (Parent),
  SUPERCLASS (Parent2),
  methodName(child_method)

ENDOF_IMPLEMENTATION

initClassDecl() 
{
  /* inicializar clases padre */
  initSuper(Parent);
  initSuper(Parent2);

  /* redefinición de métodos */
  overload(Parent.print) = 
	  methodOvldName(print, Parent);


}

dtorDecl()
{
	/*Liberación de recursos de Child*/
	Parent._Parent(super(this,Parent));
	Parent2._Parent2(super(this,Parent2));
}

/*...*/

void
methodDecl_(init)
     char const name[]__
{
  Parent.init(super(this,Parent), name);
  Parent2.init(super(this,Parent2));
  /*más código de inicialización*/
}

CLASS_IMPLEMENTATION

  methodName(new),
  methodName(init),
  methodName(copy)

ENDOF_IMPLEMENTATION
```
Para redefinir métodos se utilizan las macros especiales para su declaración. La siguiente tabla muestra las macros para las declaraciones en las clases base y su equivalente para su redefinición en las clases heredadas:


|clase base|clase heredada|
|-----------------|-----------------|
| methodDecl()  | methodOvldDecl()  |
| methodDecl_()  | methodOvldDecl_()  |
| constMethodDecl() | constMethodOvldDecl()   |
| constMethodDecl_() | constMethodOvldDecl_()|

En el código del ejemplo podemos apreciar la redefinición del método `print()` con la macro `constMethodOvldDecl()`, que luego se debe asignar en la inicialización de la clase a su referencia en la tabla virtual mediante las macros `overload` y `methodOvldName` (en el ejemplo la línea: `overload(Parent.print) = methodOvldName(print, Parent);`).
Debajo de la macro `OBJECT_IMPLEMENTATION` indicamos la herencia de las clases padre mediante la macro `SUPERCLASS`. Luego en la inicialización de la clase, bajo la macro `initClassDecl()`, inicializamos las instancias de la clases padre dentro de la clase base  (dentro de las estructuras de la tabla virtual y de RTTI), además se actualizan los valores de `extraSuper` y `extraOffset` (ver modelo de objetos).  
Al final del destructor de `Child()` (`dtorDecl()`) se llama a los destructores de las clases padre.  
En el ejemplo donde creamos los métodos `init()`  para la inicialización de instancias de cada clase, el método `init()` de `Child` llama a los de sus clases padre al principio.

#### Clase abstracta
Una clase abstracta no puede instanciarse y puede no proveer una definición de sus métodos. Esto se logra parcialmente en OOPC proveyendo la macro `ABSTRACTCLASS_INTERFACE` para ser utilizada en vez de `CLASS_INTERFACE` y cuya única diferencia es que no define al método `alloc()`. Luego las asignaciones a las referencias a métodos mediante las macros `methodName()` y `methodOvldName()` de los métodos que no proveen una implementación deben ser reemplazadas por 0.

#### Clase genérica
Para codificar clases genéricas OOPC utiliza un mecanismo simple valiéndose del preprocesador de C. Todos los tipos genéricos se declaran como `gType1`,  `gType2`, etc. postergando su definición a la implementación del usuario (también podrían usarse otros nombres como `T`). Luego para cada clase genérica que el usuario instancie en una clase concreta deberá especificar un prefijo para dicha clase mediante la definición de la macro `gTypePrefix`.


##### Instanciación  


El siguiente es un ejemplo de instanciación de una clase genérica tanto para un tipo entero como uno flotante en los mismos archivos .c y .h
```C
//array.h
/* integer array */

#define gTypePrefix i
#define gType1      int

#include <g_array.h>

#undef gType1
#undef gTypePrefix

/* double array */

#define gTypePrefix d
#define gType1      double

#include <g_array.h>

#undef gType1
#undef gTypePrefix
``` 
```C
//array.c
/* integer array */

#define gTypePrefix i
#define gType1      int

#include <g_array.c>

#undef gTypePrefix
#undef gType1

/* double array */

#define gTypePrefix d
#define gType1      double

#include <g_array.c>

#undef gTypePrefix
#undef gType1
```


##### Declaración  


El siguiente es un ejemplo de la interfaz de una clase genérica.

```C
//g_array.h
#if defined(GENERIC) || !defined(G_ARRAY_H)
#define G_ARRAY_H
/*...*/
#undef  OBJECT
#define OBJECT GENERIC(array)

/* Object interface */
OBJECT_INTERFACE

  INHERIT_MEMBERS_OF(GENERIC(memBlock));

  gType1* private(data);

OBJECT_METHODS

  INHERIT_METHODS_OF(GENERIC(memBlock));

  gType1* method(getData);
  /*...*/
ENDOF_INTERFACE


CLASS_INTERFACE
	/*...*/
ENDOF_INTERFACE

#endif
```
La macro `OBJECT` se define con la macro `GENERIC` que es la encargada de agregar el prefijo a la instanciación. La clase genérica array desciende de la clase genérica memblock (para facilitar la referencia al destructor de la clase padre se utiliza la macro `GENERIC_DTOR(memblock)`).
El tipo genérico `gType1` es luego definido por el usuario.

### Dificultades en la codificación
| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores  | Inicialización de métodos polimórficos | Contrario a otros LPOO a la definición de un método debe  asignársele su implementación en la función de inicialización de la clase (bajo la macro `initClassDecl()`), si esto se olvida generaremos un error crítico al enviar un mensaje con el método no inicializado. |  
|  2 | Propensión a errores y obtención de información  | Prototipado de métodos polimórficos redefinidos | Para crear el prototipo de un método ya definido por una clase padre se debe conocer exactamente cuál de los padres es el que definió el método por primera vez, además de que en caso de equivocarnos recibiremos una advertencia por parte del compilador, esto agrega una demora al programador que debe obtener esta información. |  
|  3 | Propensión a errores y obtención de información  | Asignación en la tabla virtual de métodos polimórficos redefinidos | Para asignar la reimplementación de un método en la tabla virtual a través de la macro `overload()` se debe conocer toda la jerarquía de clases hasta la clase que definió por primera vez el método, además de la posible equivocación de cálculo esto demora tiempo al programador para obtener dicha información. Además si nos encontramos en el contexto del problema del diamante deberemos repetir esto por cada uno de los caminos de ascendencia. |  
|4|Repetición de información|Cada método implica de 3 referencias al mismo | Incluirlo en la estructura de la clase o la tabla virtual (dependiendo si es polimórfico o no), en su implementación, asignar la implementación al puntero a función de la estructura que lo representa.|
|5|Repetición de información|Repetición en la información de la herencia| En la interfaz se duplica la información de la herencia de clases ( bajo las macros`INHERIT_MEMBERS_OF` y `INHERIT_METHODS_OF`), lo mismo pasa en el archivo de implementación en cada redefinición de un método, en el método del destructor y bajo la macro `OBJECT_IMPLEMENTATION`. |
| 6 | Propensión a errores  | Destructor de instancias | No se debe olvidar para el destructor de una clase heredada llamar al destructor de la clase padre **al final** del destructor. | 
|7| Propensión a errores  |Orden de los métodos| Para la definición de métodos (y no su redefinición que se realiza bajo la macro `initClass()`) el orden de los métodos debajo de `BASEOBJECT_METHODS` debe ser el mismo que debajo de `OBJECT_IMPLEMENTATION`. En caso de haber invertido el orden y el prototipo de los métodos invertidos es el mismo, no nos lo advertirá el compilador.| 
|8|Repetición de información|Duplicación en la redefinición de métodos|Se duplica la información de la redefinición de un método (con la macro `constMethodOvldDecl()` y la asignación mediante las macros `overload` y `methodOvldName`).| 
|9| Dificultad de aprendizaje| Gran cantidad de nuevas macros| OOPC no es un framework minimalista como los anteriores, hay una gran cantidad de macros que utilizar y aprender.|
|10| Repetición de código | Repetición en cada clase| Comparado con otros LPOO, hay mucho código repetido entre una clase y otra tan solo para definir la clase y su herencia. |
|11| Propensión a errores | Macros parecidas para distintos contextos en la implementación de métodos| Por ejemplo, para un método declarado en la interfaz con `method()` luego se lo implementa con `methodDecl()` y se lo asigna en la tabla virtual o estructura de clase con `methodName()`, luego para su redefinición en una clase derivada se utiliza `methodOvldDecl()` y la asignación en la tabla virtual se realiza con `methodOvldName()`, muy fácilmente podemos confundirnos entre ellos. |

### Propuesta de expresión en UML
En la siguiente figura representamos el diagrama de clases UML para el ejemplo de codificación de herencia múltiple.  
<!-- 
@startuml
class Parent{
const char * name
int attribute
void print()
  Parent*const new (char const name[])
  void init (char const name[])
  void copy (Parent const*const parent)
  int class_attr
}

class Parent2{

}


class Child{
int child_attribute
void child_method()
  Child*const new (char const name[])
  void init (char const name[])
  void copy (Child const*const child)
}

Parent <|-- Child
Parent2 <|-- Child
@enduml
-->
![Representación UML para OOPC \label{rep_uml_oopc}](http://www.plantuml.com/plantuml/png/hOyz3i8m38Ltdy9ZMwbBPuPAN80TeIeaaHefFrNYWb3XxX0d0rsOsDel_kptnaGYqkeiI2jImfE8sjC6ClX4A6SHiKCld0RZ2GLHDDULDDo2KRZ43snQmAfrnVBwZWsh_C_w-PB7s3BUqAzt6PO7DcLZcQXx5qPvBcVXl5DE12_OfHysoAYousoise2L_3djIrJeDCr1SHSM_bc55-wQyCqsXwmr3y--BuSh6RxHgBrQdNq3){width="70%" height="70%"}
\ 

De acuerdo al manual de referencia de UML  [@UML2017], los atributos y métodos privados se marcan con el atributo `visibility` en `private`, y los públicos con `public`, si son constantes entonces el atributo `isReadOnly` debe ser `true` y si son de clase `isStatic` (por ejemplo `class_attr`).
Los métodos constantes contienen el atributo `isReadOnly` (por ejemplo `print()`) en `true`, los abstractos  `isAbstract` (no se genera implementación para el método), los no polimórficos `isLeaf` (por ejemplo `init()` y `copy()`) y los de clase `isStatic` (por ejemplo `new()` ).
Además, las clases pueden ser abstractas (no proveen el método `alloc()`) con el atributo `isAbstract` en `true` este podría ser el caso por ejemplo de `Parent2` (manteniendo el ejemplo de codificación dado).
Todo esto se puede especificar para OOPC.

El siguiente diagrama representa la propuesta de representación en UML de las clases genéricas en OOPC.  
<!-- 
@startuml
class gParent<gType1: PrimitiveType>{
gType1 attribute
void method(gType1 arg)
}

class gParent2<gType2: Class>{
gType2 attribute
void method(gType2 arg)
}


class gChild{

}

gParent <|-- gChild
gParent2 <|-- gChild

gChild <|-- ip_gChild : gType1 -> int, gType2 -> Parent
gChild <|-- dp_gChild : gType1 -> double, gType2 -> Parent
@enduml
-->
![Representación UML de genéricos para OOPC \label{rep_uml_genericos_oopc}](http://www.plantuml.com/plantuml/png/VOv1oeCm44RtSufPjb0NpL9412_WeliIcw03YKgS2CNsxYKa-K6GVnNU6xv7cfMbOsyDl8nSLnmwwVJ4rV1uB_fUOkV84jEc0zSxH8-IsL7lMSCsaqAhUPpL9H_TS8Kl78iY9aM9RT0v9VvBYRzKRhKZ6RL3KAcBrQSeqY4xSP0Gt-XeUIOiCNstg94claKK0MFbi5EdEpNxtkYJQQCdvQtv0G00)
\ 

La clases genéricas pueden heredarse por otras. También pueden ser instanciadas en clases concretas asignando un tipo concreto a sus tipos genéricos. En el ejemplo, la relación entre `gChild` y `ip_gChild` o `dp_gChild` no es de herencia sino de vinculación de elementos genéricos (template binding), donde se asigna el tipo concreto. Las clases vinculantes (`ip_gChild` y `dp_gChild`) no pueden declarar miembros nuevos, pero sí lo podría hacer una clase que herede de cualquiera de ellas (esto permite no tener que obligar esa herencia para permitir la declaración de nuevos miembros como lo permite UML). Las clases vinculantes deben terminar con el nombre de la clase genérica siendo el prefijo restante el asignado a la macro `gTypePrefix`.
Ninguna de las dificultades enumeradas en la sección anterior aplican para estos diagramas.


## Object-Oriented  C de Tibor Miseta (ooc)
### Introducción
El propósito de OOC es facilitar la POO en microcontroladores pequeños y para que el programador C aprenda conceptos de orientación a objetos. Está escrito bajo ISO C89.
Junto con la biblioteca del sistema de objetos y macros para facilitar la codificación, provee una herramienta para facilitar la creación de clases, interfaces y mixins desde templates u otras clases ya implementadas. El autor escribe acerca de la codificación de estos artefactos:  
<!--
"Creating ooc classes by typing from scratch may be labor-intensive, error prone, but mostly
boring".
-->
"Crear clases en ooc escribiéndolas desde cero puede requerir mucho trabajo, es propenso a errores, pero principalmente aburrido"[Tibor2017].
Lamentablemente esta herramienta no facilita la inicialización de la tabla virtual ni la implementación de miembros de clase o instancia como sí lo puede hacer un generador de código desde UML.  


### Conceptos soportados
1. Encapsulamiento
2. Herencia
3. Polimorfismo
4. Interfaces (sin herencia de interfaces)
5. Mixins (sin herencia de mixins)
6. Excepciones


### Modelo de objetos


#### Clases
El siguiente diagrama nos muestra las estructuras que dan soporte a las clases en ooc.   
<!--
@startuml
class oocType << (S,orchid) >> 
{
	const char * name
}
hide oocType methods

enum ooc_TypeID {
 _OOC_TYPE_CLASS
 _OOC_TYPE_INTERFACE
 _OOC_TYPE_MIXIN
}

hide ooc_TypeID methods

oocType  *-- "1" ooc_TypeID: value >

class Class as "ClassTable,*Class" << (S,orchid) >>
{

	const size_t size
	const Class parent
	const size_t vtab_size
	Itable itable
	const size_t itab_size
	

	void (* init) ( Class this );
	void (* ctor) (Object self, const void * params );
	void (* dtor) (Object self, Vtable vtab);
	int (* copy) (Object self, const Object from);
}

class Vtable as "BaseVtable_stru,*BaseVtable,*Vtable" << (S,orchid) >> 
{
	_class_register_prev;
	_class_register_next;
	int (* _destroy_check )( Object )
}

Vtable o-- "1" Class :  _class >

class ClassCommonsTable as "_ClassCommonsTable,ClassCommonsTable,* ClassCommons" << (S,orchid) >>
{
	
void (* finz)( ClassCommons this ) /* class finalizer */

}

oocType	"1" --*  "1" ClassCommonsTable :	< type 
Vtable	"1" --o  ClassCommonsTable :     < vtable

ClassCommonsTable "1" -up-* "1" Class : c <

class BaseObject as "BaseObject,*Object" << (S,orchid) >>											
hide BaseObject methods

Vtable -down-* BaseObject :	< _vtab

class InterfaceID as "InterfaceID_struct,*InterfaceID" << (S,orchid) >>


InterfaceID *-- oocType : type >

class Itable as "InterfaceOffsets_struct,*Itable" << (S,orchid) >>
{
	size_t vtab_offset
	size_t data_offset

}

Itable o-- InterfaceID : id >

Itable "*" -up-o Class : itable <
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/RLHDZzem43tZNp7AYLY9Lhso4TejfLAaTgaAghOdooHESPl4o7PesHN_lN6CdK320SVFRsRUVDXFIbEfcwf4MKcL0Y6otVd884bWles4p0uy3s6v1FIEPfcebORiG2LWg6d5q0MrvynRLKmVHAuGOdLJ6PGOEFq4xmZ8PhCYkv_VrcJrvNcx7GBfosxz_VFpQZq4lwQlwKiRm4TmldmG5nLm74Fm8HYG5d2YPSDWYQvvhRf_gY3eldPqNx88Tz_1ADasLvUiuc-CsCL1rjMHIbRhEzf9qpsnt5IR4C2xvOx6UreRw2Hu3dCClEOwXFdLlpvm1U5ZVvnf8TlZpVutopGeLXOHMAyT0nj5jBgroITiVbXXHggXybfttiNnFEtz2XLIL2t_uWfgtNGL_KWLirkYj6mYt0CHjcimFL2ayqKa-yMLPf8S9JizZk6Q_TEzL9ApDemua-p0iZyGpft4qAYxwX9s9Ax5N01OdtS3iH9LrIQvywcG4HoD4JmqdXwV6N8jA7Zz5iw75gwty831Qcavj6p7GG9-GEZY9tjcCeXZ33QLaUB5B07Tyi1cxVW2fhXWVWcSx42YCQCpReunlYbS1eahcMchBRLlkzr6sAxZKipwdxt7lP6xnxvbSIx-rZ4UKao2n2Xs4j9QCrdGpBm3HiDWtysUqJB09dg3qD07ZcF_U2riBVs4s1jy6sPJ58ffvMDPplG83Dy4qTbvBAUQEipqE-s7TgXk0Jo7fJyEi6sGyCtXzY11JwpEcwhy3m00)
\ 


La estructura base de la cual todas las estructuras de instancia de clases heredan es `BaseObject`. Como en los frameworks anteriores, la herencia se consigue incluyendo a la estructura de la clase padre como primer miembro de la estructura de la clase derivada.  
 `BaseObject` contiene una referencia a una tabla virtual (con estructura `BaseVtable_stru`), de las cuales también heredan las tablas virtuales de otras clases para declarar sus métodos polimórficos.
La tabla virtual referencia a la estructura que representa a la clase y que sirve de estructura de RTTI, contiene los constructores y destructores de clase (para instanciar y liberar la clase) y de instancia. El nombre de la clase y su tipo (para una clase `_OOC_TYPE_CLASS`). También incluye un listado de interfaces y mixins (qué es un mixin se explicará a continuación) a través de la referencia al arreglo itable, el mismo nos dice donde se encuentran los métodos definidos para la interfaz dentro de la tabla virtual (a través del atributo `vtab_offset`) y en caso de tratarse de un mixin, dónde se encuentran sus datos dentro de la estructura de la instancia de clase (a través del atributo `data_offset`). Qué mixin o interfaz se está implementando se asigna en la referencia `id`.


##### Clases Base
Para entender como funciona el modelo de objetos veremos las estructuras e instancias que se crean para una clase base de ejemplo (`Parent`).
Las estructuras que ya introducimos aparecen en gris.
<!--
@startuml
class Vtable as "BaseVtable_stru,*BaseVtable,*Vtable" << (S,orchid) >>  #lightgray
{
	ClassCommons	_class_register_prev;
	ClassCommons	_class_register_next;
	int   	(* _destroy_check )( Object )
}

class BaseObject as "BaseObject,*Object" << (S,orchid) >>	 #lightgray										
hide BaseObject methods




object "ParentVtableInstance:ParentVtable_stru" as ParentVtable_stru {
[(*methods)()]
}

ParentVtable_stru  *--  "1" Vtable : Base

class ParentObject << (S,orchid) >> 
{
[data]
}


ParentObject  *--  "1" BaseObject : Base >

ParentObject  --  "1" ParentVtable_stru : Base._vtab >

object "ParentClass:Class" as ParentClass {
c.type.value = _OOC_TYPE_CLASS
c.type.name = "Parent"
c.finz = Parent_finalize
c.vtable = &ParentVtableInstance
size = sizeof( struct ParentObject )
parent = & BaseClass  
vtab_size = sizeof( struct ParentVtable_stru )			
init = Parent_initialize
ctor = Parent_constructor
dtor = Parent_destructor
copy = Parent_copy
}

ParentClass -- ParentVtable_stru : < Base._class
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/XLBHYjim47oslc9neTWc2VGrJSEreG-5GWvI2gKKePFt4hMsP2GbD7VqtwkrrDFb7AWU8dPcF9dTrOtpmlfZtp7P2UVWgnTt7O9mK7uG3cF9dRV7MPE1MHFl4bOhg7OpO-L1jJMirm2lEhK_-BqLP_R8YWsvRapV6-qABgdY5lVAURHyi7Xw-n-Dnjy-Q9Js051K3V0MGnnpvlA0yXVK5MplVgBqKBC_B7L1IHDAdUHoriHx6hnu5hnuEYom-DokHtymhMDqJ8JAMs5H-pYHJzfveIKkCvZcLrAK2GgFx7lL9DUwgdzG4rCLDFCvGFccX8Z2SWoL-assCU9q9IpyHoky6BtPXJRxvXwJDwnVQfDqsaJwOi5F0OFroycC-rsElta84GtjouK_3xWuYUw8y0xuThlXNxxTVkIRp-ztkt-q5ZsnoR4CyBtI3m6900-LwDG31e92T2H-VMqnp0LL8EaoznLGVkal9rYpOIp9O-miPWL6rlowmNGeDRqVfPNFAQbIAQStDXFIQFg6GDPUC2q-4T8CvqnGbHzBJ3YVNrtEAgr7aeZTe6wFVVSN)
\ 


Como vemos en el diagrama hay dos herencias de estructuras. Una de `ParentObject` con `Object` de la cual hereda su referencia a una tabla virtual y se extiende declarando sus propias variables de instancia. Y otra de `ParentVtable_str` con `BaseVtable_str` que le permite usarse como una tabla virtual y extenderse definiendo nuevos métodos de instancia y métodos y atributos de clase.
Esta tabla virtual referencia a su propia instancia de la estructura Class donde se definen miembros comunes a todas las clases. El atributo `parent` de la clase, que referencia a la clase padre de la clase `Parent`, referencia a `BaseClass` lo que indica que esta es una clase base. Una clase que herede de `Parent` directamente tendría una referencia a `ParentClass` en este atributo.

Esta clase no realiza interfaces o hereda mixins, los que veremos a continuación.


#### Interfaces
Las estructuras en instancias que dan soporte a una interfaz son muy sencillas. El siguiente diagrama nos muestra un ejemplo con una interfaz llamada `Serializable`

<!--
@startuml
class SerializableMethods <<(S,orchid)>>{

[(*methods)()]

}

object "SerializableID:InterfaceID_struct" as SerializableID{											
		value=_OOC_TYPE_INTERFACE									
		name="Serializable"								
}
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLWXEBSfCpCciIapAIVLDBSd8JobMiB7H2DR9BqhEo4pHjBEhvkAArj3AXSXfQcZ6Sd7LSd7b9sMb9fSeA27hztInyimhIIrAIqm6ikEBIufAaqkK51AB5L1LLNCY01SdPrbYJccgRRo_ltDyI6I0QxodNuXha9kZiokoehp4t5HR5DkKuBArN0wfUIb0Fm40)
\ 


La instancia de `InterfaceID_struct` sirve para identificar a las instancias de `SerializableMethods` como tales.
Si deseamos realizar esta interfaz en la clase `Parent` deberíamos modificar su tabla virtual y estructura de RTTI. El siguiente diagrama nos muestra estas modificaciones.

<!--
@startuml
object "ParentVtableInstance:ParentVtable_stru" as ParentVtable_stru {
[(*methods)()]
}


class ParentObject << (S,orchid) >> #lightgrey
{
[data]
}


ParentObject  --  "1" ParentVtable_stru : Base._vtab >

object "ParentClass:Class" as ParentClass {
itab_size = sizeof(itable)
[...]
}

ParentClass -- ParentVtable_stru : < Base._class


class  SerializableMethods << (S,orchid) >> {

[(*methods)()]

}

ParentVtable_stru "1" *-- "1" SerializableMethods : Serializable 


object "SerializableID:InterfaceID_struct" as SerializableID{											
		value=_OOC_TYPE_INTERFACE									
		name="Serializable"								
}


object ParentItable{
}

ParentItable "1" --o "1" ParentClass : itable

object ":InterfaceOffsets_struct" as InterfaceOffsets_struct{
vtab_offset = posición Serializable en tabla virtual
data_offset = 0
}

ParentItable "1" *-- "*" InterfaceOffsets_struct

InterfaceOffsets_struct "1" o-- "1" SerializableID
@enduml
-->

![enter image description here](http://www.plantuml.com/plantuml/png/TL9DRvmm43tl_8eHlS2gHkqLBQkqcwt4eILgeafLL247JDQHiIlR83Kelwe_eN-iODfW4fO3WzvylFTck32MQjis0jJT0oij1bTKCsc_MNedM2Q7j2nPueE5iReDa1fyXM8Fj-6sOVQaAXE5qGzu0e1IKFE_E9zOTZiChzygNPvu5U5-Zsy4lp_PUyr-mJ2ZefPElOik90GnU1-i42VuaHeM5zs0uHvUk3cC2XBtTi8z57lWGqzX-2F35CUWwf2xqH7Snd7iXFWTXAmAsFsJuEm-syPhfZaL_74i_JnjvhNz7bukRYRrM9pvBI4khWrE5YZ2l0WVpowJJ5gcQreEtsvoQTrYbbNzPdvWi-ceQ5bQvFcXkFb-TIooBpV7hvy-78v-aQGDInTimNFsQTOpMSkSjNvC-82pHuXoqTzyWjDXPbkpaRok3RF6TtCcrqCtNbmv45FygGml-P_VShay9d4aezXnRLigOFmhvwvtgwhTSRR1EMg0CmdNh5Okcrt21PDLsuY_)
\ 


<!--
según wikipedia los mixins son heredados
-->
#### Mixins
Los mixins son muy parecidos a las clases pero en vez de instanciarse, son heredados por una clase. A la vez agregan una interfaz a la clase contenedora del mixin por lo que son como una Interfaz que define parcial o totalmente su implementación.
El siguiente diagrama nos muestra las estructuras e instancias que dan soporte a un mixin llamado `Countable` (podría llamarse de cualquier otra forma).  

<!--
class CountableFields_ << (S,orchid) >>{
[data]	
}

class CountableMethods << (S,orchid) >>{
[(*methods)()]	
}

class MixinTable << (S,orchid) >>
{
	const size_t		size										/* size of the mixin data */
	void				(* init) ()        							/* class initializer */
	void				(* populate) ( void (** methods)() )			/* populate the interface methods */
	void				(* ctor) (void (** methods)(), void * self)							/* constructor */
	void				(* dtor) (void (** methods)(), void * self)							/* destructor */
	int				  	(* copy) (void (** methods)(), void * self, const void * from) 		/* copy constructor */
}

MixinTable "1" *-- "1" ClassCommonsTable :	c >

object "CountableVtableInstance:BaseVtable_stru" as CountableVtableInstance

object "CountableID:MixinTable" as CountableID = {																							
		c.type.value = _OOC_TYPE_MIXIN														
		c.type.name = "Countable"																																
		c.finz = Countable_finalize										
	    vtable = CountableVtableInstance																														
		size = sizeof( struct CountableFields_ )
		init = Countable_initialize					
		populate = Countable_populate
		ctor = Countable_constructor
		dtor = Countable_destructor
		copy = Countable_copy
	}

-->
![enter image description here](http://www.plantuml.com/plantuml/png/XP7HIiCm58QlawSuxAeLsGF878Efq8iwmI6AICcICnP9azAS3DUnTpUrEDivyToK_j__SKxfp9EeAPI6IoEyXxaBbiJAu9r6etm1amaaZvUkbXkjKfXEz_nL2H9lZ1_ugPCZRPpovvpaeknWcgITwrRlA0b6HpkxkShrXxRBzcK4maCFmPwT7ywO7DEkml5Mc81m3SLYCI-MBm-tHPuzP_T_rQqeOxktVyJEpz1TQzj4xMWLCH16DzZlGPpj5-vNdxfliPw4bVZlBg-RLcyVRfs0fpf8-ls3qbZLLjFWg3RGFsV5IkMgO0JXeFOTHYx9rGCcdUrMkZfYTOeLzgXqrMuenu2p0v-XLQ4qdm00){width="80%"}
\ 


Al igual que una interfaz, se define una estructura terminada en "Methods" (en nuestro caso `CountableMethods`) que a su vez define las referencias a los métodos definida para el mixin, y a diferencia de las interfaces se define una estructura de variables  (`CountableFields_`) definida para el mixin.
La instancia de `MixinTable` (`CountableID`) es análoga a la estructura `ClassTable` de una clase. Incluye la información RTTI del Mixin. Esto permite identificar las instancias de las estructuras `CountableMethods` y `CountableFields_` al asociarlas con `CountableID`. Al igual que una clase, un mixin contiene, métodos de inicialización y finalización del mixin, y métodos de inicialización, copia y finalización de instancia. Las instancias de mixins son componentes de las clases que las heredan.

Un método `populate()` que es el encargado de inicializar la tabla virtual de la clase que instancia al mixin con los métodos implementados por el mixin. Los métodos implementados por el mixin pueden ser redefinidos por la clase que la hereda.


La estructura MixinTable hereda de ClassCommonsTable como se representa en el siguiente diagrama.  
<!--
class MixinTable << (S,orchid) >>
{
	const size_t size /* size of the mixin data */
	void (* init) () /* class initializer */
	void (* populate) ( void (** methods)() ) /* populate the interface methods */
	void (* ctor) (void (** methods)(), void * self) /* constructor */
	void (* dtor) (void (** methods)(), void * self) /* destructor */
	int (* copy) (void (** methods)(), void * self, const void * from) /* copy constructor */
}

class ClassCommonsTable as "_ClassCommonsTable,ClassCommonsTable,* ClassCommons" << (S,orchid) >>
{
	
oocType	type 
Vtable	vtable
void (* finz) ( ClassCommons this ) /* class finalizer */

}

MixinTable "1" *-- "1" ClassCommonsTable :	c >
-->
![enter image description here](http://www.plantuml.com/plantuml/png/bP5BQyCm48JFrgzOShADG-Yr11F8kQVsNbHfZGMIraZhq0VzxvMb6Ee-eBruOJtpxOWvHPQ1PsU5iZ96k3FFnZ_89ujmF49ztr5Gez4DzBru4vKY7ncYUSN7Ck3Gbaa3y8ZW5ZzeoHBQWwWkP3JKBHXlk86wMUJvKDuOQPCrR9GJJRELZ4aDrrqB3daa7Pi4o8XLb4yQpnW6gN2LRNYAAIJM3wYky5D-j4F6vjU5UR5i6FeV38qRH0gNKz3qyWT2LoAigo6GIz1Y_vhkNLmhEo_VCpcN_l_IdFXKw-vc1-r-d-Ttwssbe1Sdz7fszWC0)
\ 


El siguiente diagrama nos muestra cómo quedarían las estructuras de `Parent` al heredar el mixin `Countable`

 <!--
@startuml
object "ParentVtableInstance:ParentVtable_stru" as ParentVtable_stru {
[(*methods)()]
}


class ParentObject << (S,orchid) >>
{
[data]
}

class CountableFields_ << (S,orchid) >>{
[data]	
}

ParentObject "1" *-- "1" CountableFields_ : Countable


ParentObject "*" o--  "1" ParentVtable_stru : Base._vtab >

object "ParentClass:Class" as ParentClass {
itab_size = sizeof(itable)
[...]
}

ParentClass "1" --o "1" ParentVtable_stru : < Base._class


class  CountableMethods << (S,orchid) >> {

[(*methods)()]

}

ParentVtable_stru "1" *-- "1" CountableMethods : Countable 


object "CountableID:MixinTable" as CountableID{																		
}


object "ParentItable:InterfaceOffsets_struct[]" as ParentItable << array >>{
}

ParentItable "1" --o "1" ParentClass : itable

object ":InterfaceOffsets_struct" as InterfaceOffsets_struct{
vtab_offset = posición de Countable en tabla virtual
data_offset = posición de Countable en ParentObject
}

ParentItable "1" *-- "1" InterfaceOffsets_struct

InterfaceOffsets_struct "1" o-- "1" CountableID
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/ZLB12jim33rNNo5oIieIs3MqfQnZa4FfO6ENKeAREDGZjOVjb6rXNxLFs8yjLjR6n0JMGzq-wKbFJzeRoxJjxYsewrTUMOm-Cisb_MBPjUM57CAouhaFbiRgBa9cC42nXtEyldDxKxL9ukG2lm2WQfbv9f_6BfiDnf_UA5tTH9tWRWS3iMQM4M7CFwXEKkKFWhUrAGFEax9o7BywHcyZNASflK6PV89Wpbj7g0OU4SFXSdp73C_Anu3X3cQE7PpgdBx970_57iJ0AOtuoN6Bxb5DBAXq0kSiosXkZq4AqbGjAjdyqq9UlJoUHZkEEmXCmnxcAwBMGOlGmwcgxo72PCGBBDxdH_5ToC_k3xdXXVfLy74IPdOMb9qNqdBTi8gVciPmQqXSPSyNgkgdkb6Prkm7dGRDv0LzFtsVSnpNCBLVQaaD5s8zk9ie5O6unM_AY4hy-IsnvfvLNABxmV0XjErO2-v-_uVbNsaushIf9Nq02m4YgP7ixmZsNDRTlVqB)
\ 

La estructura `ParentObject` contiene los atributos de instancia. También contiene los atributos definidos por el mixin bajo el nombre `Countable`.
La tabla virtual de `Parent` contiene los métodos definidos por el mixin también bajo el nombre `Countable`, al igual que con una interfaz.
El arreglo `ParentItable` contiene una instancia de la estructura `InterfaceOffsets_struct`, la misma relaciona a la tabla que describe al mixin (`CountableID`) con la posición del mixin dentro de la tabla virtual (`vtab_offset`) y la posición del mixin dentro de las variables de instancia (`data_offset`). De esta manera desde la estructura de RTTI de la clase `Parent` se puede obtener.


### Codificación


#### Visibilidad {#Visibilidad}
La visibilidad de las clases y mixins en ooc son de dos tipos: de declaración (encapsulación fuerte) o de implementación y dependen del archivo de interfaz que incluyamos. Los dos archivos de interfaz, por convención, tienen el nombre de la clase (por ejemplo `clase.h`). La interfaz para obtener visibilidad de implementación se encuentra, por convención, en la carpeta `implement`. Las variables de instancia son accesibles bajo la visibilidad de implementación. Las clases heredadas acceden, en su implementación, a sus clases padres bajo la visibilidad de implementación.


#### Clases


##### Interfaz de declaración  


Las clases se declaran a través de la macro `DeclareClass`. El primer parámetro de la macro es la clase a declarar y la segunda la clase de quién hereda. Si se trata de una clase base se debe utilizar como segundo argumento de `DeclareClass`  la palabra `Base`:
```C
//Parent.h
DeclareClass( Parent, Base );
```
La tabla virtual se declara con con las macros `Virtuals` y `EndVirtuals`. Contiene la declaración de métodos virtuales, variables de clase y los métodos de las interfaces (tanto interfaces comunes como mixins) a través de la macro `Interface`:
```C
//Parent.h
Virtuals( Parent, Base )
	void	(* method)  ( Parent );
	int		classVariable;
	Interface( MyMixin );
	Interface( MyInterface );
EndOfVirtuals;
```


##### Interfaz de implementación  


Las variables de instancia y así como las variables heredadas de los mixins (a través de la macro `MixinData`) se declaran con las macros `ClassMembers` y `EndOfClassMembers`:
```C
//implement/Parent.h
ClassMembers( Parent, Base )
	int		instanceVariable;
	MixinData(MyMixin);
EndOfClassMembers;
```

##### Implementación (.c)  


Las posiciones de las interfaces en la tabla virtual son registradas en un vector especializado mediante las macros `InterfaceRegister`, `AddMixin` y `AddInterface` :
```C
//Parent.c
InterfaceRegister( Parent )
{
	AddMixin( Parent, MyMixin ),	
	AddInterface( Parent, MyInterface )
};
```
La tabla virtual es instanciada mediante la macro `AllocateClassWithInterface`, en caso de que la clase no realice interfaces o mixins se utiliza la macro `AllocateClass`:
```C
//Parent.c
AllocateClassWithInterface( Parent, Base );
```
Existen una variedad de funciones que deben implementarse para cada clase: inicialización y finalización de la clase, constructor y destructor de instancia y constructor de copias. En la inicialización de la clase se debe inicializar la tabla virtual con la implementación de los métodos declarados en la misma (sólo para los métodos en los que la clase defina o especialice al método, los métodos heredados son automáticamente inicializados en la tabla virtual por los ancestros de la clase):
```C
static
void
Parent_initialize( Class this )
{
	ParentVtable vtab = & ParentVtableInstance;
	((ParentVtable)vtab)->method = Parent_method;
	((ParentVtable)vtab)->MyInterface.myInterfaceMethod = 
	           (void(*)(Object self))Parent_myInterfaceMethod;
```

#### Interfaces
Las interfaces no contienen variables de instancia por lo que no contienen un archivo header adicional.


##### Interfaz (.h)  


La declaración de la interfaz se realiza con la macros `DeclareInterface` y  `EndOfInterface` declarando los métodos de la misma:
```C
//MyInterface.h
DeclareInterface( MyInterface  )
	void (*myInterfaceMethod)(Object self);
EndOfInterface;
```  


##### Implementación(.c)  


Por último se debe instanciar la estructura de RTTI de la interfaz en cualquier archivo, por ejemplo uno particular para la interfaz:
```C
//MyInterface.c
AllocateInterface( MyInterface );
```


#### Mixins
Se componen de los mismos tres archivos que las clases (o sea uno de implementación y dos de interfaz).


##### Interfaz de declaración  


Para declarar la interfaz del mixin se utiliza la macro `DeclareInterface` y `EndOfInterface` declarando los métodos de la misma:
```C
//MyMixin.h
DeclareInterface( MyMixin  )
	unsigned int (*myMixinMethod)(Object self);
EndOfInterface;
```


##### Interfaz de implementación.  


Los datos miembros del mixin se declaran entre las macros `MixinMembers` y `EndOfMixinMembers`:
```C
//implement/MyMixin.h
MixinMembers( MyMixin )
	unsigned int instanceNum;
EndOfMixinMembers;
```

##### Implementación (.c)  


La instanciación de la estructura de RTTI del mixin se realiza en el archivo source con la macro `AllocateMixin`:
```C
//MyMixin.c
AllocateMixin( MyMixin );
```
Junto con esta macro deben implementarse varias funciones para el mixin: inicialización y finalización de la estructura de RTTI del mixin, constructor, constructor copia y destructor de la instancia del mixin en la clase (estas se llaman automáticamente al crear, copiar o destruir la instancia de la clase) y por último un método `populate` para actualizar la tabla virtual de la clase con los métodos que vayan a implementarse en el mixin mismo:
```C
//MyMixin.c
static
void
MyMixin_populate( MyMixin mymixin )
{
	mymixin->myMixinMethod	=	MyMixin_myMixinMethod;
}
```


### Dificultades en la codificación
| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores  | Inicialización de métodos polimórficos | Contrario a otros LPOO, a cada referencia de un método debe  asignársele su implementación en la función de inicialización de la clase (en el método `initialize()`), si esto se olvida generaremos un error crítico al enviar un mensaje con el método no inicializado. Si se trata de un mixin esto se debe hacer en el método populate.|  
|  2 | Propensión a errores y obtención de información | Asignación de métodos redefinidos en la tabla virtual | Para poder asignar un método ya definido por una clase padre se debe conocer exactamente cuál de los padres es el que definió el método por primera vez, y en caso de tratarse de una interfaz se debe conocer cúal es y cuál de los padres la realizó por primera vez, además de que en caso de equivocarnos recibiremos una advertencia por parte del compilador, esto agrega una demora al programador que debe obtener esta información.|  
|3|Repetición de información|Cada método polimórfico nuevo implica 4 referencias al mismo y una redefinición de método implica 3| Si es nuevo se lo referencia al incluirlo en la tabla virtual y tanto si es nuevo como redefinido: en su implementación, y dos veces al asignar la implementación a su referencia en la tabla virtual.|
|4|Repetición de información|Repetición en la información de la herencia| En las macros `DeclareClass`, `Virtuals` y `AllocateClass`, lo mismo pasa en el método `constructor()`  al tener que llamar al mismo método de su padre.|
|5|Dificultad de aprendizaje| Nuevas macros que aprender| Hay una importante cantidad de macros a aprender y para utilizar correctamente el framework.|
|6|Repetición de código | Repetición en cada clase| comparado con otros LPOO, hay mucho código repetido entre una clase y otra tan solo para definir la clase y su herencia, repitiendo varias veces el nombre de la clase como argumento de distintas macros |
|6|Repetición de información | Duplicación de información al realizar  una clase a una interfaz | El hecho de que la clase realiza a una interfaz se debe declarar bajo la macro `Virtuals` y con la macro `InterfaceRegister`.|
|7|Repetición de información | Triplicación de información de herencia de mixins | Bajo la macro `Virtuals` se declaran los métodos del mixin en la clase, bajo la macro `ClassMembers` se declaran las variables del mixin en la clase y mediante la macro `InterfaceRegister` se lo referencia en la estructura RTTI. |

`ooc` ofrece una herramienta para crear clases en base a plantillas para mitigar muchas de estas dificultades (específicamente las dificultades número 3 y 5 y en parte la 4)

### Propuesta de expresión en UML
<!--
@startuml
class Parent
{
	type attribute
	type class_atribute
	type method()
	type class_method()
}

class Child
{
	
}

Parent <|-- Child

Interface MyInterface1{
	type method()
}

Interface MyInterfaceN{
	type method()
}

MyInterface1 <|.. Parent
MyInterfaceN <|.. Parent


Interface MyMixin1 <<mixin>> << (M,green) >>
{
	type attribute
type class_attribute
	type method()
	type class_method()
}

MyMixin1 <|.. Parent

Interface MyMixinN << (M,green) >>
{
type atribute
type method()

}

MyMixinN <|.. Parent
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/bL712i8m33tRh-Xn0pVmFih0auSD_q3g5bsXgz9bu53_tKwxqK0ltjBavRsNrt90QN7iDMkq70P-b1OCiYVRu7G7BX6jEey8_lq5dIHjze3ThKrIWbcRR-QfzvtIxSmyjtuwl7XbcH-mWq6m5za0hwQrtYrEGhueieuXGoOdbkVBWU4Y6L3sIZsKSPj5FnT2k8edrVPg0KpAXOW4HNBwCoWlIAn63DKH6zw5rwDo0JKzjWJJkh__0000)
\ 


Cualquier clase puede realizar una o varias interfaces, heredar uno o varios mixins, y heredar de hasta una clase. Todas las combinaciones de estas posibilidades son permitidas. 
Cualquier método en una clase o mixin puede definirse como polimórfico o no (mediante el atributo `isLeaf`), si es de clase o no (mediante el atributo `isStatic`), si es constante o no (mediante el atributo `isConst`), y si es abstracto o no (mediante el atributo `isAbstract`). Las interfaces sólo definen métodos abstractos, pero pueden ser definidos como constantes.
Los mixins se representan mediante una interfaz. La misma debe contener atributos o el estereotipo `<< mixin >>`. De esta manera se distinguen de las interfaces.
Los atributos de una clase o mixin pueden ser de clase (mediante el atributo `isStatic`) y constantes (mediante el atributo `isConst`).
Los atributos de visibilidad son ignorados. Las opciones de visibilidad para `ooc` ya fueron explicadas en la sección [Visibilidad](#Visibilidad).



## Object Oriented C - Simplified (OOC-S) de Laurent Deniau


### Introducción
<!-- fuente
Object Oriented C - Simplified release is an oversimplified version of OOC-2.0 with about 300 sloc (ultra light!), which provides some very simple coding guidelines and programming techniques to allow C programmers to write object oriented program nearly as in Java. OOC-S provides some object oriented features like class (i.e. TYPE and INTERFACE), single inheritance of class, attribute access control, java-like interface, interface default method implementation, multiple inheritance of interfaces and ownership management (including autorelease). OOC-S is not a framework and minimizes the use of macros (one per class) and typedefs (none) and therefore requires some manual coding (class initialization) and some care from the programmer. Following OOC-S templates or examples with all compiler warning options enabled should avoid most common mistakes and forgetting. One can still build some macros on top of OOC-S to automatize some part of the coding. OOC-S requires a C89 compiler.
-->
OOC-S busca ser una versión simplificada de OOC-2.0 [@OOC2], un framework en C basado en JAVA y abandonado para el desarrollo de COS. Está escrito en unos pocos cientos de líneas y proporciona algunas pautas de codificación y técnicas de programación muy simples para permitir a los programadores de C escribir programas orientados a objetos casi como en Java. OOC-S proporciona gestión de propiedad (incluida la liberación automática soportada en varios threads con TLS, opción no soportada por la mayoría de los sistemas altamente restringidos por lo que requerirá una modificación para poder ser usado en los mismos). OOC-S requiere un compilador C89.
El autor ya nos advierte de sus dificultades de programación: "minimiza el uso de macros (uno por clase) y typedefs (ninguno) y por lo tanto requiere algo de **codificación manual** (inicialización de clase) y algo de **cuidado por parte del programador**. Seguir las plantillas de OOC-S o los ejemplos con todas las opciones de advertencia del compilador habilitadas debería evitar los **errores y olvidos** más comunes. Todavía se pueden construir algunas macros sobre OOC-S para automatizar parte de la codificación". En esta tesis proponemos un generador de código UML en vez o además de macros para sortear estas dificultades.


### Conceptos soportados
1. Encapsulamiento
2. Herencia
3. Polimorfismo
4. Interfaces


### Modelo de objetos
OOC-S posee un modelo de objetos muy simple. En el siguiente ejemplo mostramos una clase `Parent` que hereda de la clase base `Object` y realiza una interfaz `interface_example`  
<!--
@startuml
class object as "object_attribute" << (S,orchid) >>{
unsigned refcnt
}

class object_interface as "struct object_interface" << (S,orchid) >>{
  struct super_interface *super
  void*  (*interface  )(object*,void*)          
  void*  (*cast       )(object*,void*)         
  int    (*equal      )(object*,object*)         
  size_t (*hash       )(object*)            
  object*  (*alloc      )(size_t)        
  void   (*free       )(object*)
  object*  (*retain     )(object*)
  void   (*release    )(object*)
  object*  (*autorelease)(object*)
  object*  (*init       )(object*)
  object*  (*deinit     )(object*)
}

object "*" o-- "1" object_interface : _

class parent as "parent_attribute"<< (S,orchid) >>{
attributes
}

parent "1" *-up- "1" object



object "object._:parent_interface" as parent_interface{
(*method1)(parent*)
}

parent_interface *-- "1" object_interface

parent -- "1" parent_interface

class interface_example << (S,orchid) >>{
   (*method2)(interface_example*)
}


parent_interface *-- "1" interface_example

object "interface_example_interface:interface_example" as interface_example_interface
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/XLFRQlCm3BxtKuXS9QOTT9UbbBx37Y2uZhfuE4vcosEizDt_BIA7n6t_tDZeE-YJKKw1bAVOMA6j2W7QymTg0XKWuskfYBmvHy8C3WV8tpQjrxMf2ZWUhoAwODuTLk3neXs9crZubCOH-elIs3i6yb5JWjqp1c0oXDYXd_d8lY00lbfJIO1SJXWKENlBJOyMC7vpWLQ1k9e8vdpZUbOkyJCgk-RpkLG4yuCb_Ifg5UgnGy9aCfVxGChQLWzqDYbMmViW5u-Os2wjF98oBgMC7Xujge3FF5IabcduY6ASeVyagP19IyfD2Bv39ZDejrl8TbcwBNieXqtgb4V76ydNsKQcUpDYuQyL2_eMSXkxUJCXfYHylfHxviytKmLO5wyYbmrItLQx8cUG1riJGJuORmp6UD9X67qibFYjcixYtHy5XZYlHPueEDdZQ8bWUfK4ckJxDSOlzKGXJkYgsDX_)
\  


La clase base de todas las clases es `object`, la misma contiene un único atributo (`refcnt`) que sirve para la gestión de propiedad o de memoria y una referencia a una tabla virtual con varios métodos y a la tabla virtual de la clase padre (para la de `object` es nula). Toda clase definida por el programador (como en el diagrama `parent`) hereda de object, referencia a una tabla virtual (por ejemplo `parent_interface`) que puede definir nuevos métodos así como incluir interfaces (como `interface_example`), las interfaces son referenciadas mediante una única instancia a la interfaz cuyo nombre es el de la interfaz seguido de `interface` (como `interface_example_interface`).
Para obtener la interfaz implementada en un objeto (por ejemplo la interfaz `interface_example` implementada por `parent`) se realiza una llamada al método `interface()` con la referencia a la interfaz. Por ejemplo para obtener la implementación de la interfaz `interface_example` de la clase del objeto referenciado por una variable `obj` se debería ejecutar `obj->i->interface(obj,interface_example_interface)`. Entonces la implementación dada por el usuario del método `interface()` debe, al recibir como segundo argumento la instancia `interface_example_interface`, devolver la instancia de `interface_example` dentro de su tabla virtual.


### Codificación
Mostraremos la implementación de la clase `parent` y de la interfaz `interface_example` del ejemplo anterior.


#### Clases


##### Visibilidad	{#Visibilidad-OOCS}
La visibilidad de las clases en ooc-s es de dos tipos: de declaración (encapsulación fuerte) o de implementación y dependen del archivo de interfaz que incluyamos. La visibilidad de interfaz se obtiene incluyendo el archivo que tiene el nombre de la clase (por ejemplo `clase.h`). La visibilidad de implementación se obtiene incluyendo el archivo que tiene el nombre de la clase seguido por `_p` (por ejemplo `clase_p.h`). Las variables de instancia son accesibles bajo la visibilidad de implementación. Las clases heredadas acceden, en su implementación, a sus clases padres bajo la visibilidad de implementación.


##### interfaz (.h)
```c
#include "object.h"
#include "interface_example.h"

#define TYPE      struct parent
#define INTERFACE struct parent_interface

#define PARENT_INTERFACE                         \
        OBJECT_INTERFACE                         \
        INTERFACE_EXAMPLE_INTERFACE              \
		void (*method1)(TYPE*);

TYPE {
  INTERFACE *i;
};

INTERFACE {
  struct object_interface *super;
  PARENT_INTERFACE
};

TYPE      *parent_new      (void);
INTERFACE *parent_interface(void);

#undef TYPE
#undef INTERFACE
```
Se puede apreciar, en esta especificación, otra forma de implementar la herencia de estructuras que es mediante un listado de los miembros de la estructura referenciados por una macro. Esto simplifica enormemente la navegación en la estructuras heredadas. En este caso simplifica para el implementador de la clase la inicialización de la tabla virtual y como veremos simplifica también el mapeo a UML. Esto mismo puede hacerse elegantemente bajo ISO C11 con las estructuras anónimas como vimos en la especificación de Ben Klemens, pero esto es realizable incluso bajo ISO C89. 


##### Interfaz de implementación (_p.h)  
Esta interfaz es la que provee la visibilidad de implementación haciendo visibles los atributos de la clase. La siguiente interfaz sigue al ejemplo anterior.  
```c
#include "parent.h"
#include "object_p.h"

struct parent_attribute {
  struct object_attribute object;
  /* atributos aquí  */
};
```

##### Implementación (.c)
Explicaremos el ejemplo por partes.
Primero se definen macros de uso común para la clase:
```c
#include "parent_p.h"

#define TYPE          struct parent
#define INTERFACE     struct parent_interface
#define SELF        ((struct parent_attribute*)self)
#define AS_SELF(a)  ((struct parent_attribute*)(a) )
#define SUPER       ((struct object          *)self)
#define AS_SUPER(a) ((struct object          *)(a) )
```
Luego se crea una instancia privada de la tabla virtual de la clase, que puede ser inicializada y obtenida mediante la función `parent_interface()` 
```c
static INTERFACE interface;
```
Luego se define la implementación para parent del método `interfaces()` que es el encargado de devolver las interfaces realizadas por la clase. En el ejemplo si se pide la interfaz `interface_example_interface` se devuelve la posición de esa interfaz dentro de la tabla virtual que corresponde a la dirección del primer método implementado. En caso de que esta clase no realice la interfaz consultada se reenvía la consulta su padre lo que esto continúa hasta llegar a `object`  que retorna con valor `NULL` al no realizar ninguna interfaz.
```c
static void*
parent_interfaces(TYPE* self, void *ref_i)
{
  if (ref_i == &interface_example_interface)
    return &self->i->method2;
  
  /* más interfaces aquí */

  return interface.super->interface(SUPER,ref_i);
}
```
Luego son definidas las implementaciones de los métodos de la clase, en este caso el método `init()` está siendo redefinido.
```c
static TYPE*
parent_init(TYPE *self)
{
  interface.super->init(SUPER);
  /* inicialización de atributos  */
  return self;
}

static void
method1(TYPE *self){}

static void
method2(TYPE *self){}

```
Luego se define una función `parent_initialize()` que es la encargada de inicializar la tabla virtual. La tabla virtual es accesible desde afuera a través de la función `parent_interface()` y en caso de no estar inicializada se llama a `parent_initialize()`. Primero se hace una copia de la tabla virtual del padre, en este caso `object`, en la tabla virtual de la clase, esto permite heredar el comportamiento del padre (la implementación de sus métodos). Luego se realiza una copia de las interfaces por defecto en la posición de la interfaz en la tabla virtual, esto permite heredar los métodos por defecto definidos por la interfaz. Luego a la tabla virtual de la clase se le asignan las implementaciones específicas de los métodos de la clase que pueden sobrescribir los métodos copiados anteriormente, además la referencia a `super`, es decir, a la clase padre es sobrescrita con la correspondiente, en este caso con `object`.
```c
static void
parent_initialize(void)
{
  struct object_interface *super_interface = object_interface();

  memcpy(&interface, super_interface, sizeof *super_interface);
  memcpy(&interface.method2, &interface_example_interface,
	sizeof interface_example_interface);
  
  interface.super        = super_interface;
  interface.interface    = parent_interfaces;
  interface.init         = parent_init;
  interface.method1 = method1;
  interface.method2 = method2;
  /* put parent methods initialization here */
}
```
Por último tenemos las funciones para obtener la tabla virtual y para instanciar un objeto de la clase en memoria dinámica.
```c
INTERFACE*
parent_interface(void)
{
  if (!interface.interface)
    parent_initialize();
  
  return &interface;
}

TYPE*
parent_new(void)
{
  TYPE *self = parent_interface()->alloc(sizeof *SELF);
  assert(self);
  
  self->i = &interface;
  
  return parent_init(self);
}
```

#### Interfaces 


##### Interfaz (.h
```c
#define TYPE      struct interface_example
#define INTERFACE struct interface_example_interface
```
Lo primero que define una interfaz es una macro con un listado de los métodos miembro. Esta macro es incluida en la tabla virtual de las clases que realicen la interfaz.
```c
#define INTERFACE_EXAMPLE_INTERFACE                         \
  void (*method2)(TYPE*);                          \
  /* more interface_example methods here         */         \
  /* TYPE* (*method_example)(TYPE*); */
```
Luego se define un tipo para un objeto que realice la interfaz y una estructura que representa la interfaz, es decir el conjunto de métodos que la componen.  
```c
 TYPE {
  struct interface *i;
};

INTERFACEINTERFACE
};
```
Por último se da visibilidad pública a la variable que identifica a la interfaz ()l`interface_example_interface`), utilizada para obtener la implementación de la misma por parte de cualquier objeto a través del método `interfaces()`. Esta variable, además ,contiene la implementación por defecto de la interfaz (o sea la implementación de sus métodos por defecto).
```c
extern INTERFACE interface_example_interface;

#undef TYPE
#undef INTERFACE
```


##### Implementación (.c)
El archivo de implementación tan sólo consta de la implementación de los métodos por defecto de la interfaz y la inicialización de la variable que identifica a la interfaz y que contiene las implementaciones de métodos por defecto.
```c
#include "interface_example.h"

#define TYPE      struct interface_example
#define INTERFACE struct interface_example_interface


/* put INTERFACE default methods implementation here, if any */
static void method2(TYPE*arg){}

INTERFACE interface_example_interface=
 {method2 }
;
```

### Dificultades en la codificación
| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores  | Inicialización de métodos polimórficos | Contrario a otros LPOO, a cada referencia de un método debe  asignársele su implementación en la función de inicialización de la clase (en la función `initialize()`), si esto se olvida generaremos un error crítico al enviar un mensaje con el método no inicializado. |  
|2|Repetición de información|Cada método polimórfico nuevo implica varias referencias al mismo. |
|3|Repetición de información|Repetición en la información de la herencia| Varias referencias a la clase padre para poder implementar la herencia.|
|4|Dificultad de aprendizaje| Convenciones | Si bien esta especificación no posee macros, sí hay que seguir sus convenciones de codificación para estar incluida en la misma. |
|5|Repetición de código | repetición en cada clase| comparado con otros LPOO, hay mucho código repetido entre una clase y otra tan solo para definir la clase y su herencia. |
|6|Repetición de información | Duplicación de información al realizar  una clase a una interfaz | El hecho de que la clase realiza a una interfaz implica referenciarla en la definición e inicialización de la tabla virtual del objeto que la realiza, así como en el método interfaces.  |

Destacamos que este framework no posee la dificultad de tener que conocer qué clase definió por primera vez un método para poder redefinirlo, esto lo logra mediante la redefinición constante de las macro `TYPE`e `INTERFACE`.


### Propuesta de expresión en UML
<!--
@startuml
class Parent
{
	type attribute
	type class_atribute
	type method()
	type class_method()
}

class Child
{
	
}

Parent <|-- Child

Interface MyInterface1{
	type method()
}

Interface MyInterfaceN{
	type method()
}

MyInterface1 <|.. Parent
MyInterfaceN <|.. Parent
@enduml
 -->
![Propuesta de expresion OOC-S](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLWX8B4hDA-6gvk8igIn8LKWiAIdAJ2ejIONomOhY4r45SrDBCl9JD3HHrC05QxcWHZjdPEQaW4m62K7iKR2fqTM5Id1vvfMa5gKb9gSg-5R2sORLw9RKub3fXqqbiab0o_JqO1v4reWYmSNba9gN0WmG0000)
\


Cualquier clase puede realizar una o varias interfaces y heredar de hasta una clase. Todas las combinaciones de estas posibilidades son permitidas. 
Cualquier método en una clase puede definirse como polimórfico o no (mediante el atributo `isLeaf`), si es de clase o no (mediante el atributo `isStatic`), si es constante o no (mediante el atributo `isConst`), y si es abstracto o no (mediante el atributo `isAbstract`). Las interfaces pueden definir métodos como abstractos o no (o sea que ofrecen una implementación del método por defecto), como constantes o no y como de clase o no.
Los atributos pueden ser de clase (mediante el atributo `isStatic`) y constantes (mediante el atributo `isConst`).
Los atributos de visibilidad son ignorados. Las opciones de visibilidad para `ooc-s` ya fueron explicadas en la sección [Visibilidad](#Visibilidad-OOCS).


## OOC de Axel Tobias Schreiner
### Introducción
<!--
ooc was developed primarily to teach about object-oriented programming
without having to learn a new language. If you see how it is done
in a familiar setting, it is much easier to grasp the concepts and
to know what miracles to expect from the technique and what not.
-->

<!--
This old framework uses the ooc preprocessor written
in A WK [40] to provide a basic object-oriented layer. It relies
on void pointers and requires much defensive programming
to ensure correct use of objects. Besides, it gives full control
over inheritance like in prototype-based languages.
-->
Este famoso framework fue uno de los primeros trabajos en el área de programación orientada a objetos bajo lenguaje C. Fue desarrollado por un profesor universitario para enseñar orientación a objetos sin la necesidad de aprender un nuevo lenguaje aparte de C. En las palabras del autor, si el alumno ve cómo se hace en un entorno familiar, es mucho más fácil comprender los conceptos y saber qué milagros puede esperar de la técnica (la orientación a objetos) y qué no.  
Está basado en punteros `void`. Por esto utiliza bastante programación defensiva para asegurar el uso correcto de los objetos. Esto último significa que verifica que los argumentos pasados por punteros `void` sean realmente objetos. Soporta el reenvío de mensajes (message forwarding) (ver el modelo de objetos) lo que le da mucha flexibilidad y junto con delegaciones <!--explicar que son delegaciones: Given respondsTo(), we can implement delegates: a client object announces
itself as a delegate object to a host object. The host queries the client with
respondsTo() if it can answer certain method calls. If it does, the host will use
these methods to inform the client of some state changes.
Delegates are preferable to registering callback functions and to abstract base
classes for defining the communication between a host and a client. A callback
function cannot be a method because the host does not have an object to call the
method with. An abstract base class imposes unnecessary restrictions on
application-oriented development of the class hierarchy. Similar to callback func-
tions, we can implement for delegates just those methods which are interesting for
a particular situation. The set of possible methods can be much larger.
An application framework consists of one or more objects which provide the
typical structure of an application. If it is well designed, it can save a great deal of
routine coding. Delegates are a very convenient technique to let the application
framework interact with the problem-specific code. (ooc axel)-->le permite programar bajo Duck Typing. Soporta metaclases lo que permite tratar a las clases como objetos y tener absoluto control de la herencia como en lenguajes prototipados [@Deniau2009].

### Conceptos soportados
1. Encapsulamiento
2. Herencia
3. Polimorfismo
4. Reenvío de mensajes (message forwarding) / Delegados (Delegates)
5. Metaclases
6. Excepciones


### Modelo de objetos
El modelo de objetos de ooc es bastante sencillo para la cantidad de conceptos que soporta. Mantiene una jerarquía distinta para las clases y las metaclases.
El siguiente diagrama representa las estructuras que dan soporte al modelo de objetos.

<!--
@startuml
class Object <<(S,orchid)>>{
	unsigned long magic
}

class Method  <<(S,orchid)>>{
	const char * tag
	void (*selector)()
	void (*method)()
}

class Class<<(S,orchid)>>{
	const char * name
	const void * super
	size_t size
	struct Method ctor
	struct Method dtor
	struct Method puto
	struct Method delete
	struct Method forward
	struct Method new
}

Class "1" *-- "1" Object : _ >

Object "*" o-- "1" Class : class
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/VP31JiCm38RFpLDOFRKHEt2TfceIPyI11vY2OzgW99uIXqaWtXrIsXrMn2MMF_d_4kUOnIGfmIlq9cTuUdaZ53WSkkSxJZWvsm_3fsfAp6wCPC5p726OqQ7wKaleaMHY2ziKSim2E9a46iICgdbdPw7JcJoXSEgx_ih2RAdaAdwev__MQ0AjQ1PfoELCIJNPVT19e9QVHb91MLzQhxvbzWzsBiAREV8a6-Chfuj9zXP7kjHjvZsWlMz1xtQrhl-yXnCCIYrTgrlWPU0tiWUiLHqfsXByDm00)
\ 

Una instancia de la estructura `Object` es un objeto, una instancia de la estructura `Class` es una clase. Todas las clases heredan de la clase `Object`. La misma está compuesta de un número llamado `magic`, que sirve para saber si cierta variable es un  objeto y tiene el mismo valor para todos los objetos. Esto se utiliza para la programación defensiva y asegurarse de que un objeto pasado por argumento en un puntero `void` sea realmente un objeto. Además contiene a su clase que se referencia con la variable `class` y es una instancia de la estructura `Class`.
La referencia a su clase contiene su nombre, una referencia a su clase padre, su tamaño y los métodos polimórficos de instancia. Los métodos polimórficos de instancia por defecto son su constructor (`ctor()`), su destructor (`dtor()`), un serializador de la clase (`puto()`), un instanciador (`new()`) y desinstanciador (`delete()`) y un método que se encarga del reenvío de mensajes (`forward()`) como se explicará.
Una clase a la vez hereda de `Object`, por lo que la convierte en un objeto que tiene su propia referencia a su clase. Esta última clase (la clase de una clase) se llama una metaclase y contiene los mismos métodos polimórficos por defecto que ya nombramos. Esto significa, por ejemplo, que a través de una metaclase puede instanciarse una nueva clase mediante el método `new()`. Este método permite redefinir los métodos que se deseen para la nueva clase. Los métodos definidos en la metaclase son métodos de clase.

El siguiente diagrama representa a la instancia que representa a la clase `Object` y a la que representa a la metaclase `Class`  
<!--
@startuml
object "_Object:Class" as _Object{
	_.magic = MAGIC

	
	name = "Object"
        
size = sizeof(struct Object),
	ctor.method = Object_ctor
	dtor.method = Object_dtor
	puto.tag = "puto"
        puto.selector = puto
puto.method = Object_puto
	delete.tag= "delete"
delete.selector = delete
delete.method= Object_delete
	forward.tag = "forward"
forward.selector=forward
forward.method= Object_forward
	new.method = Object_new
}

object "_Class:Class" as _Class{
	_.magic = MAGIC
	name = "Class"
        size = sizeof(struct Class)
	ctor.method = Class_ctor
	dtor.method = Class_dtor
	puto.tag = "puto"
        puto.selector = puto
puto.method = Object_puto
	delete.tag= "delete"
delete.selector = delete
delete.method= Class_delete
	forward.tag = "forward"
forward.selector=forward
forward.method= Object_forward
	new.method = Object_new
}

_Object -- _Object : super >
_Object o-- "1" _Class : _.class >


_Class -- _Object : super >
_Class o-- "1" _Class : _.class >
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/pP51ImCn48NFcLynv6J1BdWjL2ezY0Vn9onnazRAfYb9beBYVtUIIRPbNRngBfbvtzktWRS9KVaus1xSwxlf8ihs9G-hRQz2aAW25kKJHDjOjJzqkCRdXyUdBO00SLJMa23P907BWN3uI21TRdSJeXyedbsBMn1TTBwn9huvJIxMsoI2q7D8PtGQeckYsfCkqtpvNoR1z2Pb44uxP76Qb8dGP8qcPL4KBnBulevXfGAE6fCA53ldpyhh-h2oIgXwpLiNeO9fNiNYQCu_daqQVC6bfrpGLKqipBGqLiHUm79c2ygUnQGULkVQOVAlok4d_KatBQ-uN68TLnY6a_5uFq97LDx9qXSPsgPB4ra0M9mFOFRRznjpr8Fjlm40)
\ 



En estas estructuras básicas es interesante hacer notar que la clase `Object` desciende de sí misma y la clase de la metaclase `Class` es `Class` (lo que significaría su meta-metaclase) y así continúan recursivamente. Esto es lo que se llama un modelo de objetos uniforme, donde tanto las clases como los objetos son objetos con el mismo uso [@Deniau2009].
Cada método en una clase se define con la estructura `Method`, la misma contiene un tag (bajo el nombre `tag`), una implementación (`method`) y un selector (`selector`). El tag permite referenciar a un método mediante una cadena de caracteres. Esto permite crear módulos desacoplados pero que pueden interactuar entre sí mediante convención de nombres. Esto se logra a través de un método no polimórfico llamado `respondsTo()` que recibe como argumentos el objeto a interrogar si posee un método con cierto tag y el tag mismo. La función devuelta por `respondsTo()` es la del selector y no  la de la implementación, por eso se incluye al selector en esta estructura ya que, de lo contrario, se lo puede llamar directamente por su prototipo declarado en el archivo de interfaz. Los objetos que poseen métodos que se ajustan a las convenciones de otros objetos, permitiéndoles a estos últimos utilizarlos, se llaman delegados (delegates en inglés).
El selector (o despachador, dispatcher en inglés) es el encargado de seleccionar la implementación correcta del método al cual representa y ejecutarlo. Para eso recibe como argumento el objeto que se supone implementa el método en cuestión. En caso de que no lo implemente se llama al método `forward()` del objeto con la información del mensaje que se envió al método y desde ahí el objeto puede decidir reenviar el mensaje a otro objeto o procesarlo de otra forma (por ejemplo imprimiendo un error y terminando el programa). Esta capacidad de reenviar un mensaje hacia otro método se llama message forwarding y provee una excepcional flexibilidad al programador. La misma puede suplir la necesidad de la herencia múltiple mediante la composición con otras clases y reenviando los mensajes a las clases que implementan dichos mensajes.  

Tanto las clases como las metaclases pueden heredarse. Heredar una metaclase permite definir nuevos métodos y permitir instanciar nuevas clases que implementen dichos métodos.
El siguiente diagrama representa las estructuras de una clase `Child` que hereda de `Object` y que define también una nueva metaclase que hereda de `Class`.  
<!--
@startuml
class Object <<(S,orchid)>> #lightgrey{
}

class Child <<(S,orchid)>>{
[data]
}

Child "1" *-- "1" Object

class Method  <<(S,orchid)>>  #lightgrey{
	const char * tag
	void (*selector)()
	void (*method)()
}

class Class<<(S,orchid)>>  #lightgrey{
}

Class "1" *-- "1" Object : _ >

Object "*" o-- "1" Class : class

class ChildClass<<(S,orchid)>> {
}

ChildClass "1" *-- "1" Class
ChildClass "1" *-- "*" Method
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/VP2nIi0m48RdtbCSSMc37LnBAGLdSN0KaPY49f8sa5m5ARwx98rAIt5fw7_VtNVS64a4McS7qeaOyV7jNKl2lg-VRdsGngfc6F36sSdG5FJd1bzGq7jZdJgH6pmhGU8bKNkTtJ7aRPlUClod_q6JyGhFheEiadw9XDA8W1n9J51zUAkmvb4xBSc7fcv-ipbFJCdVakdxZo6HcRbO53jynG6W_37Eq1TWR-amImu7kHBkbb8zgN9oMUIid0X6lQXrTjy0)
\ 

`Child` incluye a `Object` como primer elemento lo que convierte a un `Child` en un `Object`, la estructura `Child` puede contener más atributos de instancia (`data` en el diagrama). Para definir nuevos métodos, se incluyen en la estructura de la que se instancia que representa a la clase `Child`, `ChildClass`, que no puede incluir atributos de clase (esto se debe a que `respondsTo()` espera que haya solo métodos). La metaclase seguirá siendo una instancia de la estructura `Class` pero cambiará principalmente su constructor para construir instancias de la estructura `ChildClass` (una de ellas la clase de `Child` misma).
Si un nuevo objeto desea heredar de `Child` pero no define nuevos métodos de instancia o métodos y atributos de clase, entonces heredará de la estructura `Child` pero no precisa heredar de la estructura `ChildClass`, ya que la metaclase de `Child` permite instanciar una nueva clase con las implementaciones de los métodos definidos para la nueva clase (al igual que sus tags).


### Codificación
El autor del framework reconoce las dificultades de implementar clases bajo este framework y provee un preprocesador escrito en AWK para facilitar su codificación. Luego analizaremos el uso de preprocesadores distintos al provisto por C para sortear estas dificultades. En esta tesis buscamos presentar generadores de código desde UML para resolver dichas dificultades. Lo que nos interesa aquí es mostrar qué partes del código C conllevan esa dificultad para evaluar más adelante si un generador de código desde UML podría factiblemente resolverlas.

#### Visibilidad
Las posibilidades de visibilidad de los miembros de clase (atributos y métodos) son  iguales a los del framework `ooc` de Tibor Miseta. Los métodos privados se declaran en un archivo con el nombre de la clase y extensión `.c`, los métodos públicos con extensión `.h` y los métodos de implementación con extensión `.r`. Todos los atributos tienen visibilidad de implementación. Para el autor, la visibilidad de implementación ,en principio, es sólo para las clases derivadas (y obviamente también para la clase implementadora) por lo que sería equivalente a la visibilidad protegida para otros LPOO. Para los sistemas altamente restringidos, sin embargo, resulta importante poder instanciar objetos en variables automáticas o estáticas  por lo que implicaría incluir este archivo en más fuentes que solo las de esas clases.

#### Archivo de interfaz de implementación (.r)
```c
//Child.r
# include "Object.r"

struct Child { const struct Object _;
	/*Child data*/
};

struct ChildClass { const struct Class _;
	struct Method myMethod;
	/*more methods here*/
};

struct Object * super_myMethod (const void * _class, 
						void * _self, void * anObject);
```
En este archivo se definen las estructuras de instancia de clase (en este caso las instancias son objetos de tipo `Child`) y de instancias de metaclase (clases de tipo `ChildClass`). Al comienzo se incluye el archivo de interfaz de implementación de la clase padre (`Object`). En la estructura `Child` se declaran variables de instancia. En la estructura `ChildClass`, los métodos de polimórficos. El selector de los métodos polimórficos declarados (en el ejemplo `myMethod`) se declara en el archivo `.h`. El método `super_myMethod()` se utiliza para llamar a la implementación del método `myMethod` en la clase padre.


#### Archivo de implementación (.c)
```C
//Child.c
int Child_myMethod (void * _self, void * anObject) {
	/*...*/
}

int myMethod (void * _self, void * anObject) {
	int result;
	const struct ChildClass * class = 
					(const void *) classOf(_self);

	if (isOf(class, ChildClass()) 
				&& class -> myMethod.method) {
		cast(Object(), anObject);

		result = 
			((struct Object * (*) ()) class -> myMethod.method)
				(_self, anObject);
	} else
		forward(_self, & result, (Method) add, "add",
											 _self, anObject);
	return result;
}

int super_myMethod (const void * _class, void * _self, 
					void * anObject) {
	const struct ChildClass * superclass = 
									cast(ChildClass(), super(_class));

	cast(Object(), anObject);

	assert(superclass -> myMethod.method);
	return ((struct Object * (*) ()) superclass -> myMethod.method)
					(_self, anObject);
}

static void Child_forward (const void * _self, void * result, 
							Method selector, 
							const char * name, 
							va_list * app) {
	const struct Child * self = cast(Child(), _self);

	if (selector == (Method) otherMethod)
	{	
		/* process otherMethod message */
	}
	else
		super_forward(Child(), _self, result, selector, name, app);
}

static void * ChildClass_ctor (void * _self, va_list * app) {
	struct ChildClass * self = super_ctor(ChildClass(), _self, app);
	Method selector;
	va_list ap;
	va_copy(ap,*app);

	while ((selector = va_arg(ap, Method)))
	{	const char * tag = va_arg(ap, const char *);
		Method method = va_arg(ap, Method);

		if (selector == (Method) myMethod)
		{	if (tag)
				self -> myMethod.tag = tag,
				self -> myMethod.selector = selector;
			self -> myMethod.method = method;
			continue;
		}
		/*other methods*/
	}
	return self;
}

static const void * _ChildClass;

const void * const ChildClass (void) {
	return _ChildClass ? _ChildClass :
		(_ChildClass = new(Class(),
			"ChildClass", Class(), sizeof(struct ChildClass),
			ctor, "", ChildClass_ctor,
			(void *) 0));
}

static const void * _Child;

const void * const Child (void) {
	return _Child ? _Child :
		(_Child = new(ChildClass(),
			"Child", Object(), sizeof(struct Child),
			myMethod, "myMethod", Child_myMethod,
			forward, "forward", Child_forward,
			/* other methods here*/
			(void *) 0));
}
```
`Child_myMethod()` es la implementación de `myMethod` para `Child`, sólo se declara en este archivo por lo que tiene visibilidad privada . La función `myMethod()` es el selector del método, se declara en el archivo de interfaz por lo que tiene visibilidad pública, realiza las verificaciones de tipos de los argumentos y verifica que el objeto al cual se envía el mensaje sea del tipo que definió ese método (en este caso `Child`), si no se reenvía el mensaje al método `forward()` del objeto.  `super_myMethod()` ya fue analizado, también verifica que el tipo del objeto al cual se le envía el mensaje sea `ChildClass`.
Se muestra en el ejemplo una implementación del método `forward()` para procesar una llamada a un objeto `Child` con el método `otherMethod`, el mismo podría reenviarse a un objeto que comprenda ese mensaje.  
La función `Child()` retorna a la clase `Child`, si es la primera vez que se la llama, se la instancia en ese momento. Al constructor se le indican las implementaciones de los métodos y sus tags que debe contener la clase `Child`, en este caso las ternas selector, tag, implementación para `myMethod` y para `forward`. Cómo se procesan estos argumentos y se asignan a la clase se lo puede ver en la función `ChildClass_ctor()`.  La función `ChildClass()` devuelve una instancia de la metaclase  `ChildClass`, que desciende de `Class`, y tiene sigue la misma lógica de argumentos para se construcción.   


### Dificultades en la codificación
| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores  | Inicialización de métodos polimórficos | Contrario a otros LPOO, a cada referencia de un método debe  asignársele su implementación a través del método de inicialización de la clase, si esto se olvida generaremos un error crítico al enviar un mensaje con el método no inicializado. |  
|2|Repetición de información y obtención de información|Cada método polimórfico implica varias referencias al mismo| Si es la primera vez que se define el método: en el selector, el selector "superclass", su asignación en el constructor de la metaclase, y tanto sea una definición como una redefinición: su implementación y como argumento en el constructor de clase. Es necesario obtener la información de sí es la primera vez que se define el método o no. |
|3|Repetición de información|Repetición en la información de la herencia tanto para clases como para metaclases| Además de indicarse esto mediante la herencia de estructuras en el archivo de interfaz de implementación se lo indica como argumento del constructor de la clase o de la metaclase.|
|4|Repetición de código|Nombre de la clase en cada implementación de método o en un método no polimórfico| Para no colisionar con los nombres utilizados por otras clases, o con el selector, todas los métodos antedichos llevan por delante el nombre de la clase. |
|5|Repetición de código y exigencia de codificación|Repetición en los métodos selectores y constructores de metaclase| Los métodos selectores y constructores de metaclase son prácticamente iguales o siguen la misma lógica para toda clase y método polimórfico. Además de eso implican una gran cantidad de código. |
|6|Exigencia de codificación|La programación defensiva implica codificar gran cantidad de verificaciones (mediante la función `cast()`) | Prácticamente no existe un método donde no haya que realizar alguna verificación de los argumentos que se le pasan por `void*`, sólo las implementaciones de métodos cuyos selectores ya hicieron las verificaciones pertinentes pueden obviarlas. |
|7|Propensión a errores y exigencia de codificación|El reenvío de mensajes implica un manejo complicado de los argumentos| Para poder enviar cualquier mensaje al método `forward()` el mismo utiliza argumentos variables que luego deben recuperarse mediante `va_arg()`. Fácilmente puede equivocarse el orden en que están guardados tales argumentos llevando a un error en el código.  |
|8|propensión a errores y exigencia de codificación|Los constructores de instancia reciben sus argumentos en una lista de argumentos variable| Fácilmente puede equivocarse el orden en que se definieron tales argumentos llevando a un error en el código. Además se debe incluir una llamada a `va_arg()` por cada uno. |


### Propuesta de expresión en UML
Vemos dos formas convenientes de representar este framework en UML, uno con metaclases explícitas y otra con implicitas.


#### Propuesta de expresión con metaclases explícitas
<!--
@startuml
class ChildClass  <<metaclass>>{
method()
}

class AClassClass <<metaclass>>{
other_method()
}

class GrandChildClass <<metaclass>>{
method2()
}

class Child{
type data
ctor(type arg)
}

class AClass{
other_method()
}

class GrandChild{
<<tag>> method()
method2()
forward()
}

AClassClass <.. Child : <<forwards>>


Child <|-- GrandChild
ChildClass <|-- GrandChildClass
ChildClass <|.. Child
GrandChildClass <|.. GrandChild
AClassClass <|.. AClass
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/ZP713e8m44Jl-nKzuW4E7iscmN3mCyo6LZ01QieQOz1_bxGbMSI3DphpfZCDvSZa-TvtK7SqZgfghvsfmgT2x2rJqBMUO3wqpcGxU4E2Zm6Cz1VikBN-l8sSF0r6bFpisCj4W2VWvyqgGqnGi_DPE99lDcl-gPu0aQdHMYsKABuu_o2VagiN5aKSemuACM7pQW28ChxoNBI0VAQqahZsbyjXeQGbed9IzA80fHtC_1y_)
\ 


Se puede apreciar la representación tanto de clases como de las metaclases de las cuales se instancian. Las metaclases poseen el estereotipo `metaclass`. La relación entre una clase una metaclase se indica con la relación `realize` que se utiliza para relacionar una clase con la interfaz que realiza. Una clase sólo puede ser instanciada desde una metaclase. Una metaclase hereda de otras metaclases (o de la metaclase `Class` si no se le especifica herencia) y una clase hereda de otra clase (o de `Object` si no se le especifica herencia). Si una clase no tiene representada una metaclase de la cual se instancia entonces es instancia de la metaclase `Class`.   
Los métodos en una metaclase describen los métodos polimórficos de instancia de las clases que son instanciadas de dicha metaclase. Una clase que provea una implementación para alguno de dichos métodos debe incluir al método como una operación propia. Una clase que no posea una implementación ni propia ni heredada de alguno de los métodos de la metaclase de la cual se instancia, es una clase base (en el ejemplo la clase `Child` no implementa el método `method()`) y alguna clase hija puede proveer una implementación para los mismos (en el ejemplo la clase `GrandChild` define una implementación de `method()`).
Los métodos `ctor()`, `dtor()`, `puto()`, `new()`, `delete()` y `forward()` definidos en una clase son considerados implementaciones de los métodos definidos por la metaclase `Class`.
A las implementaciones de métodos polimórficos se les puede aplicar el estereotipo `<<tag>>`. El mismo contiene un atributo `nombre` que lo representa (el mismo lo referencia para obtenerlo a través del método `respondsTo()`). Si el nombre estuviese vacío se le asignaría el nombre del método.
Las variables de instancia (`data`) pueden ser de clase (lo que significa que se instancian en el archivo de implementación de la clase (.c)) y constantes.
Los métodos no polimórficos se implementan en el archivo de implementación de la clase (.c) y si tienen visibilidad pública se declaran en el archivo de interfaz de la clase.
De acuerdo a `ooc` los constructores reciben sus argumentos en una lista de argumentos variables, sin embargo su representación es con una lista de argumentos fija.
Para especificar la capacidad de procesar un mensaje definido por una metaclase (que no pertenece a la jerarquía de metaclases de la metaclase que se realiza) a través del método `forward()` se utiliza un estereotipo que extiende la relación `usage` y se llama `<<forwards>>`. El final de dicha relación (el proveedor, supplier en inglés) es el método mismo y no la metaclase que lo contiene.
Se escogió la relación `usage` ya que según la especificación no define el uso que el cliente da  al proveedor [@UML2017], pero mediante el estereotipo sí lo especificamos.
También se puede declarar el método `forward()` dentro de una clase para  implementarlo (como es el caso en `GrandChild`).

 
#### Propuesta de expresión con metaclases implícitas 
<!--
@startuml
class Child{
type data
<<tag>> method()
method2()
ctor(type arg)
}

class AClass{
other_method()
}


AClass <.. Child : <<forwards>>

class GrandChild{
method()
forward()
}


Child <|-- GrandChild
@enduml
-->

![enter image description here](http://www.plantuml.com/plantuml/png/JO_13e9034Jl_OeUuG07ZwRPO3Zu6QQXAvW0Qxerng3_BhAhyTH9PkQrRQAnscqQeHivHco7ooWBsEFgKTWOY8nxvt3oDWGfIaXYjwhEWXPRahKluGMPSMW_Ou5WWzTJBcu-90kfhjCct2FHEUYTLQ9pNy9HUPPyog-VOvcKwlIighyqD7wMzPat)
\ 



Las clases que definan nuevos métodos polimórficos (es decir, que una clase padre no lo haya ya definido), generan metaclases con el nombre de la clase seguido por `Class`, además implican selectores y selectores de clase padre (super class selector). En caso de no poseer métodos polimórficos nuevos, tanto por esa clase como por las clases padre, entonces la metaclase de dicha clase es `Class`.
Los métodos `ctor()`, `dtor()`, `puto()`, `new()`, `delete()` y `forward()` definidos en una clase no se consideran métodos nuevos ya que ya son definidos para `Object`.
A los métodos polimórficos se les puede aplicar el estereotipo `<<tag>>`. El mismo contiene un atributo `nombre` que lo representa (el mismo lo referencia para obtenerlo a través del método `respondsTo()`). Si el nombre estuviese vacío se le asignaría el nombre del método.
Las variables de instancia (`data`) pueden ser de clase (lo que significa que se instancian en el archivo de implementación de la clase (.c)) y constantes.
Los métodos no polimórficos se implementan en el archivo de implementación de la clase (.c) y si tienen visibilidad pública se declaran en el archivo de interfaz de la clase.
De acuerdo a `ooc` los constructores reciben sus argumentos en una lista de argumentos variables, sin embargo su representación es con una lista de argumentos fija.
Para especificar la capacidad de procesar un mensaje definida por otra clase a través del método `forward()` se utiliza un estereotipo que extiende la relación `usage` y se llama `<<forwards>>`. El final de dicha relación (el proveedor, supplier en inglés) es el método mismo y no la clase que lo contiene.
Se escogió la relación `usage` ya que según la especificación no define el uso que el cliente da  al proveedor [@UML2017], pero mediante el estereotipo sí lo especificamos.
También se puede declarar el método `forward()` dentro de una clase para  implementarlo (como es el caso en `GrandChild`).
<!--
A Usage is a Dependency in which one NamedElement requires another NamedElement (or set of NamedElements) for
its full implementation or operation. The Usage does not specify how the client uses the supplier other than the fact that
the supplier is used by the definition or implementation of the client .
-->



## GObject de GLib


### Introducción
<!-- fuente:
The C library ”Glib” is an open-source cross-platform general-purpose
utility library, which provides common data structures, abstractions, utilities
and functions. In this respect, ”Glib” can be compared to the standard library
of C++. ”Glib” also offers a type and object system for C which allows the C
developer to implement an OO design with ”ease”. The latter is of course of
high interest.
-->
Glib es un paquete de bibliotecas C de código abierto y para uso de propósito general. Dentro del paquete Glib se encuentra la biblioteca GObject o GLib Object System, la misma ofrece un sistema de tipos y objetos para C que da soporte para conceptos de la POO.
<!--
The ”Glib” library originated from the GIMP project, the GNU Image
Manipulation Program, a project started by two Berkeley students called
Spencer Kimball and Peter Mattis. When this project moved from the GUI
toolkit ”Motif” to its own toolkit ”GTK” (GIMP ToolKit), the ”Glib” library
was born. However, at that time ”Glib” did not contain a type and object
system. Later-on, ”GTK” became ”GTK+”. ”GTK” used a flat hierarchy of
widgets, ”GTK+” added an inheritance hierarchy. Therefore a type and
object system was introduced in GTK+, known as ”GtkObject”. When
GTK+ reached version 2.0, the type and object system moved out of the
toolkit into the Glib library. There it grew into an advanced independent
system, that could be used in any C project.
Today, ”Glib” and its type and object system are used in a variety of
projects, including GIMP, GNOME, Evolution and Gstreamer. All those
projects prove that OO programming with Glib is certainly possible and are a
legacy of the power of Glib.
-->
Hoy en día, GObject se utiliza en una variedad de proyectos, incluyendo GIMP, GNOME, Evolution y Gstreamer. Todos 
esos proyectos demuestran que la programación OO en C es ciertamente posible incluso fuera de los sistemas embebidos [@Hendrickx2004].
Glib es portable a distintos sistemas operativos pero no es simplemente portable a un sistema embebido sin sistema operativo.


### Conceptos soportados
1. Encapsulamiento
2. Herencia
3. Polimorfismo
4. Interfaces (sin herencia de interfaces)
5. Propiedades
6. Señales (sistema de notificaciones y mensajes de bajo acoplamiento entre clases)
7. Programación clave-valor


### Modelo de objetos
Un análisis detallado del modelo de objetos de GObject queda fuera del alcance de esta tesis, aunque no difiere en esencia con la de otros frameworks ya vistos. Todas las clases heredan de una clase común llamada GObject, la misma a su vez hereda de un tipo (no un objeto) llamado GTypeInstance que es la base para otros tipos comunes. La estructura de RTTI de las clases se crea en RAM y en tiempo de ejecución y posibilita agregar manejadores de señales y propiedades en tiempo de ejecución. Las interfaces heredan de GTypeInterface y pueden poseer implementaciones por defecto de sus métodos.
Las señales y propiedades son comparables a los delegados en OOC de Schreiner, una forma de acceder a métodos (asociados por una clase con cierto texto) mediante un texto en lugar de mediante una interfaz.


### Codificación
En el siguiente ejemplo de codificación la clase `Child` que hereda de `GObject` posee un atributo público (`publicAttr`) y uno privado (`privateAttr`), una propiedad para su atributo público, un método polimórfico y una señal.


#### Interfaz de clases (.h)
```c
#include <glib-object.h>

typedef struct _ChildClass ChildClass;
typedef struct _Child      Child;
```
Primero se declara la tabla virtual de la clase, la misma debe comenzar con un `_`, seguir con el nombre de la clase y terminar con la palabra `Class`. El primer miembro es la tabla virtual de la clase de la cual hereda, en este caso GObject, el nombre de este miembro es a elección aunque el nombre `parent` es lo usual.
```c
struct _ChildClass {
    GObjectClass parent;
    void (*method) (Child *self);
    void (*signal1Process) (Child *self);
};
```
Luego se declara la estructura de instanciación de la clase, la misma debe comenzar con un `_`, y terminar con el nombre de la clase. El primer miembro es la estructura de instanciación de la clase de la cual hereda, en este caso GObject, el nombre de este miembro es a elección aunque el nombre `parent` es lo usual. Los atributos públicos son declarados a continuación. Si la clase es final, o sea que no es heredable, esta estructura se define en el archivo de implementación.
```c
struct _Child {
    GObject  parent;
    gint     publicAttr;
};
```
Las siguientes seis macros se deben declarar para la clase.
```c
#define TYPE_CHILD           (child_get_type             ())
#define CHILD(obj)           (G_TYPE_CHECK_INSTANCE_CAST ((obj), \
				TYPE_CHILD, Child))
#define CHILD_CLASS(cls)     (G_TYPE_CHECK_CLASS_CAST    ((cls), \ 
				TYPE_CHILD, ChildClass))
#define IS_CHILD(obj)        (G_TYPE_CHECK_INSTANCE_TYPE ((obj),\ 
				TYPE_CHILD))
#define IS_CHILD_CLASS(cls)  (G_TYPE_CHECK_CLASS_TYPE    ((cls),\ 
				TYPE_CHILD))
#define CHILD_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS  ((obj),\ 
				TYPE_CHILD, ChildClass))
```
La función `child_get_type()` devuelve una referencia a la clase registrada en el sistema de tipos de glib.
```c
GType  child_get_type  (void);
```
Por último se declaran los selectores de los métodos polimórficos.
```c
void   child_method     (Child *self);
```


#### implementación de clases (.c)
```c
#include <glib-object.h>
#include "child.h"

#include "my_interface1.h"
```
Definiciones para referenciar propiedades.
```c
enum {
    PROP_0,
    PROP_ATTR
};
```
Definiciones para referenciar señales:
```c
enum {
  SIGNAL1,
  LAST_SIGNAL
};
```
Estructura para atributos privados:
```c
typedef struct _ChildPrivate ChildPrivate;

struct _ChildPrivate {
	int privateAttr;
};

static guint child_signals[LAST_SIGNAL] = {0};

```
Selector de `method()`:
```c
void child_method (Child *self) {
    CHILD_GET_CLASS(self)->method(self);
}
```
Implementaciones de métodos y señales. `method1()` es definido por `MyInterface1`:
```c
static void child_method_impl (Child *self) {
    /* ...  */
}

static void child_method1_impl (Child *self) {
    /* ...  */
}

static void child_signal1_process (Child *self) {
    /* ...  */
}
```
Métodos get y set para dar soporte a las propiedades definidas por la clase:
```c
static void child_get_property (GObject    *obj,
                                guint       prop_id,
                                GValue     *value,
                                GParamSpec *pspec) {
    Child *child = CHILD(obj);

    switch (prop_id) {
    case PROP_ATTR:
        g_value_set_int(value, child->publicAttr);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, pspec);
        break;
    }
}

static void child_set_property (GObject      *obj,
                                guint         prop_id,
                                const GValue *value,
                                GParamSpec   *pspec) {
    Child *child = CHILD(obj);

    switch (prop_id) {
    case PROP_ATTR: {
        gint new_attr = g_value_get_int(value);
        child->publicAttr = new_attr;
        break;
    }
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(obj, prop_id, pspec);
        break;
    }
}

```
Inicialización de la clase, de la clase, se asocia a la clase la estructura de atributos privados, las propiedades y señales (estas últimas pueden asociarse con la clase incluso luego de su inicialización ), se inicializan los métodos de la tabla virtual con las implementaciones de los métodos y las señales. 
```c
static void child_class_init (ChildClass *cls) {
    GObjectClass *g_object_class = G_OBJECT_CLASS(cls);
    GParamSpec *attr_param;

    g_type_class_add_private (cls, sizeof (ChildPrivate));


    g_object_class->get_property = child_get_property;
    g_object_class->set_property = child_set_property;

    cls->signal1Process = child_signal1_process;
    cls->method = child_method_impl;

    attr_param = g_param_spec_int(
        "attr", "attr", "attribute of child",
        INT_MIN, INT_MAX,
        0,
        G_PARAM_READWRITE);

    g_object_class_install_property(
        g_object_class,
        PROP_ATTR,
        attr_param);


    child_signals[SIGNAL1] = g_signal_new(
        "signal1",                               /* signal_name */
        TYPE_CHILD,                            /* itype */
        G_SIGNAL_RUN_LAST | G_SIGNAL_DETAILED, /* signal_flags */
        G_STRUCT_OFFSET(ChildClass, signal1Process),/*class_offset*/
        NULL,                                  /* accumulator */
        NULL,                                  /* accu_data */
        g_cclosure_marshal_VOID__VOID,         /* c_marshaller */
        G_TYPE_NONE,                           /* return_type */
        0);                                    /* n_params */
}
```
Inicialización de la interfaz en la tabla virtual:
```c
static void child_init_my_interface1(MyInterface1Iface* iface, 
	gpointer iface_data) {
    iface->method1 = 
	(void (*)(MyInterface1 *instance))child_method1_impl;
}
```
Registro de la clase y asociación con sus interfaces. Luego del registro se obtiene la referencia a la clase que es siempre devuelta por la función `get_type()`:
```c
GType child_get_type (void) {
    static GType child_type = 0;

    if (!child_type) {
        static const GTypeInfo child_info = {
            sizeof (ChildClass),                /* class_size */
            NULL,                               /* base_init */
            NULL,                               /* base_finalize */
            (GClassInitFunc) child_class_init,  /* class_init */
            NULL,                               /* class_finalize */
            NULL,                               /* class_data */
            sizeof (Child),                     /* instance_size */
            0,                                  /* n_preallocs */
            NULL,                               /* instance_init */
            NULL                                /* value_table */
        };

        child_type = g_type_register_static(
            G_TYPE_OBJECT, /* parent_type */
            "Child",       /* type_name */
            &child_info,   /* info */
            0);            /* flags */

        /* add interface */
		GInterfaceInfo interface_info_my_interface1 = {
			/* interface_init */
			(GInterfaceInitFunc)child_init_my_interface1, 
			NULL,   /* interface_finalize */
			NULL,   /* interface_data */
		};
		g_type_add_interface_static(child_type, 
			TYPE_MYINTERFACE1, &interface_info_my_interface1);

    }

    return child_type;
}
```


#### Interfaz de una interfaz (.h)
```c
typedef struct _MyInterface1      MyInterface1;
typedef struct _MyInterface1Iface MyInterface1Iface;
```
Se define la estructura de la interfaz que contiene los métodos de la misma y que hereda de `GTypeInterface`.
```c
typedef struct _MyInterface1Iface {
    GTypeInterface parent;
    void (*method1)(MyInterface1 *instance);
} MyInterface1Iface;
```
Los siguientes cuatro macros deben definirse para cada interfaz.
```c
#define TYPE_MYINTERFACE1           (my_interface1_get_type())
#define MYINTERFACE1(obj)           (G_TYPE_CHECK_INSTANCE_CAST    \
					((obj), TYPE_MYINTERFACE1, MyInterface1Iface))
#define IS_MYINTERFACE1(obj)        (G_TYPE_CHECK_INSTANCE_TYPE    \
					((obj), TYPE_MYINTERFACE1))
#define MYINTERFACE1_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE \
					((obj), TYPE_MYINTERFACE1, MyInterface1Iface))
```
`get_type()` devuelve una referencia única a la interfaz reconocida por el sistema de tipos de glib:
```c
GType    my_interface1_get_type                 (void);
```
Selectores de los métodos:
```c
void     my_interface1_method1                 (MyInterface1 *self);
```


#### Implementación de una interfaz (.c)
Implementación de los selectores:
```c
void my_interface1_method1 (MyInterface1 *self) {
    MYINTERFACE1_GET_IFACE(self)->method1(self);
}
```
Métodos por defecto de los métodos definidos por la interfaz:
```c
void my_interface1_impl_method1 (MyInterface1 *self) {
    /* ...  */
}
```
Mediante la inicialización de la interfaz se asignan las implementaciones por defecto, además podrían tomarse recursos para la misma:
```c
static void my_interface1_iface_init (MyInterface1Iface *iface) {
    iface->method1 = my_interface1_impl_method1;
}
```
Registro de la interfaz en el sistema de tipos de glib. Luego del registro `get_type()` devuelve siempre la misma referencia:
```c
GType my_interface1_get_type (void) {
    static GType type = 0;
    if (type) return type;

    static const GTypeInfo info = {
        sizeof (MyInterface1Iface),
        NULL,
        NULL,
        (GClassInitFunc) my_interface1_iface_init
    };

    type = g_type_register_static(
        G_TYPE_INTERFACE,
        "MyInterface1",
        &info,
        0);

    return type;
}
```
x	


### Dificultades en la codificación
| # | Tipo de dificultad | Nombre | Descripción |  
|-|---|---|--------|  
|  1 | Propensión a errores  | Inicialización de métodos polimórficos | Contrario a otros LPOO, a cada referencia de un método polimórfico debe  asignársele su implementación en la función de inicialización de la clase. |  
|  2 | Propensión a errores y obtención de información | Prototipado y asignación de métodos polimórficos redefinidos | Para crear el prototipo de un método ya definido por una clase padre y para poder asignarlo en la tabla virtual, se debe conocer exactamente cuál de los padres es el que definió el método por primera vez, además de que en caso de equivocarnos recibiremos una advertencia por parte del compilador, esto agrega una demora al programador que debe obtener esta información. |  
|3|Repetición de información|LOCs por cada método polimórfico nuevo o redefinido| Si es nuevo se lo referencia al incluirlo en la tabla virtual y al definir su selector y tanto si es nuevo como redefinido: en su implementación, y al asignar la implementación a su referencia en la tabla virtual.|
|4|Repetición de información|Repetición en la información de la herencia| Se hacen varias referencias al padre para lograr una herencia|
|5|Dificultad de aprendizaje| Nuevas macros que aprender| Hay una importante cantidad de macros, funciones y especificaciones a aprender para utilizar correctamente el framework. |
|6|Repetición de código | repetición en cada clase| comparado con otros LPOO, hay mucho código repetido entre una clase y otra tan solo para definir la clase y su herencia, repitiendo varias veces el nombre de la clase como argumento de distintas macros. |

Comparado incluso con otros frameworks puede apreciarse una gran cantidad de código de plantilla (en inglés boilerplate code) asociado al prototipado de una clase y sus métodos.


### Propuesta de expresión en UML
<!--
@startuml
class Parent
{
type attribute
	<<property>> type attribute_prop
	<<signal>> type method()
type method1()
}

class Child
{
	
}

Parent <|-- Child

Interface MyInterface1{
	type method1()
}

Interface MyInterfaceN{
	type methodN()
}

MyInterface1 <|.. Parent
MyInterfaceN <|.. Parent
@enduml
 -->
![Propuesta de expresion GObject](http://www.plantuml.com/plantuml/png/RO-noeCm481dxoaojeE2kuZGgKFbVuDoDTSg954kvo1jtxq9Qb7wRxbyN-uZbHTa6Qs1ng3twW-PdC0JP1f8eGXtjr48ag8Ok1-8PIfBjOVNIABXkuT3it9BqlRwS8JDb8Vn3Klhr7P6XrGIh-QmAbvfkW0uEo6-OqFgCdtFUV1_5_whrdkrdjNjhf3BilNFsvSxK97JepKV)
\


Cualquier clase puede realizar una o varias interfaces y heredar de hasta una clase.
Cualquier método en una clase puede definirse como polimórfico o no (mediante el atributo `isLeaf`), si es de clase o no (mediante el atributo `isStatic`), si es constante o no (mediante el atributo `isConst`), y si es abstracto o no (mediante el atributo `isAbstract`). Las interfaces pueden definir métodos como abstractos o no (o sea que ofrecen una implementación del método por defecto), como constantes o no y como de clase o no.
Los atributos pueden ser de clase (mediante el atributo `isStatic`) y constantes (mediante el atributo `isConst`).
Los atributos pueden poseer el estereotipo `property` con los siguientes valores etiquetados (correspondientes a los nombres de los parámetros pasados a la función `g_param_spec_int ()`):  `nick`: apodo de la propiedad, `blurb`: descripción de la propiedad, y un flag para todas las opciones de flags en el tipo enumerativo [`GParamFlags`](https://developer.gnome.org/gobject/stable/gobject-GParamSpec.html#GParamFlags "enum GParamFlags"). El nombre y los valores minimos y maximos y por defecto de la propiedad son tomados de los definidos para el atributo.
Un método puede poseer el estereotipo `signal` lo que lo hace un manejador de la misma (no analizaremos como modelar una señal sin manejador), los valores etiquetados del estereotipo serían  (correspondientes a los nombres de los parámetros pasados a la función `g_signal_new()`): `name`: el nombre de la señal y  un flag para todas las opciones de flags en el tipo enumerativo [`GSignalFlags`](https://developer.gnome.org/gobject/stable/gobject-Signals.html#GSignalFlags "enum GSignalFlags"). Hay más opciones para las señales y, por consiguiente, más análisis necesario de cómo modelarlas pero quedan fuera del alcance de esta tesis, ya que, como veremos, no presentan una dificultad respecto a la factibilidad de generar código desde tales modelos. Del mismo modo podría buscarse una propuesta de expresión para closures.


## Dynace de Blake McBride


### Introducción
Dynace tiene el objetivo de servir como un lenguaje de propósito general, con el poder de CLOS y SmallTalk, simple de usar como Objective-C, eficiente y lo más cercano posible a la sintaxis de C.  
La filosofía de Dynace es focalizarse en la programación genérica que dista de la programación en lenguajes de tipado estático (como C++ o JAVA) (El manual de Dynace contiene una fuerte crítica al diseño que promueve trabajar con C++). Para eso se sostiene en un modelo de objetos dinámicos basado en CLOS y metaclases, lo que le provee una gran flexibilidad. Implementa funciones genéricas al estilo de CLOS, por lo que cualquier clase puede implementar estas funciones y la implementación a ser ejecutada es escogida en tiempo de ejecución de acuerdo al tipo del primer parámetro (Dynace sólo soporta funciones genéricas de rango 1, los multimétodos son funciones genéricas que se escogen de acuerdo al tipo  de varios de sus parámetros en tiempo de ejecución).  
Al contrario de los demás frameworks presentados en este estudio, Dynace se define de acuerdo a la sintaxis entendible por su preprocesador, el cual no es el estándar de C. Con esto, aunque no utiliza la sintaxis de C, logra que sea muy amigable para el programador C, y el cuerpo de los métodos sí conserva la sintaxis de C.
Las facilidades en tiempo de ejecución de Dynace lo hacen más costoso en el uso de memoria RAM y tiempo de ejecución (pero distan mucho de programar, por ejemplo, para una máquina virtual) por lo que no es recomendable para microcontroladores demasiado restringidos.
Las clases en Dynace son codificados en un único archivo .d y el código generado sigue la norma ISO C89.
Contiene una jerarquía de clases núcleo equivalente a la de SmallTalk, una biblioteca de clases completa, threads cooperativos así como soporte para threads nativos y gran cantidad de documentación para el principiante.
Ha sido usado por más de 20 años en ambientes de producción.


### Conceptos soportados
1. Encapsulamiento
2. Herencia (múltiple) 
3. Polimorfismo
4. Duck typing 
5. Manejo de excepciones
6. Metaclases
7. Recolector de basura
8. Programación clave-valor  <!-- pág 305 del manual-->
 
 
### Modelo de objetos
El análisis del modelo de objetos de Dynace excede el alcance de esta tesis.


### Codificación
La especificación detallada de la sintaxis de Dynace se encuentra en su manual, aquí solo daremos una introducción suficiente para nuestro análisis.
Dynace define un único tipo para el uso de todas las clases, el tipo `object`.
La implementación de una clase, con variable de instancia, de clase y especificación de una función de inicialización de clase (se usa en caso de que se necesiten instanciar recursos para la clase) se realiza con la palabra clave `defclass`:
```c
//Parent.d
defclass  Parent{
	int instanceVariable; //Variable de instancia
	class:
	int classVariable;	//Variable de clase
	init: initParentClass; //función de inicialización de la clase
};
```
Las clases pueden tener visibilidad privada (por defecto) o pública, lo que permite referenciar a la estructura de instancia de la clase (de otra forma la misma está oculta).

En caso de herencia múltiple , el listado de clases sigue a la clase:
```C
//MyClass.d
defclass MyClass : SomeClass, SomeOtherClass;
```

La implementación de genéricos puede especificar un solo método para varias funciones genéricas:
```
//MyClass.d
imeth int gMyGeneric, gGeneric2, gGeneric3 (char param)
{
return param;
}
```
`imeth` indica que se trata de un método de instancia con verificación del tipo de los parámetros en tiempo de compilación. Las otras variantes son `cmeth` para métodos de clase y sus variantes con lista de argumentos variable: `ivmeth` y `cvmeth`. El tipo de retorno en este caso es integer, pero en caso de omitirse por defecto es `object`, la clase padre de todas las clases. 
La simplicidad en la sintaxis muestra el poderío de Dynace como lenguaje de programación.


### Dificultades en la codificación
Dynace no presenta ninguna de las dificultades de las que venimos estudiando salvo el hecho de tener que aprender su sintaxis para el prototipado de clases.


### Propuesta de expresión en UML
![enter image description here](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLd06aORLN8ab9KLSkQab6VafXXgQNBN8qaPGQHkRn9pCnEBKOZixY4eZT9N6rK0-5yHC1PiQNLsei094p0XPZ0jPemaM5pYavgK07Ge0)
\ 

La visibilidad de las clases y métodos sólo puede ser pública o privada. Los atributos no tienen atributo de visibilidad.
Todos los métodos públicos son polimórficos, por lo que el atributo `isLeaf` es ignorado. Puede indicarse si el método es de clase o no (mediante el atributo `isStatic`), si es constante o no (mediante el atributo `isConst`). Existe una forma de exigir que un método sea  implementado mediante el método `gSubclassResponsibility()` que arroja una excepción si un método no está siendo redefinido por un objeto hijo, con esto pueden definirse métodos abstractos mediante el atributo `isAbstract`.   Si la implementación del método es el mismo para varios otros métodos, entonces el estereotipo `aliases` es agregado con el valor etiquetado `names` donde se indican los nombres de dichos métodos separados por coma.
Los atributos pueden ser de clase (mediante el atributo `isStatic`) y constantes (mediante el atributo `isConst`).
Cabe aclarar que por más que las clases no tengan una ascendencia en común pueden implementar los mismos métodos que son llamados con los mismos mensajes (en el ejemplo tanto una instancia de `Class1` como de `Class4` pueden responder al mensaje `method1()`).



## COS de Laurent Deniau


### Introducción
<!--
fuente:
COS uses the C99 preprocessor to parse its DSL (Domain Specific Language) embedded in C files and to generate pure C89 code on-the-fly during the translation phases of C compilers, using its advanced framework of C99 macros.
The design and the DSL of COS are strongly inspired by Objective-C and CLOS (Common Lisp Object System), one of the most flexible object model ever developed, and to some lesser extend by Cecil, Dylan, Haskell, Python, Slate and SmallTalk. Contrary to CLOS, COS enforces strong encapsulation and separation of concerns through its open object model, which allows to use and to extend COS components (e.g. classes) defined in shared libraries without having the source code (see papers).
-->
COS utiliza el preprocesador ISO C99 para generar código ISO C89.
El diseño y la sintaxis de COS están fuertemente inspirados en Objective-C y CLOS (Common Lisp Object System), según el autor, uno de los modelos de objetos más flexibles jamás desarrollados, y en menor medida por Cecil, Dylan, Haskell, Python, Slate y SmallTalk . COS impone la encapsulación fuerte y la separación de incumbencias (en inglés concern separation) a través de su modelo de objeto abierto, que permite usar y extender los componentes de COS (por ejemplo, clases) definidos en bibliotecas compartidas sin tener el código fuente.  Este modelo de objetos abierto exige una etapa extra de linkeo para  recolectar símbolos distribuidos en el código, esto se realiza a través de los makefiles que acompañan al framework.
<!--
The core of COS is only 7,000 SLOC and fulfils very well the five principles it aims: simplicity, flexibility, extensibility, efficiency and portability. It is available on GitHub and described in DLS'09 and OOPSLA'09 papers, and a presentation available in the doc directory. It tries to keep minimal the available concepts for the sake of simplicity and flexibility: uniform object model, open classes, metaclasses, property metaclasses, generics, multimethods,
delegation, ownership, properties, exceptions, contracts and closures.
-->
El kernel de COS es de sólo 7000 líneas de código fuente y cumple muy bien los cinco principios a los que apunta: simplicidad, flexibilidad, extensibilidad, eficiencia y portabilidad.  Posee un modelo de objeto uniforme, y permite la programación con contratos y closures.  
<!--
COS design is tuned to provide efficient portable implementation of these concepts, especially for its two key features: dynamic message dispatch supporting multimethods (i.e. many receivers) as well as generic message forwarding (i.e. delegation without limitations). COS message dispatch is x1.7-x2.3 slower than indirect function call (called through pointers) and about x1.2-x1.5 faster than Objective-C message dispatch. COS message forwarding is as fast as message dispatch and about x40-x80 faster than Objective-C message
forwarding, which has strong limitations on the returned values. On top of these two efficient concepts, it is easy to implement high order messages, class-predicate dispatch, multiple inheritance, dynamic inheritance, dynamic classes, adaptive object model, reflection and advanced memory management (some of them are described in the papers).
-->
El diseño de COS está optimizado para proporcionar una implementación portátil y eficiente, especialmente en sus dos características principales: los multimétodos, así como el reenvío de mensajes genéricos (es decir, la delegación). El envío de mensajes en COS es de 1,7 a 2,3 veces más lento que la llamada a una función indirecta (o sea a través de punteros) y aproximadamente de 1,2 a 1,5 veces más rápido que el envío de mensajes en Objective-C. El reenvío de mensajes en COS es tan rápido como el envío de mensajes y aproximadamente de 40 a 80 veces más rápido que el reenvío de mensajes en Objective-C, que, además, tiene fuertes limitaciones en los valores devueltos. Estos dos conceptos junto a su modelo de objetos permiten implementar fácilmente: mensajes de orden superior (high order messages), predicado de clase (class-predicate dispatch), herencia múltiple, herencia dinámica, clases dinámicas, modelo de objeto adaptativo, reflexión y gestión avanzada de memoria.
<!--
COS achieves the principles of simplicity, flexibility and extensibility as well as existing mainstream scripting languages (e.g. PHP, Python, Ruby, Lua, SmallTalk) while keeping the efficiency and the portability in the range of C.
It can be used as both a dynamically or a statically typed programming language (see fast sorting of heterogeneous Array for an example of mixed use). COS is also designed for parallelisation, and it is compliant with TLS (Thread Local Storage), OpenMP and POSIX threads. Its minimal requirement is a C99 preprocessor and a C89 compiler.
-->
COS logra los principios de simplicidad, flexibilidad y extensibilidad, así como los lenguajes de scripting convencionales existentes (por ejemplo, PHP, Python, Ruby, Lua) al tiempo que mantiene la eficiencia y la portabilidad en el rango de C[@Deniau2009].
No es recomendado para ser utilizado en sistemas altamente restringidos con muy poca memoria RAM ya que la caché para optimizar el reenvío de mensajes puede ocupar algunos megabytes (esto puede ser configurado dentro de cierto rango).


### Conceptos soportados
1. Encapsulamiento
2. Herencia
3. Polimorfismo
4. Multimétodos
4. Reenvío de mensajes (message forwarding) / Delegados
6. Manejo de excepciones
7. Metaclases
8. Programación clave-valor


### Modelo de objetos
Un análisis detallado del modelo de objetos de COS excede el alcance de esta tesis. COS sólo soporta herencia simple basado en la herencia de estructuras de instancia, `Object` es la clase base de todas las clases. La codificación bajo COS está basada en la definición de tres componentes básicos: las clases o tipos, las funciones genéricas y los métodos, que definen los tipos de argumento para una función, puede existir varios métodos para una sola función. Un método es una implementación o especialización de una función. Las funciones son llamadas en tiempo de ejecución y la implementación a ser ejecutada es decidida en base a los tipos en tiempo de ejecución de sus argumentos (en COS hasta cinco argumentos pueden ser tomados en cuenta para esta decisión), las implementaciones cuyos argumentos contienen la misma especialización que los de la llamada son ejecutados, sino se escogen implementaciones con argumentos menos especializados (o sea que sean clases padre respecto a los argumentos de la llamada) priorizando los tipos de los primeros argumentos [^a], en caso de no encontrarse una implementación apropiada la función genérica `gunrecognizedMessageN ()` en llamada, donde N es la cantidad de argumentos que son tomados en cuenta y por defecto lanza una excepción. Esta última función da lugar a reenviar la llamada a alguna otra clase (message forwarding). Esto nos lleva a un modelo que en vez de tablas virtuales para cada clase tenemos tablas de métodos para cada función.

[^a]: Para una descripción más detallada leer **Methods  specialization** en la documentación de COS.

### Codificación
El modelo de objetos abierto de COS permite definir cada uno de los elementos que ejemplificaremos en un archivo distinto, es decir, cada definición de clase, método o propiedad puede estar en un archivo distinto. Esto permite una separación de incumbencias muy bueno para el buen diseño de un sistema.


#### Funciones genéricas
Se declaran en una interfaz (.h) de la siguiente manera:
```c
//gfunction.h
#include <cos/Object.h>

defgeneric(void, gfunction, self, arg1);
```
Luego de la macro `defgeneric` el primer argumento es el tipo a retornar por la función, luego el nombre de la función genérica (por convención comienza con g) y luego los argumentos de la función.
Los argumentos `self` y `arg1` son llamados tipos abiertos, ya que cada tipo puede variar de acuerdo a la implementación.
Puede definirse funciones genéricas que también posean tipos cerrados por ejemplo:
```c
//gfunction.h
#include <cos/Object.h>

defgeneric(void, gfunction3, _1 ,(int)i);
```


#### Clases
Las clases poseen visibilidad de declaración y de implementación como ya hemos visto en otros frameworks, pero a diferencia de ellos no es necesario escribir otro archivo de interfaz adicional, el único que se debe escribir es el que corresponde a la visibilidad de implementación.


##### interfaz (.h)
Sólo es necesario definir los atributos de la clase, su nombre y su clase padre.
```c
//Child.h
#include "Parent.h"

defclass(Child, Parent)
	int attribute;
	OBJ class1;
endclass
```
En caso de que una clase descienda de `Object` el segundo argumento de `defclass()` es omitido. 

##### implementación (.c)
Mostraremos la instanciación de la clase junto a la implementación de algunos métodos.
```c
//Child.c
#include "Child.h"
#include "gfunction.h"
#include "gfunction2.h"

#include <cos/gen/message.h>
```
La siguiente declaración instancia la clase:
```c
makclass(Child, Parent);
```
Antes de poder utilizar una clase externa, la misma debe ser declarada con `useclass()`:
```c
useclass(Class1);
```
Los siguientes son los métodos de inicialización y destrucción de la clase:
```c
defmethod(OBJ, ginit, Child)
  self->class1 = gnew(Class1);
  retmethod(_1);
endmethod

defmethod(OBJ, gdeinit, Child)
  grelease(self->class1);
  retmethod(_1);
endmethod
```
El siguiente método es una especialización de la función genérica `gfunction()`, a partir del tercer argumento de `defmethod()` se indican los tipos específicos:
```c
defmethod(void, gfunction, Child, Object)
  /*...*/
endmethod
```
Es posible asignar una misma implementación a varias funciones genéricas, esto se hace mediante los alias, a continuación se asigna la implementación anterior de `gfunction()` a `gfunction2()`:
```c
defalias(void, (gfunction )gfunction2, Child, Object);
```
Se puede definir un método rodeador (en inglés around) que es ejecutado en vez del método que rodea y puede definir acciones antes y después del mismo:
```c
defmethod(void, (gfunction), Child, Object)
	/*antes de gfunction*/
	next_method(self1,self2); //llamada a gfunction()
	/*después de gfunction*/
endmethod
```
El siguiente es un ejemplo de reenvío de mensajes a `class1`:
```c
defmethod(void, gunrecognizedMessage1, Child)
		if(gunderstandMessage1(self->class1,_sel) == True)
			forward_message(self->class1); // delegación
endmethod
```

#### Propiedades
Las propiedades en COS son parte de su biblioteca base, es decir, no tienen un soporte especial desde su modelo de objetos sino que son soportadas tan solo con los multimétodos.


##### Definición
```c
//value.h
#include <cos/Property.h>

defproperty( value );
```


##### Definición como propiedad de clase
```c
//ChildProperties.c
#include "Child.h"
#include "value.h"
#include <cos/Number.h>
#include <cos/gen/value.h>


static OBJ int2OBJ(int val) { 
return  aInt(val); 
}


defproperty(Child, (attribute)value,int2OBJ,gint);
```
En la definición anterior de la propiedad el atributo `attribute` es accedido a través de la propiedad `value`. 


### Dificultades en la codificación
Muy pocas dificultades podemos mencionar para el prototipado bajo este framework. Primero el nuevo lenguaje debe aprenderse. Segundo podemos notar la duplicación de información en la declaración de una clase y su herencia en las macros `makclass()` y `defclass()`.
Una dificultad más encontrada que no entra dentro de nuestro estudio pero vale la pena mencionar para trabajos futuros es que una biblioteca de macros tan compleja produce que se arrojen una gran cantidad de mensajes de error al introducir un error de sintaxis al codificar bajo la especificación del framework, esto dificulta encontrar dicho error, por lo que una herramienta previa que analice errores en el uso del framework podría ser de gran utilidad.


### Propuesta de expresión en UML
Cómo modelar los multimétodos en UML requiere un análisis profundo ya que fueron concebidos luego de que UML haya sido creado y puede no ser un buen lenguaje de modelado para los mismos. Esta propuesta puede ser tomada como puntapié para el modelado bajo otros lenguajes que soportan multimétodos como CLOS.

<!--
@startuml
class gfunction <<generic>>{
void gfunction(self,arg1) 
}

class gfunction2<<generic>>{
void gfunction2(arg1,arg2)
}


class fimpl1 <<method>>{
void gfunction(Child,Object) 
}
gfunction <|.. fimpl1
gfunction2 <|.. fimpl1
@enduml
-->
![](http://www.plantuml.com/plantuml/png/SoWkIImgAStDuKhEIImkLaXFAyrBBidCprEmiKbFpKijoaosiwlcAilFJ478QHIdvgJf91Qb6segSDLoeMirmgFLI0Ea3QJNI1Ee5QOtBJEt8CSGQ6TkQab6VWeMAvqpCdDITFoJibAJIy2s8hcqHayFQW92r0X5s24rBmNeIG00)\ 

<!--
@startuml
class Parent{
}

class Child{
type attr
type1 gfunction4(Object,Object,Object,Object,Child,type2,type3)
gunrecognizedMessage1(Child)
void (gfunction2)(Child,Object) 
}

Parent <|-- Child 


class value <<property>>


value <|.. Child

class mygenerics<<generic>>{
type1 gfunction4(arg1,arg2,\n arg3,arg4,arg5,type2 arg6,type3 arg7)
void gfunction5(_1)
}

mygenerics <|.. Child
@enduml
-->
![enter image description here](http://www.plantuml.com/plantuml/png/RP71QiCm38RlUWeTOqWBITljOa9Xvx8zm61uZkPvf4wmbKAMxjrNnsuts2wIBF-Iln_l0olFux43tQaGy4bvSZp35-J6mxljsXbu6WWLir-g2itRw3JRtcsBnzSFqbp-cvRfCexKIzm8CAFpf7lZx2Ur1mf16QgAHIZWrDiMYzloMgIBl4vWv4g8AC-hLQ93kCAUL3SIIZduVY3FKzC0G6wUr-ialugFao57tkeWPQwQPlxhJtbJbPTGbyyEBtaJ3ziOTibPRDubUx6yppPkAtR5IoKY-S-Blt7svDhu0Ty0)
\ 

La base de este modelado es relacionar clases (sujetos) y funciones genéricas (verbos) a través de métodos (especialización).  
Las clases poseen atributos y métodos (que explicaremos mejor a continuación). Pueden heredar de una única clase y en caso de no indicarse una herencia heredan de la clase `Object`.
Las funciones genéricas se definen como operaciones dentro de una clase con  el estereotipo `<<generic>>` (esto permitiría que una o varias funciones genéricas sean definidas en un mismo archivo). Los argumentos de las funciones genéricas que no poseen un tipo son tipos abiertos y se admiten hasta cinco (que deben además ser los primeros), los argumentos que posean tipo serán tipos cerrados.
Los métodos pueden ser definidos tanto dentro de una clase como fuera de ellas dentro de una clase con el estereotipo `<<method>>` (esto permite que sean definidas en un archivo aparte del de alguna clase). Los métodos pueden realizar varias funciones genéricas (en el ejemplo el método `gfunction4()` en `Child` realiza `gfunction4()`), en caso de realizar más de una función genérica con la misma signatura, las mismas son alias entre sí, si el método tiene el mismo nombre que alguna de ellas, la misma es definida (`defmethod`) y el resto son considerados alias (`defalias`). Los métodos deben contener la misma cantidad de argumentos que la función genérica que realizan y deben definir todos sus tipos. En el caso de que un método no realice una función genérica se considera que realiza a la función genérica que posee su nombre (en el ejemplo sería el caso de `gunrecognizedMethod1()`), de ser necesario, una dependencia de tipo usage deberá hacer visible la función genérica para el método (relacionando a ellos dos o a sus contenedores).
Un método around no agrega ninguna regla adicional a las anteriores para poder modelarlo, tan sólo se precisa un método con el nombre de la función genérica entre paréntesis (En el ejemplo es el caso de `(gfunction2)()` dentro de `Child`).   
Las propiedades se modelan como clases (ya que internamente es lo que son) con el estereotipo  `<<property>>` y pueden relacionarse con un atributo que las realice (en el ejemplo el atributo `attr` de `Child` está realizando a la propiedad `value`).   


## Conclusiones
Hemos visto una gran cantidad de frameworks muy dispares entre sí en sus  modelos de objetos, algunos tan solo dando soporte al polimorfismo y otros con modelos comparables con los de C++ (OOPC), JAVA (OOC-S y OOC de Miseta), Small Talk (Dynace) y otros con CLOS (Dynace y aún más COS). Sin embargo, respecto a lo que atañe a la dificultad en el prototipado bajo cada uno de ellos podemos sacar varias conclusiones:  
Los frameworks que se valen de herramientas externas en el proceso de convertir código fuente en artefactos como bibliotecas o ejecutables pueden sortear las dificultades del prototipado de objetos en C, en general la herramienta más utilizada es un preprocesador como en el caso de Dynace (también OOC de Schreiner y para el caso de GObject existen compiladores fuente a fuente como Vala y Genie). La desventaja principal que se introduce al utilizar este enfoque es que  el editor que se utilice no entenderá el código que estamos escribiendo por lo que herramientas de búsqueda, autocompletar o refactoring no nos servirán. Además, un nuevo lenguaje -y no estándar- debe aprenderse. Por otro lado, como se vió en el caso de Dynace, un preprocesador externo puede dejarnos tan sólo con la dificultad un lenguaje muy reducido en nuevas palabras respecto del lenguaje C. 
Entre los frameworks que se valen de herramientas externas COS se distingue de los demás valiéndose tan sólo de una etapa extra de linkeo y una increíble biblioteca de macros C99, esto le permite utilizar C puro sin adición de dificultades significativas en el prototipado. Creemos que este framework debe tomarse como objeto de investigación para futuros frameworks que deseen implementar un modelo de objetos distinto y quizás uno aplicable a los sistemas embebidos.
Respecto del resto de los frameworks que no se valen de herramientas externas  (de los estudiados: Ben Klemens, SOOPC, OOC de Tibor Miseta, OOPC, OOC-S y GObject), a pesar de que el procesador de C y bibliotecas puede ayudar, precisan una gran cantidad de código repetitivo y repetición de información para el prototipado de clases. La forma en que el código crece en líneas al adicionar métodos a las clases es de aproximadamente 3 a 5 veces más respecto de utilizar un LPOO. Justamente algunos de estos frameworks son los más apropiados para sistemas altamente restringidos. Pero como ya hemos dicho, evaluar el framework en soledad no es algo correcto. Todavía podemos introducir herramientas dentro del tool suite del programador que disminuyan dichas dificultades y nos permitan seguir trabajando bajo el lenguaje C. Nuestra propuesta es un generador de código desde diagramas UML a introducir en el siguiente capítulo.


