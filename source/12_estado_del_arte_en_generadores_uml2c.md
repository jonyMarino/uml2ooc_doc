# Estado del arte en generadores de código C desde diagramas UML


## El uso de modelos para la generación de código C orientado a objetos
Una de las herramientas adecuadas para conseguir un toolsuite que simplifique la codificación OO en C es un generador de código desde UML [@Hendrickx2004], sobre todo en lo que respecta al prototipado de clases que fue el centro de atención en las dificultades que exploramos en el capítulo anterior . A través de la generación automática de código se reducen errores y esfuerzo [@Maranzana], esto es especialmente valedero para cuando hablamos en COO. Todavía hay mucho por desarrollar en esta disciplina de la generación automática de software que se encuentra “inmadura por sus restricciones o pobre en su optimización” [@Murillo]. Las aplicaciones para generar diagramas UML por sí solas pueden ser útiles pero su verdadera utilidad se da cuando tienen un generador de código automático asociados a ellos y, al mismo tiempo, son costeables por el presupuesto del desarrollo [@economics]. En esta sección mostraremos que esto es posible con herramientas de código abierto.


## Estado del arte en generadores de código C desde diagramas de clase UML
Si bien existen herramientas para generar código C desde el lenguaje UML, todavía no existe un generador de código C que se ajuste correctamente al paradigma de la orientación a objetos y tampoco para ninguno de los frameworks estudiados.  
A continuación introduciremos la capacidad de generación de código orientado a objetos por las siguientes herramientas populares: Enterprise Architect [@Enterprise], Rational Rhapsody [@Rational], Astah [@Astah] y UML Generators [@UMLGenerators] (esta última no fue seleccionada por su popularidad sino más bien porque será la base del generador de código que propondremos).


### Enterprise Architect
La siguiente cita fue extraída de la página de Enterprise Architect respecto de cómo modelar COO en Enterprise Architect:

<!--
> **Object-Oriented C Code Generation for UML Model**
>
>The basic idea of implementing a  UML Class  in C code is to group the data variable (UML attributes) into a structure type; this structure is defined in a .h file so that it can be shared by other Classes and by the client that referred to it.
>
>An operation in a UML Class is implemented in C code as a function; the name of the function must be a fully qualified name that consists of the operation name, as well as the Class name to indicate that the operation is for that Class.
>
>A delimiter (specified in the 'Namespace Delimiter' option on the 'C Specifications' page) is used to join the Class name and function (operation) name.
>
>The function in C code must also have a reference parameter to the Class object - you can modify the 'Reference as Operation Parameter', 'Reference Parameter Style' and 'Reference Parameter Name' options on the 'C Specifications' page to support this reference parameter.
> 
> **Limitations of Object-Oriented Programming in C**
> 
> -   No scope mapping for an attribute: an attribute in a  UML Class  is mapped to a structure variable in C code, and its scope (private,
> protected or public) is ignored
> -   Currently an inner Class is ignored: if a UML Class is the inner Class of another UML Class, it is ignored when generating C code
> -   Initial value is ignored: the initial value of an attribute in a UML Class is ignored in generated C code
-->
> Generación de código C orientada a objetos para el modelo UML
>
> La idea básica de implementar una Clase UML en código C es agrupar la variable de datos (atributos UML) en un tipo de estructura; esta estructura se define en un archivo .h para que pueda ser compartida por otras Clases y por el cliente que se refirió a ella.
>
> Una operación en una clase UML se implementa en código C como una función; el nombre de la función debe ser un nombre completamente calificado que consiste en el nombre de la operación, así como el nombre de la Clase para indicar que la operación es para esa Clase.
>[...]
>
> La función en el código C también debe tener un parámetro de referencia para el objeto Clase: puede modificar las opciones 'Referencia como parámetro de operación', 'Estilo del parámetro de referencia' y 'Nombre del parámetro de referencia' en la página 'Especificaciones de C' para admitir esto parámetro de referencia.
>
>  Limitaciones de la Programación Orientada a Objetos en C
>
> - Sin asignación de visibilidad para un atributo: un atributo en una Clase UML se asigna a una variable de estructura en el código C, y su alcance (privado,
> protegido o público) se ignora.
> - Actualmente se ignora una Clase interna: si una Clase UML es la Clase interna de otra Clase UML, se ignora al generar el código C.
> - Se ignora el valor inicial: el valor inicial de un atributo en una Clase UML se ignora en el código C generado.  
[@Enterprise]

Como podemos ver, sólo una noción básica de encapsulamiento es soportada por este generador de código, ninguna especificación de herencia o polimorfismo es dada, quizás lo mejor que podríamos hacer es incluir una estructura padre dentro de otra hija para obtener herencia, y declarar punteros a función como métodos polimórficos, pero estaríamos abandonando ya la sintaxis estándar de UML para dichos conceptos.


### Rational Rhapsody
Douglass [@Douglass2010] propone representar las clases en estereotipos UML de tipo `<<File>>` que pueden procesarse en Rhapsody para obtener archivos .c y .h colocando las variables, declaraciones, funciones y prototipos en el archivo correspondiente de acuerdo a la visibilidad definida en el estereotipo. De nuevo tenemos los mismos inconvenientes que para con la herramienta anterior.


### Astah
Astah posee un plugin llamado UML2c concebido con el propósito de facilitar la codificación bajo la especificación de Grenning [@Grenning] para obtener código orientado a pruebas. El plugin además de generar código C, genera el código de mocks para ser utilizados con Google Test [@GoogleTest].  
Esta es la primer herramienta que facilita en alguna medida el uso del polimorfismo bajo lenguaje C.
A continuación se presenta un diagrama de muestra del plugin con los distintos elementos de los cuales genera código. 


![enter image description here](http://astah.net/features/styles/images/plugins/uml2c-7.png)\  

Como puede apreciarse existe una relación de herencia entre la clase padre `Abstract` y la clase hija `Concrete`, `Abstract` está definida como una clase abstracta en el diagrama.  
El generador de código genera 3 archivos por cada clase, uno de implementación (.c), y 2 de interfaz (.h). En uno de los archivos de interfaz, si la clase es abstracta, entonces se crea una estructura con punteros a función que corresponden a las operaciones definidas para esa clase, esta estructura serviría para instanciar una tabla virtual para la clase y sus derivadas. Luego es definida una estructura de instancia de clase cuyo primer miembro es un puntero a la tabla virtual y a continuación los atributos de la clase.  
Las clases hijas no extendien a la tabla virtual por más que también sean abstractas, por lo que esta facilidad solo sirve para 1 nivel de herencia.  
Los demás archivos de la clase hija son esqueletos de las operaciones definidas, por lo que no existen más facilidades que esta.  
La clase hija no contiene ninguna referencia a la clase padre por lo que tampoco brinda facilidades en la implementación del polimorfismo (instanciación de la tabla virtual, asignación de su referencia en una instancia, asignación de implementaciones de funciones).


A continuación presentamos el código generado para la clase `Abstract` y `Concrete`
```c
//Abstract.c
#include "Abstract.h"

void Abstract_Destroy(Abstract self)
{
	/* TODO */
}

uint8_t Abstract_Operation(Abstract self)
{
	/* TODO */
}
```
```c
//Abstract.h
typedef struct AbstractStruct *Abstract;


void Abstract_Destroy(Abstract self);
uint8_t Abstract_Operation(Abstract self);

#include "AbstractPrivate.h"
```
```c
//AbstractPrivate.h
typedef struct AbstractInterfaceStruct *AbstractInterface;


struct AbstractStruct {
	AbstractInterface vtable;
	uint64_t id;
	char *type;
};


typedef struct AbstractInterfaceStruct {
	void (*Destroy)(Abstract self);
	uint8_t (*Operation)(Abstract self);
};
```
```c
//Concrete.c
#include "Concrete.h"

void Concrete_Create(Concrete super, uint64_t id, const char *type)
{
	/* TODO */
}

void Concrete_Destroy(Concrete super)
{
	/* TODO */
}

uint8_t Concrete_Operation(Concrete super)
{
	/* TODO */
}
```
```c
//Concrete.h
typedef struct ConcreteStruct *Concrete;


void Concrete_Create(Concrete super, uint64_t id, const char *type);
void Concrete_Destroy(Concrete super);
uint8_t Concrete_Operation(Concrete super);

#include "ConcretePrivate.h"
```
```c
//ConcretePrivate.h
struct ConcreteStruct {
};
```
Como puede apreciarse, la ayuda que provee este generador de código es muy reducida dejando gran cantidad de trabajo al programador tan solo para el prototipado.


### UML Generators
UML generators es un proyecto de código abierto que se compone de varios generadores de código desde UML (algunos implementan la ingeniería inversa). Los generadores están escritos en Acceleo [@Acceleo]. Acceleo implementa el estándar de transformación de modelo a texto del MOF (model object facility) perteneciente al OMG (object model group). El mismo utiliza OCL (object constraint language) para obtener información del modelo a convertir en texto.  
UML Generators posee 2 generadores de código desde UML a lenguaje C. Uno de ellos (C generator) posee una aplicación de ingeniería inversa para generar los modelos desde el código y el otro (UML to Embedded C generator) está orientado a su uso en sistemas embebidos.


#### C generator
<!-- https://www.eclipse.org/umlgen/documentation/releases/1.0.0/org.eclipse.umlgen.gen.c.doc/c-generator/user/user-guide.html -->
C generator utiliza la notación de UML pero define un lenguaje propio para modelar el código que el programador C está acostumbrado a escribir. Así por ejemplo, una clase privada representa un archivo de implementación (.c), una interfaz un archivo de interfaz (.h) y una clase pública a ambos. Esto ya nos indica que ni siquiera el concepto de clase de la OO es facilitado por este modelado. 


#### UML to Embedded C generator
UML to Embedded C generator es una contribución de Spacebel [^4] a UML Generators. 
El objetivo de este generador ha sido:  

1. Generación de código repetible y confiable.
2. Preservación de los campos de implementación. 
3. Diseño detallado altamente documentado. 
4. Cumplimiento de las directrices comunes del lenguaje C en el dominio aeroespacial. 
5. Trazabilidad de la especificación (requisitos) en las fuentes. 

Para extender la semántica de UML posee un perfil (en inglés profile) UML que posee los estereotipos que precisa para su modelado.  
El modelado y la generación de código orientado a objetos soportados son muy similares a los vistos en Enterprise Architect y Rhapsody. Una clase representa un TDA (tipo de dato abstracto) cuyos atributos son declarados en una estructura de instancia de la clase, las operaciones públicas definidas en la clase pasan a ser funciones declaradas en el archivo de interfaz donde se declara el TDA y las privadas son definidas en el archivo de implementación.   
La relación de generalización no tiene significado para este generador por lo que los conceptos de herencia no son facilitados.


## Resumen
La conveniencia de expresar modelos aparte del código ha impulsado el desarrollo de generadores de código C desde diagramas de clase UML para expresar la estructura contenida en dicho código. Debido a que C no es un LPOO mientras que UML es un lenguaje inherentemente orientado a objeto ha llevado a un recorte del lenguaje o redefinición de su semántica para ser utilizado junto con C. En general, de los conceptos de la OO, el encapsulamiento es el único concepto soportado por estos generadores. El único generador que incorpora en alguna medida herencia y polimorfismo es el uml2c de astah, la herencia soportada es de hasta un nivel (un padre y varios hijos que no pueden a su vez ser padres de otras clases) y el código generado deja mucho de la implementación al programador (inclusión de la clase padre en la del hijo, inicialización de la tabla virtual, inicialización de un objeto con su clase padre).  
El estado actual de los generadores de código C desde UML nos muestra que un generador que implemente varios conceptos de la orientación a objetos y que se encargue en su totalidad del prototipado de los artefactos relacionados con la orientación a objetos (clases, interfaces, etc.) está vacante.

 


[^4]: https://www.spacebel.be
