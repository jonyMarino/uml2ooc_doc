# Implementación de generadores de código C embebido, eficiente y orientado a objetos desde diagramas de clase UML


## Punto de partida
En base a nuestro estudio en el capítulo anterior hemos decidido investigar la factibilidad de generar código desde UML para los frameworks estudiados (de acuerdo a sus propuestas de expresión presentadas) con el lenguaje Acceleo debido a que es el lenguaje basado en un estándar del OMG (Otros lenguajes utilizados para transformar modelos en texto son Xpand[@XPand] y Xtend[@Xtend]), además, este estudio permitirá extender el generador de código UML to Embedded C generator de UML generators que fue realizado para el dominio de aplicaciones que han sido el objetivo de varios de los frameworks estudiados, sin por eso ser inadecuado para su uso en otros dominios.  
Acceleo permite delinear en el código generado los lugares donde el programador puede codificar sin que el código sea borrado por el generador de código. 


## Factibilidad
Los lenguajes de modelo a texto, y en particular Acceleo, ya tienen su experiencia en haber sido usados para implementar generadores para distintos LPOO, para eso fueron concebidos.  
La pregunta está en si estos lenguajes utilizados junto con el metamodelo de UML nos permitirán generar código para los frameworks de COO estudiados.    
Esta pregunta puede no tener una respuesta trivial ya que el código escrito bajo estos frameworks requiere de información adicional, obtenible del modelo mediante consultas personalizadas, del que se requiere en los LPOO, por ejemplo qué clase fue la primera en declarar cierto método para determinar el tipo del argumento `self` (si es el caso para ese framework), o poder a partir de esa información codificar la inicialización en ROM de una tabla virtual con los métodos ordenados en forma correcta en dicha tabla bajo el estándar ISO C89 (a partir del ISO C99 la inicialización de una estructura se puede codificar con el nombre de sus miembros y no con el orden de los mismos). Los únicos frameworks que no presentan dificultades de este tipo son Dynace y COS.  
<!--En la medida que esta herramienta nos permita interrogar al modelo en los puntos que nos interesan para la generación automática como poder generar una salida a través de ellos podremos cumplir nuestro objetivo.
-->


## Verificaciones
Para verificar que las consultas sean correctas se ha analizado el código generado a partir de ellas, desde un modelo UML. En la mayoría de los casos el generador utilizado ha sido el desarrollado a partir del *UML to Embedded C generator* de *UML Generators* para los frameworks SOOPC, OOC de Miseta y Dynace.  
Para cada una de las siguientes secciones de generación de código se encuentran tanto el modelo como el código utilizado.


## Mejoras en el código para facilitar la generación de código
Si bien nos limitaremos a generar código para los seis frameworks que se han vuelto el objetivo de esta tesis (Ben Klemens,  SOOPC, OOC-S, OOPC , OOC de Miseta y  OOC de Schreiner) no nos contendremos en proponer mejoras en las especificaciones de codificación de dichos frameworks que faciliten el mapeo entre modelos UML y dicho código. Esto será útil ya que simplificará a los generadores de la nueva  especificación e incluso facilitará en alguna manera al programador de clases. Estas mejoras pueden tener su contrapartida, quizás la adición de más líneas de código o el cambio de estándar de codificación C (por ejemplo de ISO C89 a C99).  


## Generación de código
Hay algunas cuestiones que tomaremos a priori (acertadamente) como posibles a resolver sin problemas bajo Acceleo y que son comunes a otros lenguajes de programación orientado a objetos: obtener toda la información de una clase, sus atributos y métodos y sus relaciones inmediatas con otros.  
Teniendo esto en cuenta sólo deberemos investigar la factibilidad y facilidad de obtener información no inmediata a la clase o artefacto a generar. Las siguientes secciones tratarán esos puntos.  


### Plantilla para generar la tabla virtual en SOOPC
Lo primero que intentaremos resolver es generar el código de la instanciación de la tabla virtual de una clase escrita para SOOPC.
Para esto deberemos poder llegar hasta el final de la cadena de herencias hasta la primer clase que declare método polimórficos, cada clase hija generará llaves nuevas debido al anidamiento de estructuras además de agregar métodos nuevos.  
  
Lo primera información que buscaremos obtener es toda la jerarquía de clases (por tratarse de herencia simple esto puede obtenerse en una lista). Esto podemos hacerlo utilizando una consulta (query) recursiva.
```java
[query 
public 
getEntireClassesHierarchechy(aClass: Class) : OrderedSet ( Class ) =
if aClass<>null then
	aClass.superClass.getEntireClassesHierarchechy() -> asOrderedSet()
		->prepend(aClass)
else
	OrderedSet{}
endif
 /]
```

Luego de que podemos obtener esta lista podemos fácilmente obtener la lista de clases padre de una clase excluyendo a la clase misma de la lista. Esta consulta la llamamos `getAncestors()`.  
Luego para cada clase deberemos saber si los métodos virtuales que definen se tratan de una redefinición polimórfica de los mismos (por lo que está declarada en una tabla virtual anterior) o es la primera definición de la misma por lo que genera una referencia nueva en su tabla virtual.  
La siguiente consulta nos resuelve eso:
```java
[query 
public 
isAMethodRedefinition(o: Operation) : Boolean =
o.class.getAncestors().allNotFinalOperations()
	->exists(o1:Operation |redefine(o1,o))/]
```
En Acceleo al aplicar una función sobre un contenedor (como el `OrderedSet` devuelto por `getAncestors()`) con el símbolo `->` entonces el contenedor mismo es computado devolviendo cualquier otro tipo, al aplicar una función con `.` entonces la función es operada sobre cada miembro del contenedor devolviendo un contenedor nuevo (conteniendo los tipos devueltos por la función).  
De esta manera la consulta anterior se lee de la siguiente manera: para la operación `o` se obtiene la clase la contiene, de esa clase se obtiene la lista de sus ancestros, luego se obtiene un contenedor reemplazando cada ancestro por sus métodos polimórficos (devueltos por la función `allNotFinalOperations()`) luego se interroga si dentro del conjunto de los métodos polimórficos de las clases padre existe (`exists()`) un método que tiene el mismo nombre que el método `o` (`redefine(o1,o)`).  
De esta manera podemos escribir fácilmente otra consulta que nos ayude a obtener los métodos polimórficos definidos por primera vez por la clase que llamaremos `getNotRedefine ()`.  
La última información que deberemos poder obtener es cual es la última implementación disponible de cierto método para asignarla a la tabla virtual. Para eso usamos la recursividad comenzando desde las operaciones de la clase yendo hacia la de su ancestro.
```java
[template 
public 
getLastClassDefinerName(aClass: Class ,aMethod: Operation)]
	[if (aClass.ownedOperation->exists(m:Operation | 
		m.redefine( aMethod ) ))]
		[aClass.name/]
	[else]
	[if aClass.hasSuperclass()]
		[aClass.getSingleInheritanceSuperClass().
			getLastClassDefinerName(aMethod)/]
	[/if]
	[/if]
[/template]
```
`getLastClassDefinerName()` recibe la clase de la que se quiere saber cual es la última clase en su jerarquía que implementa el método pasado.   

Luego que tenemos todas estas consultas podemos crear una plantilla para instanciar la tabla virtual de una clase: 
```java
static [aClass.name/]Vtbl const vtbl={
[for (c : OclAny | aClass.getAncestors()) ]{[/for] 
[for (c : Class | entireClassesHierarchechy(aClass)->reverse()) ]
[let seqOp2 : Sequence(uml::Operation) = 
	getNotRedefine ( getOwnedOperations ( c ) ) ]
[for (o : Operation |  seqOp2 ) separator (',')]
	[aClass.getLastClassDefinerName(o)/]_[o.name/]_
[/for]	
[/let]
[if (c <> aClass)]		},[/if]
[/for]
};
[/if]
```
Generar el código de declaración de la clase virtual es mucho más simple ya que solo debe conocerse la clase padre y los nuevos métodos polimórficos (`getNotRedefine ()`). 


#### Verificación
Utilizando el siguiente modelo UML:  
![](source/figures/soopc_model.png)\
 

Donde `vPrint()` es definida como un método abstracto y `getNameFollowedByClassName()` como un método polimórfico (`isLeaf` falso), la plantilla anterior genera la siguiente tabla virtual para la clase `GrandChild`:  
```c
static GrandChildVtbl const vtbl={
{{
	Child_vPrint_			},
	GrandChild_getNameFollowedByClassName_			},

};
```  
Puede notarse el uso correcto de llaves para asignar la función en el nivel de anidamiento de estructura correspondiente ( `vPrint()` en la estructura `ParentVtbl` y `getNameFollowedByClassName()` en `ChildVtbl` que incluye a la anterior y a la vez es contenida por `GrandChildVtbl`)
Por cuanto que la última clase en implementar `vPrint()` fue `Child` (obtenido mediante la consulta `getLastClassDefinerName()`) la misma es la utilizada.


#### Mejoras
Como puede apreciarse, el aspecto más difícil del mapeo es la obtención de nueva información del modelo por encima definir una plantilla con mucha información en ella. Esta será la guía para todas las mejoras propuestas.
La mejora propuesta es primero reemplazar el anidamiento de estructuras con nombre por las estructuras anónimas de ISO C11 que vimos en [Ben Klemens](#BenKlemens) (pero en este caso para la tabla virtual), con esto los métodos de la tabla virtual pueden inicializarse con su nombre:
```c
static ChildVtbl const vtbl={
.method1 = Parent_method1_
.method2 = Child_method2_
}
```
 Luego para poder mantener un listado de las últimas implementaciones, una macro de inicializaciones puede mantenerse, el siguiente sería el template de la misma:
```java
#define [aClass.name.toUpper()/]_METHOD_IMPLEMENTATIONS \
	[aClass.superClass.toUpper()/]_METHOD_IMPLEMENTATIONS \
	[for (o : Operation |  aClass.allNotFinalOperations() )]
	.[o.name/] =  [aClass.name/]_[o.name/]_,
	[/for]
```
Luego de esta plantilla simple de generar, una plantilla para la instanciación de la tabla virtual se reduciría a:
```c
static [aClass.name/]Vtbl const vtbl={
[aClass.name.toUpper()/]_METHOD_IMPLEMENTATIONS
}
```
Esto es posible ya que desde C99 la asignación utilizada en la lista de asignaciones es la última que aparece. De esta manera nos hemos deshecho de todas las consultas que necesitamos para la instanciación de la tabla virtual. 

Una dificultad adicional a resolver sería no agregar a la declaración de la tabla virtual métodos redefinidos polimórficamente sin tener que consultar con la consulta `getNotRedefine ()`.  
Este se puede lograr creando macros y compilaciones condicionales por cada método polimórfico:
```c
typedef struct [aClass.name/]Vtbl={
	struct [aClass.superClass.name/]Vtbl;
		[for (o : Operation |  aClass.allNotFinalOperations() )]  
		#ifndef [o.name.toUpper()/]
		#define [o.name.toUpper()/]
		[o.generateSOOPCMethodPrototype()/],
		#endif
		[/for]
}
```
Ahora una clase hija que defina de nuevo el método polimórfico no lo estará incluyendo en su estructura de tabla virtual por más que desconozca que se trata de una redefinición. La desventaja de esto es que ahora no podrán existir dos métodos con el mismo nombre.
De esta manera también pasamos la resolución de esta problemática al precompilador C.


### Plantilla para el prototipo de un método polimórfico en SOOPC
Los métodos polimórficos son declarados por primera vez en alguna clase tabla virtual de alguna clase con el primer argumento (`self`) siendo del tipo de esa clase. Si una clase heredada quiere redefinir dicho método, deberá respetar su firma original para no obtener errores de parseo. Para esto es necesario poder saber cual es la primer clase que definió el método. Esto se puede conseguir con una consulta `getFirstClassDefinerName()` que funcione a la inversa de la ya presentada `getLastClassDefinerName()`.

```java
[template public getFirstClassDefinerName(classes: OrderedSet(Class) ,aMethod: Operation) 
post(substituteAll('\t','').trim())]
[let c: uml::Class = classes->first() ]
[if (c.ownedOperation->exists(m:Operation | m.redefine( aMethod ) ))]
	[c.name/]
[else]
[comment] **Note** : at the time of writing, the OCL standard library sports a bug 
which changes *OrderedSets* in *Sets* when excluding elements.[/comment]
	[getFirstClassDefinerName(classes -> asSequence() -> excluding(c) -> asOrderedSet() , aMethod)/]
[/if]
	[/let]
[/template]
```

Con esa consulta podemos generar la siguiente plantilla para los prototipos de métodos virtuales:
```java
[template public genVirtualMethodPrototype (o : Operation) ]
[generateReturnType(o)/] [o.class.getName()/]_[o.getName()/]_ ([o.class.getFirstClassDefinerName(o)/] * self[for(p:Parameter | getOperationParameters(o)) before(', ') separator(', ')][p.genParameter()/][/for])
[/template]
```

#### Verificación
Utilizando el modelo de la sección anterior, para el método `vPrint()` de Child la plantilla `genVirtualMethodPrototype()` genera el siguiente código.
```c
void Child_vPrint_ (Parent * self)
```
Se puede ver que a pesar de que `Child` es la clase que está redefiniendo el método, el argumento utilizado para el argumento `self` es la clase `Parent` la cuál es la primera en declarar el método `vPrint()`.


#### Mejoras
De nuevo para simplificar el mapeo esta información puede proveerla el preprocesador de C. Si cuando declaramos el método definimos una macro con el nombre de la clase. Junto con la mejora anterior esto quedaría de la siguiente manera:
```c
typedef struct [aClass.name/]Vtbl={
	struct [aClass.superClass.name/]Vtbl;
		[for (o : Operation |  aClass.allNotFinalOperations() )]  
		#ifndef [o.name.toUpper()/]
		#define [o.name.toUpper()/]
		#define [o.name.toUpper()/]_SELF_TYPE [aClass.name/]
		[o.generateSOOPCMethodPrototype()/],
		#endif
		[/for]
}
```
Con esto definir el prototipo de la implementación de un método virtual solo requiere conocer el nombre del método, lo que es accesible desde la clase misma, para poder utilizar `[o.name.toUpper()/]_SELF_TYPE` como tipo del primer argumento.



### Redefinición de métodos polimórficos en OOPC
Como vimos, OOPC soporta herencia múltiple. El framework se vale de una importante biblioteca de macros. Para poder redefinir un método se utilizan las macros `methodOvldDecl()` (o su equivalente para métodos constantes) y `methodOvldName()`. Ambas reciben como primer argumento el nombre del método y como segundo la clase que definió el método. Además se debe utilizar la macro `overload()` con toda la secuencia línea de ascendencia de clases hasta llegar a la que definió el método por primera vez. Esta tarea se complica aún más cuando 2 (o más) clases contienen un método con el mismo nombre y firma pero no pertenecen a la misma jerarquía de clases, si una clase hereda de estas 2 clases y define dicho método entonces deberá generar dos implementaciones y redefiniciones para cada clase método.

Primero intentaremos obtener distintos listados de de líneas de ascendencias que llegan hasta las clases que definen por primera vez al método.

El primer objetivo entonces es encontrar las clases que definen por primera vez el método. Para eso de nuevo debemos recurrir a la recursividad con la siguiente consulta:
```java
[query 
public 
getFirstClassesDefinerName(aClass: Class ,anOperation: Operation): Set(Class)=
		
if (not aClass.superClass->isEmpty() and aClass.superClass
		->exists( s:Class| s.hasOperation(anOperation))) then
	Set(Class){}->union ( aClass.superClass->select( s|
	 s.hasOperation(anOperation))
		.getFirstClassesDefinerName(anOperation)->asSet() )
else
   Set(Class){}->including(aClass)
endif
/]
```
la idea es que una clase solo debe agregarse como definidora si las clases padre de la misma no contienen dicho método.
Para preguntar si las clases padre contienen dicha operación utilizamos la consulta `hasOperation()`:
```java
[query 
public 
hasOperation(aClass: Class ,anOperation: Operation): Boolean=
if( aClass.ownedOperation->exists( op 
		| op.redefines(anOperation) ) ) then	
	true
else
	if aClass.superClass->isEmpty() then
	 false
	else
	 aClass.superClass->exists(s:Class
		| s.hasOperation(anOperation))
	endif
endif
/]
```
La consulta `redefines()` debe ser cierta si el nombre de las operaciones y los atributos de sus argumentos (tipo, dirección, etc) en orden son iguales. Esto lo podemos conseguir con las siguientes queries:
```java
[query public redefines(op1: Operation,op2: Operation): Boolean=
if(op1.name = op2.name)then
 op1.ownedParameter->asOrderedSet()->forAll( par1:uml::Parameter |
 op2.ownedParameter->exists(par2:uml::Parameter | par2.type.name = 
 par1.type.name and par1.direction = par2.direction and 
 (par1.direction = ParameterDirectionKind::return 
 or parameterIndex(op1,par1) = parameterIndex(op2,par2))))
else
 false
endif
/]

[query 
public 
parameterIndex (op1 : Operation,par1: Parameter) : Integer = 
 op1.ownedParameter->asOrderedSet()->excluding
  (op1.ownedParameter->any(direction = ParameterDirectionKind::return))
   ->asOrderedSet()->indexOf(par1)/]
```

La siguiente tarea será conseguir todo el camino de herencia desde las clases que definen por primera vez la operación hasta la clase a generar. Si nos encontramos en un caso del problema del diamante los caminos pueden ser varios. Este problema es difícil de resolver en OCL y requerimos ayuda de la comunidad de Acceleo para resolverla.  
Las siguiente consulta nos devuelve toda la jerarquía de clases:
```c
[query public superPaths(aClass: uml::Class) : OrderedSet(Sequence(uml::Class))=
let superSuperPaths : Set(Sequence(uml::Class)) = if aClass.superClass->isEmpty()
then OrderedSet(Sequence(Class)){Sequence(Class){}}
else aClass.superClass->iterate(superClass1;
result : Set(Sequence(uml::Class)) = Set(Sequence(uml::Class)){}
| result->union(superPaths(superClass1)))
endif in
superSuperPaths->collectNested(superPath : Sequence(uml::Class) |
	 superPath->prepend(aClass))->asOrderedSet()
/]
```
Con esta consulta podemos armar otra que nos devuelva todos los caminos de una clase a otra:
```c
[query public superPaths(aClass: uml::Class) : OrderedSet(Sequence(uml::Class))=
[query public getPaths(fromClass: uml::Class, toClass: uml::Class) : OrderedSet(Sequence(uml::Class))=
fromClass.superPaths()->select(path: Sequence(uml::Class) | path->includes(toClass))
->collectNested(path: Sequence(uml::Class) | path->subSequence(2,path->indexOf(toClass)))
->asOrderedSet()
/]
```


Debido a la dificultad que significó obtener esta consulta en OCL, incursionamos en cómo realizar la consulta en JAVA utilizando un envolvedor (wrapper en inglés) que facilita Acceleo.  
El siguiente es el código del envolvedor:
```c
[query public getPathsJAVA(arg0 : Class, arg1 : Class) : Set ( Sequence (Class))
= invoke('org.eclipse.acceleo.module.oopc.services.UML2OOPCServices', 
'getPathsJAVA(org.eclipse.uml2.uml.Class, org.eclipse.uml2.uml.Class)',
Sequence{arg0, arg1})
/]
```
El mismo invoca al método referenciado dentro del método `invoke()`.  
El código JAVA que resuelve la consulta es el siguiente:
```java
import org.eclipse.uml2.uml.Class;

public class UML2OOPCServices {
	public Set<List<Class>> getPathsJAVA(Class fromClass, Class toClass) {
		    
		Set<List<Class>> result = new HashSet<List<Class>>();
		 
		if (fromClass == toClass)return result;
		 
		EList<Class> superClasses = fromClass.getSuperClasses();
		 
		if(superClasses.isEmpty())return result;
		
		for (Class superClass : superClasses) {
			Set<List<Class>> subPath = getPathsJAVA(superClass, toClass);
			if (subPath.isEmpty() && superClass==toClass){
				ArrayList<Class> justTheClass = new ArrayList<Class>();
				justTheClass.add(toClass);
				result.add(justTheClass);
			}
			else{
				for(List<Class> path : subPath){
					path.add(0, superClass);
					result.add(path);
				}	
			}
		} 
		return result;
	 } 
}

```
Fue mucho más sencillo resolver la consulta con JAVA ya que es un lenguaje conocido y gracias a las herramientas de depuración.  

#### Mejoras
La mejora que proponemos para simplificar el mapeo es el uso de estructuras anónimas de ISO C11 en la forma en que los usa la especificación de [Ben Klemens](#BenKlemens). Esto permitiría poder referenciar los métodos desconociendo la jerarquía de clases desde la clase a generar hasta dicha clase.
Por otro lado, tendría  como consecuencia no poder heredar más de dos clases de distinta jerarquía con métodos del mismo nombre.  
La necesidad de conocer el nombre de la clase que declaró por primera vez el método, como vimos en SOOPC, tiene que ver con mantener la firma del mismo, esto se puede cambiar como propusimos allí con una macro y compilación condicional.


#### Verificación
El siguiente modelo se utilizó para la verificación de las consultas anteriores
![](source/figures/oopc_model.png)\ 

Definimos la siguiente plantilla para utilizarla con la consulta `getFirstClassesDefinerName`
```java
[template public genClass (aClass : uml::Class) ]
[for (anOp:uml::Operation | aClass.ownedOperation)]
	[for (aClass1 : uml::Class | aClass.getFirstClassesDefinerName(anOp))]
	[if(aClass1=aClass)]
	defmethod([aClass1.name/]_[anOp.name/])
	[else]
        [for (path:Sequence(Class) | getPaths(aClass,aClass1))]
	overload([for(c:Class|path)][c.name/].[/for][anOp.name/]) = methodOvldName([anOp.name/],[aClass1.name/])
	[/for]

	[/if]
	[/for]

[/for]
[/template]
```

De esta manera para `Class5` el código generado es:
```c
	overload(Class4.Class2.Operation1) = methodOvldName(Operation1,Class2)
	overload(Class4.Class3.Operation1) = methodOvldName(Operation1,Class3)
	overload(Class4.Class2.Class6.Operation2) = methodOvldName(Operation2,Class6)
	overload(Class4.Class3.Class6.Operation2) = methodOvldName(Operation2,Class6)
```

Si distintas clases definen por primera vez el método como en el caso del método `Operation1()` por las clases `Class2` y `Class3` entonces pueden obtenerse los caminos hacia cada una.


### Declaración de métodos polimórficos en OOPC
La problemática es la misma que para SOOPC, los métodos polimórficos definidos por primera vez deben ser los únicos a ser incluidos en la tabla virtual. Las consultas presentadas para SOOPC servirán para OOPC también, principalmente porque `getEntireClassesHierarchechy()` puede utilizarse para devolver el set de clases padre incluso en un caso de herencia múltiple. Otra manera de realizar esta consulta es preguntando si la consulta `getFirstClassesDefinerName()` introducida en la sección anterior devuelve la clase que se está generando como la primera en definir el método. Esta última manera fue la utilizada en la plantilla de la verificación anterior.


#### Mejoras
La mejora propuesta sigue siendo en todos los casos mediante el uso de macros y compilación condicional.


#### Verificación
El modelo y la plantilla anterior servirán de verificación. Para `Class2` el código generado es:
```c
defmethod(Class2_Operation1)
```
con lo que se indica que el método `Operation1()` es la primera vez que se define.


### Declaración de métodos polimórficos en OOC de Tibor Miseta, OOC-S y GObject
De nuevo para saber si declarar un método polimórfico en la tabla virtual o no debemos saber si el mismo no se trata de una redefinición de método. Este caso es distinto a los anteriores ya que un método no sólo pudo haber sido definido en una clase padre sino también en una interfaz o mixin que estemos realizando o que haya sido realizada por una clase padre. Para eso podemos excluir dichos métodos polimórficos utilizando la consulta `getNotInterfaceRedefiner()` 

```java
[query 
public 
getNotInterfaceRedefiner (seq : Sequence(Operation) ) 
  : Sequence(Operation) =
 seq->select(op : uml::Operation 
  | not(op.isAnInterfaceMethodRedefinition()))
 ->asSequence()/]


[query 
public 
isAnInterfaceMethodRedefinition (anOperation : Operation ) : Boolean =  
 anOperation.class.getEntireClassesHierarchechy().getAllInterfaces()
  .getOwnedOperations()->exists(anInterfaceOperation:Operation 
  |redefines(anInterfaceOperation,anOperation))/]
```
La forma de obtener las interfaces de una clase es a través de la relación de realización como en la siguiente consulta:
```java
[query public getAllInterfaces(aClass : Class) : Bag(Interface) = 
aClass.clientDependency->filter(Realization).supplier
 ->filter(uml::Interface) 
/]
``` 

La siguiente plantilla genera la declaración de métodos nuevos para una clase escrita bajo OOC de Tibor Miseta:
```c
Virtuals( [aClass.name/], [aClass.getSuperClassName()/] );


[let seqOp : Sequence(uml::Operation) = aClass.getOwnedOperations()->getVirtual()->
			getNotInline()->getNotRedefine()->getNotInterfaceRedefiner()]
[for (o : Operation | seqOp)  ]
	[o.generateReturnType()/] (*[o.name/])([o.generateOOCMethodArguments()/]);
[/for]
[/let]

[for (namedElement : NamedElement | aClass.getAllInterfaces())]
	Interface( [namedElement.name/] );
[/for]

EndOfVirtuals;
```


#### Mejoras
Al igual que para el caso de la redefinición de métodos definidos en clases, la solución para esto es mediante una macro y compilación condicional como en SOOPC. Eso permitiría generar la operación sin saber si es que pertenece a una interfaz o no dejando la tarea al precompilador. En las interfaces sólo se debe generar la macro para cada método sin agregar la compilación condicional ya que las interfaces en estos dos frameworks no pueden heredar de otras. Esta solución no es aplicable en el caso de OOC-S ya que los métodos están definidos dentro de una macro. Otra solución podría venir por el lado del modelo especificando, mediante un estereotipo, si se trata de una definición de un nuevo método.


#### Verificación
Utilizamos el siguiente modelo para verificar las consultas  
![](source/figures/ooc_model.png)

Para `Child` se generó la siguiente definición de nuevos métodos polimórficos: 
```c
Virtuals( Child, Parent );
	char * (*getNameFollowedByClassName)(Child self);
EndOfVirtuals;
```
Como puede apreciarse, a pesar de que Child define un método polimórfico `serialize()` el mismo es identificado como declarado por primera vez por una interfaz realizada por una clase padre por lo que la misma no es incluida.


### Inicialización de tabla virtual en OOC de Tibor Miseta y GObject
Como en los frameworks anteriores que generan sus tablas virtuales anidando estructuras no anónimas (no como en el caso de [Ben Klemens](#BenKlemens) o OOC-S) para la inicialización de la tabla virtual se debe conocer la clase que realiza por primera vez cada método. En este caso además, cuando el método pertenece a una interfaz, se debe saber cuál es la clase que realiza por primera vez la interfaz. La siguiente plantilla inicializa la tabla virtual dentro de la función de inicialización:
```java
[let aClassVirtuals : Sequence(Operation) 
	= aClass.getOwnedOperations()->getVirtual()]
[for (classLink : Class 
 | getEntireClassesHierarchechy(aClass)->reverse()) ]
  [let classLinkVirtuals : Sequence(uml::Operation) 
    = classLink.getOwnedOperations()
	->getNotRedefine()->getNotInterfaceRedefiner() ]
     [for (o : Operation 
	|  aClassVirtuals->intersection( classLinkVirtuals ) ) ] 
(([classLink.getName()/]Vtable)vtab)->[o.name/] = 
	[o.generateMethodUpCast(classLink)/][o.class.name/]_[o.name/];
     [/for]	
  [/let]
  [for ( aClassLinkInterface : uml::Interface 
    |classLink.getAllInterfaces().oclAsType(uml::Interface) )]
      [let aClassLinkInterfaceOperations : Sequence(uml::Operation) 
	= aClassLinkInterface.getOwnedOperations() ]
	 [for (anInterfaceOperation : Operation 
	  |  aClassVirtuals->intersection(aClassLinkInterfaceOperations))]
(([classLink.getName()/]Vtable)vtab)-> \
[aClassLinkInterface.getName()/].[anInterfaceOperation.getName()/] = \
[anInterfaceOperation.generateInterfaceMethodUpCast(aClassLinkInterface)/] [aClass.getName()/]_[anInterfaceOperation.getName()/];
	 [/for]	
      [/let]
  [/for]
[/for]
[/let]
```
En este caso el framework exige un casteo de la implementación de los métodos para asignarlos a la tabla virtual. Esto también requiere conocer la clase o interfaz que lo definió por primera vez. 


#### Mejoras
De nuevo la mejora propuesta será anidar estructuras anónimas de ISO C11. Para el casteo de los métodos propondremos crear una tipo junto al método en la tabla virtual que posea la firma del método.


#### Verificación
Para el modelo presentado en la sección anterior se generó la siguiente inicialización de tabla virtual para la clase Child:
```c
ChildVtable vtab = & ChildVtableInstance;

((ParentVtable)vtab)->vPrint = (void(*)(Parent self))Child_vPrint;
((ParentVtable)vtab)->Serializable.serialize = 
		(void(*)(Object self, char * out))Child_serialize;
((ChildVtable)vtab)->getNameFollowedByClassName = 
		(char *(*)(Child self))Child_getNameFollowedByClassName;
```


### Constructor de MetaClase en OOC de Axel Tobías Schreiner
Para este framework habíamos presentado dos propuestas de expresión, una con metaclases implícitas y otra con explícitas. El código desde un modelo con metaclases explícitas solo requiere de información directa para sus plantillas. En el caso de un modelo con metaclases implícitas se debe saber si los métodos polimórficos declarados por la clase corresponden a una redefinición o no, esto último implica que una nueva metaclase debe declararse y los métodos nuevos se deben declarar en su constructor. Esto se puede lograr a través de la consulta `getFirstClassesDefinerName()` que puede compararse para saber si es igual a la clase actual.  
  
Esta observación de la diferencia de dificultad en el mapeo al código entre dos formas de modelado puede ser útil a la hora de implementar cualquier generador de código desde UML.  


 
## Dificultades encontradas
Acceleo y OCL son lenguajes muy declarativos, es decir basados en la matemática más que en la manera de razonar de las personas, esto nos ha dificultado la resolución de los mapeos. Como alternativa a las consultas en OCL, JAVA puede utilizarse el cual es un lenguaje mucho más amigable para la mayoría de los programadores. La única dificultad en realizar consultas en JAVA es tener que codificar un envolvedor (wrapper en inglés) para cada una. Sería importante generar un estudio comparativo entre Acceleo y otras tecnologías para generar código desde UML, Xtend por ejemplo está siendo ampliamente usado en distintos proyectos como Yakindu [^5] y Papyrus [^6].  

<!--¿Qué es un lenguaje de programación declarativo?

Un  **lenguaje declarativo**  es un tipo de  **lenguaje**  de programación basado más en las matemáticas y en la lógica que los  **lenguajes**  imperativos, más cercanos estos al razonamiento humano. Los  **lenguajes declarativos**  no dicen cómo hacer una cosa, sino, más bien, qué cosa hacer.3 oct. 2005

http://enciclopedia.us.es/index.php/Lenguaje_declarativo
enciclopedia.us.es/index.php/Lenguaje_declarativo-->
Una desventaja de acceleo actualmente es la falta de un framework de testing, aunque puede ser minimizado por la variedad de herramientas de desarrollo para OCL, el lenguaje de las consultas en acceleo. Las consultas escritas en JAVA sí pueden ser testeadas, por ejemplo, con JUnit [^7].  
Si bien se decidió codificar tres generadores para UML generators, no se obtuvo un impulso por parte de su comunidad ya que desde el comienzo de esta tesis la misma declinó rápidamente, los últimos plugins publicados del proyecto son para Eclipse Mars (al momento de escritura de esta tesis 3 versiones atrás de la última versión). Por el contrario, la comunidad de Acceleo sí es una comunidad activa.


## Resumen
Hemos podido escribir un generador de código completo para algunos de los frameworks analizados y hemos analizado los puntos necesarios para hacer factible generar código para los seis frameworks objetivo. Para eso elegimos Acceleo dentro del conjunto de herramientas especializadas en generación de código desde UML. La necesidad de información adicional, comparado con otros LPOO, que poseen los frameworks nos llevó a escribir consultas en OCL (tecnología central en Acceleo), un lenguaje altamente declarativo y que hemos encontrado difícil de usar, la dificultad en una de las consultas nos llevó a incursionar en las consultas escritas en JAVA, las mismas resultan una herramienta muy conveniente. Esta necesidad de información adicional podría suplirse modificando la especificación de codificación de los frameworks, esto puede traer consecuencias importantes como tener que cambiar el estándar en el que se está escribiendo el código, la consecuencia de tener que escribir más líneas de código es relativa si se está utilizando un generador de código. Otra manera de simplificar el mapeo es utilizando una forma de modelar que requiera inferir menos información (como vimos para para OOC en metaclases explícitas respecto de las implícitas).  
Hemos utilizado el conocimiento adquirido para esta tesis para implementar un generador de código para los frameworks SOOPC, OOC de Miseta y Dynace. Para esto hemos modificado el proyecto UML Generators y en especial el generador UML to Embedded C Generator. El proyecto resultante puede encontrarse en el repositorio especificado en el apéndice. Hemos tenido la oportunidad de utilizar el generador en los procesos de producción de una empresa dedicada a los sistemas embebidos de forma satisfactoria.

[^5]: https://www.itemis.com/en/yakindu/
[^6]: https://www.papyrus.com/
[^7]: https://junit.org/


