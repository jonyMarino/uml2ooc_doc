
# Apéndice: Estructura del repositorio que acompaña este escrito {.unnumbered}


## Ubicación {.unnumbered}
El código y los proyectos que acompañan a esta tesis y de los cuáles hemos hecho referencia en la misma se encuentran en github: [https://github.com/jonyMarino/tesis](https://github.com/jonyMarino/tesis) así como en el CD que se entrega con esta tesis. Dentro del mismo se encuentran archivos `README` que indican cómo utilizar y compilar cada proyecto.


## Estructura {.unnumbered}
La siguiente es la estructura simplificada de directorios del repositorio:

```c
.
├── ejemplos_de_codificacion_para_cada_framework
│   ├── BenKlemens
│   ├── COS
│   │   ├── COS
│   │   └── COSDemo
│   ├── Dynace
│   │   ├── Dynace
│   │   └── DynaceDemo
│   ├── GObject
│   ├── OOC_ATS
│   │       └── ejemplo_tesis
│   ├── OOCS
│   ├── OOC_Tibor_Miseta
│   ├── OOPC
│   └── SOOPC
├── generador_de_codigo_para_SOOPC_OOC_Y_DYNACE
│   ├── UML2ooec
│   │   └── org.eclipse.umlgen.gen.embedded.c
│   │       └── plugins
│   │           ├── org.eclipse.umlgen.gen.embedded.c
│   │           └── org.eclipse.umlgen.gen.embedded.c.profile
│   ├── verificaciones_generacion_de_codigo_para_OOC_TM
│   └── verificaciones_generacion_de_codigo_para_SOOPC
└── consultas_para_un_generador_de_codigo_para_OOPC
    └── org.eclipse.acceleo.module.oopc
        ├── model
        ├── src
        └── src-gen

```


Dentro de la carpeta `ejemplos_de_codificacion_para_cada_framework` se encuentra el código utilizado en la sección *Estado del arte en programación orientada a objetos en C* para analizar la codificación bajo cada framework. La misma contiene nueve carpetas correspondientes a los nueve frameworks. Dentro de la carpeta `Dynace` y `COS` se tiene un sub módulo de los repositorios de cada framework, el código de ejemplo para COS se encuentra en COSDemo y el de Dynace en DynaceDemo. Los ejemplos de SOOPC, OOC de Tibor Miseta y Dynace fueron generados con el generador de código desde UML a excepción del archivo main.c y el cuerpo de los métodos.  
Dentro de la carpeta `generador_de_codigo_para_SOOPC_OOC_Y_DYNACE` tenemos un sub módulo con un clon del repositorio de *UML Generators* con las modificaciones realizadas al *UML to Embedded C generator* tanto en el generador como en el *profile* (donde definimos los estereotipos para nuestro generador). También se encuentran en la carpeta dos proyectos utilizados para verificar el correcto funcionamiento del generador tanto para el framework SOOPC como OOC de Tibor Miseta, los mismos contienen un modelo UML modelado con papyrus y el código generado se encuentra en la carpeta `src-gen`.  
Por último, la carpeta `consultas_para_un_generador_de_codigo_para_OOPC` contiene un proyecto Acceleo dónde se encuentran en la carpeta `src` las consultas necesarias para implementar un generador de código para el framework OOPC. En la carpeta `model` se encuentra el modelo utilizado para corroborar el correcto funcionamiento de las consultas. En la carpeta `src-gen` se encuentra el código generado por la plantilla que utiliza dichas consultas.  


